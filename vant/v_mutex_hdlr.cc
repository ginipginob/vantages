/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include "v_logger.h"
#include <stdio.h>

#include "v_mutex_hdlr.h"
#include "v_mutex.h"
#include "v_rw_mutex.h"
#include "v_defs.h"

VMutexHandler::VMutexHandler(VMutex &p_oMutex)
  : m_oMutex(p_oMutex)
{
  if (!m_oMutex.lock())
  {
    v_elog(VL_CRITICAL, "Unable to lock mutex.\n");
  }
}

VMutexHandler::VMutexHandler(VRwMutex &p_oMutex, bool p_bRead /*= true*/)
  : m_oMutex(p_oMutex)
{
  if (p_bRead && !p_oMutex.readLock())
  {
    v_elog(VL_CRITICAL, "Unable to read lock mutex.\n");
  }
  else if (!p_bRead && !p_oMutex.writeLock())
  {
    v_elog(VL_CRITICAL, "Unable to write lock mutex.\n");
  }
}

VMutexHandler::~VMutexHandler()
{
  if (!m_oMutex.unlock())
  {
    v_elog(VL_CRITICAL, "Unable to unlock mutex.\n");
  }
}

