/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include "v_logger.h"
#include <stdio.h>

#include "v_thrd_pool.h"
#include "v_thrd.h"
#include "v_mutex_hdlr.h"
#include "v_defs.h"

VThreadPool::VThreadPool()
  : m_bRun(false),
    m_iSize(10)
{

}

VThreadPool::~VThreadPool()
{
  kill();
}

bool VThreadPool::init(VThread *p_pThreadType /*= NULL*/)
{
  bool bRet = false;

  if (m_bRun)
  {
    v_elog(VL_CRITICAL, "Pool is already running.\n");
  }
  else
  {
/*
    if (NULL == p_pThreadType)
    {
      p_pThreadType = new VThread(this);
    }
*/

    VThread oDefaultThrd(this);
    m_bRun = true;
    bRet = true;
    for (int i = 0; i < getSize(); i++)
    {
//      VThread *pThr = p_pThreadType->dup();
      VThread *pThr = NULL;
      pThr = (NULL == p_pThreadType) ?  oDefaultThrd.dup() : p_pThreadType->dup();

      if (!pThr->start())
      {
        v_elog(VL_CRITICAL, "Unable to start thread, killing...\n");
        pThr->kill();
        delete pThr;
        bRet = false;
        m_bRun = false;
        break;
      }
      else
      {
        v_elog(VL_CRITICAL, "Started thread.\n");
        m_oThreads.push_back(pThr);
        m_oFree.push_back(pThr);
      }
    }
  }

/*
  if (NULL != p_pThreadType)
  {
    delete p_pThreadType;
    p_pThreadType = NULL;
  }
*/

  return bRet;
}

int VThreadPool::getSize()
{
  return m_iSize;
}

void VThreadPool::setSize(int p_iSize)
{
  m_iSize = p_iSize;
}

int VThreadPool::getNumFree()
{
  return (int) m_oFree.size();
}

bool VThreadPool::checkIn(VThread &p_oThrd)
{
  bool bRet = false;

//  if (!p_oThrd.done())
//  {
//    v_elog(VL_CRITICAL, "Unable to check thread in until it is done.\n");
//  }
//  else
  //
  // BEGIN CRITICAL SECTION
  //
  {
    VMutexHandler oHandler(m_oMutex);

    m_oFree.push_back(&p_oThrd);
    m_oMutex.signalAll();
    bRet = true;
  }
  //
  // END CRITICAL SECTION
  //

  return bRet;
}

VThread *VThreadPool::checkOut()
{
  VThread *pRet = NULL;

  while (NULL == pRet && m_bRun)
  //
  // BEGIN CRITICAL SECTION
  //
  {
    VMutexHandler oHandler(m_oMutex);

    if (!m_oFree.empty())
    {
      pRet = m_oFree.front();
      m_oFree.pop_front();
    }
    else
    {
      m_oMutex.wait(1000);
    }
  }
  //
  // END CRITICAL SECTION
  //

  return pRet;
}

bool VThreadPool::kill()
{
  m_bRun = false;

  VantThrdIter_t tIter;
  for (tIter = m_oThreads.begin();
       m_oThreads.end() != tIter;
       tIter++)
  {
    (*tIter)->kill();
  }

  for (tIter = m_oThreads.begin();
       m_oThreads.end() != tIter;
       tIter++)
  {
    (*tIter)->join();
    delete (*tIter);
  }

  m_oThreads.clear();
  m_oFree.clear();

  return !m_bRun;
}
