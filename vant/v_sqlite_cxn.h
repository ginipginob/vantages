/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _V_DAO_H
#define _V_DAO_H

#include <string>
#include <list>

#include "i_v_dupable.h"
#include "v_mutex.h"
#include "v_rw_mutex.h"
#include "v_defs.h"

typedef struct sqlite3 sqlite3;
typedef struct sqlite3_stmt sqlite3_stmt;

class VDbCxn : public IVDupable
{
  // Member Variables
  private:
    sqlite3 *m_pCxn;
    sqlite3_stmt *m_pStmt;
    char *m_szError;
    bool m_bConnected;
    std::string m_sQuery;
    std::string m_sSig;
    const char *m_szConnectFile;
    int m_iConnectLine;

    static int s_iMaxCxns;
    static std::list<sqlite3 *> s_oCxnList;
    static std::list<sqlite3 *> s_oFreeCxnList;
    static VMutex s_oMutex;
    static VRwMutex s_oRwMutex;

  // Methods
  public:
    VDbCxn();
    virtual ~VDbCxn();

    virtual std::string cxnName();

    static void clearList(DbCxnList_t &p_oList);

    static bool finalize();

  protected:

    virtual bool connect(const char *p_szFile = __FILE__, int p_iLine = __LINE__);
    virtual bool disconnect();
    virtual bool clearQuery();
    virtual bool isConnected();

    virtual bool prepareQuery(const char *p_szQuery);
    virtual bool prepareQuery(std::string &p_sQuery);

    virtual bool setInt(int p_iIndex, int p_iVal);
    virtual bool setDouble(int p_iIndex, double p_dVal);
    virtual bool setStr(int p_iIndex, std::string &p_sVal);
    virtual bool setBlob(int p_iIndex, char *p_pBuff, size_t p_uLen);
    virtual bool setNULL(int p_iIndex);

    virtual bool exec();

    virtual bool update();
    virtual bool next();

    virtual int getInt(int p_iCol);
    virtual double getDouble(int p_iCol);
    virtual bool getStr(int p_iCol, std::string &p_sOutput);
    virtual bool getBlob(int p_iCol, char **p_ppOutput, int &p_iOutLen);
    virtual bool isNULL(int p_iCol);

    virtual int getLastID();
};

#endif
