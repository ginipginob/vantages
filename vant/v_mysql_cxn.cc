/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _V_NO_MYSQL

// #include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// #include <my_global.h>
#include <mysql.h>

#include "v_mysql_cxn.h"
#include "v_logger.h"
#include "v_defs.h"

using namespace std;

#if MYSQL_VERSION_ID >= 80000 && MYSQL_VERSION_ID < 90000 
#define my_bool bool
#endif

VMysqlCxn::VMysqlCxn()
  : m_pCxn(NULL),
    m_pStmt(NULL),
    m_pResult(NULL),
    m_pResBindVars(NULL),
    m_iNumResBindVars(0),
    m_pParamBindVars(NULL),
    m_iNumParamBindVars(0),
    m_bConnected(false),
    m_szConnectFile(NULL),
    m_iConnectLine(-1),
    m_iPort(0)
{

}

VMysqlCxn::~VMysqlCxn()
{
  disconnect();
}

std::string VMysqlCxn::cxnName()
{
  return string(V_MYSQL_CXN_NAME);
}

bool VMysqlCxn::connect(const char *p_szFile, int p_iLine)
{
  bool bRet = true;

  disconnect();
  m_szConnectFile = p_szFile;
  m_iConnectLine = p_iLine;

  m_pCxn = mysql_init(NULL);

  if (m_sHost.empty()
      && !parseCxnString())
  {
    v_elog(VL_CRITICAL, "Unable to connect without proper connection string.\n");
  }
  else if (NULL == mysql_real_connect(m_pCxn, 
                                      m_sHost.c_str(), 
                                      m_sUser.c_str(), 
                                      m_sPass.c_str(), 
                                      m_sDB.c_str(), 
                                      m_iPort, 
                                      NULL, 
                                      0))
  {
    v_elog(VL_CRITICAL, "Unable to open DB connection: '%s'\n", mysql_error(m_pCxn));
    setError(mysql_error(m_pCxn));
  }
  else
  {
    v_elog(VL_INFO, "DB connected mysql://%s:%d/%s\n", m_sHost.c_str(), m_iPort, m_sDB.c_str());
    m_bConnected = true;
    bRet = true;
  }

  return bRet;
}

bool VMysqlCxn::disconnect()
{
  bool bRet = false;

  clearQuery();

  m_sQuery = "";

  setError(NULL);

  m_bConnected = false;
  if (NULL != m_pCxn)
  {
    mysql_close(m_pCxn);
    m_pCxn = NULL;
  }

  return bRet;
}

bool VMysqlCxn::isConnected()
{
  return m_bConnected;
}

bool VMysqlCxn::ping()
{
  bool bRet = false;

  if (isConnected()) {
    int iRet = mysql_ping(m_pCxn);
    if (0 != iRet) {
      /*
      std::string sErr;
      if (CR_COMMANDS_OUT_OF_SYNC == iRet) {
        sErr = "Commands were executed in an improper order.";
      }
      else if (CR_SERVER_GONE_ERROR == iRet) {
        sErr = "The MySQL server has gone away: " + iRet;
      }
      else if (CR_UNKNOWN_ERROR == iRet) {
        sErr = "An unknown error occurred: " + iRet;
      }
      else {
        sErr = "Unable to locate error type: " + iRet;
      }
      */
      std::string sErr = "DB is flagged as connected, but PING failed with '";
      sErr += mysql_error(m_pCxn);
      sErr += "' [";
      sErr += iRet;
      sErr += "].  Disconnecting...";
      v_elog(VL_ERROR, "%s\n", sErr.c_str());
      setError(sErr.c_str());

      disconnect();
      bRet = false;
    }
    else {
      v_elog(VL_DEBUG, "Ping successful.  DB is connected and live.\n");
      bRet = true;
    }
  }

  return bRet;
}

bool VMysqlCxn::clearQuery()
{
  bool bRet = true;
  m_sQuery = "";

  if (NULL != m_pResult)
  {
    mysql_free_result(m_pResult);
    m_pResult = NULL;
  }

  if (NULL != m_pStmt)
  {
    mysql_stmt_close(m_pStmt);
    m_pStmt = NULL;
  }

  if (NULL != m_pResBindVars)
  {
    for (int i = 0; i < m_iNumResBindVars; i++)
    {
      if (NULL != m_pResBindVars[i].buffer)
      {
        if (MYSQL_TYPE_STRING == m_pResBindVars[i].buffer_type
            || MYSQL_TYPE_VAR_STRING == m_pResBindVars[i].buffer_type
            || MYSQL_TYPE_TINY_BLOB == m_pResBindVars[i].buffer_type
            || MYSQL_TYPE_BLOB == m_pResBindVars[i].buffer_type
            || MYSQL_TYPE_MEDIUM_BLOB == m_pResBindVars[i].buffer_type
            || MYSQL_TYPE_LONG_BLOB == m_pResBindVars[i].buffer_type
            || MYSQL_TYPE_BIT == m_pResBindVars[i].buffer_type)
        {
          delete[] (char *) m_pResBindVars[i].buffer;
        }
        else
        {
          delete (char *) m_pResBindVars[i].buffer;
        }
      }

      delete m_pResBindVars[i].is_null;
      delete m_pResBindVars[i].length;
      delete m_pResBindVars[i].error;
    }
    delete[] m_pResBindVars;
    m_pResBindVars = NULL;
    m_iNumResBindVars = 0;
  }

  if (NULL != m_pParamBindVars)
  {
    for (int i = 0; i < m_iNumParamBindVars; i++)
    {
      if (NULL != m_pParamBindVars[i].buffer)
      {
        if (MYSQL_TYPE_STRING == m_pParamBindVars[i].buffer_type
            || MYSQL_TYPE_BLOB == m_pParamBindVars[i].buffer_type)
        {
          delete[] (char *) m_pParamBindVars[i].buffer;
        }
        else
        {
          delete (char *) m_pParamBindVars[i].buffer;
        }
      }
      if (NULL != m_pParamBindVars[i].length)
      {
        delete m_pParamBindVars[i].length;
      }
    }
    delete[] m_pParamBindVars;
    m_pParamBindVars = NULL;
    m_iNumParamBindVars = 0;
  }

  return bRet;
}

bool VMysqlCxn::prepareQuery(std::string &p_sQuery)
{
  return prepareQuery(p_sQuery.c_str());
}

bool VMysqlCxn::prepareQuery(const char *p_szQuery)
{
  bool bRet = false;

  if (NULL == p_szQuery)
  {
    v_elog(VL_ERROR, "Unable to prepare NULL query.\n");
    setError("Unable to prepare NULL query.");
  }
  else if (!isConnected())
  {
    v_elog(VL_ERROR, "Connection is not established.\n");
  }
  else
  {
    clearQuery();
    MYSQL_RES *pResMeta = NULL;
    if (NULL == (m_pStmt = mysql_stmt_init(m_pCxn)))
    {
      v_elog(VL_ERROR, "Unable to init() statement\n");
      setError("Unable to init() statement");
    }
    else if (0 != mysql_stmt_prepare(m_pStmt, p_szQuery, strlen(p_szQuery)))
    {
      v_elog(VL_ERROR, "Unable to init() statement: %s\n", mysql_stmt_error(m_pStmt));
      setError(mysql_stmt_error(m_pStmt));
    }
    else
    {
      m_iNumParamBindVars = mysql_stmt_param_count(m_pStmt);
      if (0 == m_iNumParamBindVars)
      {
        v_elog(VL_DEBUG, "No params for this query, but that's ok...\n");
        bRet = true;
      }
      else
      {
        v_elog(VL_DEBUG, "Allocating %d bind params for this query.\n", m_iNumParamBindVars);
        m_pParamBindVars = new MYSQL_BIND[m_iNumParamBindVars];
        memset(m_pParamBindVars, 0, sizeof(MYSQL_BIND) * m_iNumParamBindVars);
        bRet = true;
      }
    }

    if (!bRet)
    {
      v_elog(VL_INFO, "Skipping result processing because of earlier errors.\n");
    }
    else if (NULL == (pResMeta = mysql_stmt_result_metadata(m_pStmt)))
    {
      // v_elog(VL_ERROR, "Unable to get result meta-data: %s\n", mysql_stmt_error(m_pStmt));
      // setError(mysql_stmt_error(m_pStmt));
      v_elog(VL_DEBUG, "No result set info from this query, but that's ok...\n");
      bRet = true;
    }
    else
    {
      bRet = false;
      // Since MySQL was nice enough to not do ANY result set management for
      // us, we have to look at the types coming back and manage all memory for it...
      // That starts here...
      int iNumResultCols = mysql_num_fields(pResMeta);
      if (0 > iNumResultCols)
      {
        v_elog(VL_ERROR, "Number of returned result columns is negative: %d\n", iNumResultCols);
        setError("Number of returned result columns is negative");
      }
      else if (0 == iNumResultCols)
      {
        v_elog(VL_ERROR, "Successfully created statement (no columns in result set)\n");
        bRet = true;
      }
      else
      {
        m_iNumResBindVars = iNumResultCols;
        m_pResBindVars = new MYSQL_BIND[m_iNumResBindVars];
        memset(m_pResBindVars, 0, sizeof(MYSQL_BIND) * m_iNumResBindVars);

        MYSQL_FIELD *pField = NULL;
        bRet = true;
        int i = 0;
        for (i = 0;
             NULL != (pField = mysql_fetch_field(pResMeta));
             i++)
        {
          if (i > iNumResultCols)
          {
            v_elog(VL_ERROR, "Number of returned fields is larger than meta-data indicated.\n");
            setError("Number of returned fields is larger than meta-data indicated.");
            break;
          }
          else if (!initBindVar(*pField, m_pResBindVars[i]))
          {
            v_elog(VL_ERROR, "Error creating result set bind var... failing.\n");
            break;
          }
        }
        if (i != iNumResultCols)
        {
          bRet = false;
        }
        else if (0 != mysql_stmt_bind_result(m_pStmt, m_pResBindVars))
        {
          v_elog(VL_ERROR, "Unable to bind results: %s\n", mysql_stmt_error(m_pStmt));
          setError(mysql_stmt_error(m_pStmt));
          bRet = false;
        }
        else
        {
          v_elog(VL_DEBUG, "Successfully created statement and result set binding.\n");
          bRet = true;
        }
      }

      mysql_free_result(pResMeta);
    }
  }

  return bRet;
}

bool VMysqlCxn::setInt(int p_iIndex, int p_iVal)
{
  bool bRet = false;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Unable to set param before DB is connected.\n");
    setError("Unable to set param before DB is connected.");
  }
  else if (NULL == m_pStmt)
  {
    v_elog(VL_ERROR, "Unable to set param before statement is prepared.\n");
    setError("Unable to set param before statement is prepared.");
  }
  else if (m_iNumParamBindVars <= p_iIndex)
  {
    v_elog(VL_ERROR, "Unable to set param for index that is greater than number of bind params %d >= %d\n", p_iIndex, m_iNumParamBindVars);
    setError("Unable to set param for index that is greater than number of bind params");
  }
  else
  {
    int *pVal = new int;
    *pVal = p_iVal;
    if (NULL != m_pParamBindVars[p_iIndex].buffer)
    {
      delete (char *) m_pParamBindVars[p_iIndex].buffer;
    }
    if (NULL != m_pParamBindVars[p_iIndex].length)
    {
      delete m_pParamBindVars[p_iIndex].length;
    }
    m_pParamBindVars[p_iIndex].buffer_type = MYSQL_TYPE_LONG;
    m_pParamBindVars[p_iIndex].buffer = (char *) pVal;
    m_pParamBindVars[p_iIndex].is_null = 0;
    m_pParamBindVars[p_iIndex].length = 0;
    bRet = true;
  }

  return bRet;
}

bool VMysqlCxn::setLongInt(int p_iIndex, long p_lVal)
{
  bool bRet = false;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Unable to set param before DB is connected.\n");
    setError("Unable to set param before DB is connected.");
  }
  else if (NULL == m_pStmt)
  {
    v_elog(VL_ERROR, "Unable to set param before statement is prepared.\n");
    setError("Unable to set param before statement is prepared.");
  }
  else if (m_iNumParamBindVars <= p_iIndex)
  {
    v_elog(VL_ERROR, "Unable to set param for index that is greater than number of bind params %d >= %d\n", p_iIndex, m_iNumParamBindVars);
    setError("Unable to set param for index that is greater than number of bind params");
  }
  else
  {
    long *pVal = new long;
    *pVal = p_lVal;
    if (NULL != m_pParamBindVars[p_iIndex].buffer)
    {
      delete (char *) m_pParamBindVars[p_iIndex].buffer;
    }
    if (NULL != m_pParamBindVars[p_iIndex].length)
    {
      delete m_pParamBindVars[p_iIndex].length;
    }
    m_pParamBindVars[p_iIndex].buffer_type = MYSQL_TYPE_LONGLONG;
    m_pParamBindVars[p_iIndex].buffer = (char *) pVal;
    m_pParamBindVars[p_iIndex].is_null = 0;
    m_pParamBindVars[p_iIndex].length = 0;
    bRet = true;
  }

  return bRet;
}

bool VMysqlCxn::setDouble(int p_iIndex, double p_dVal)
{
  bool bRet = false;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Unable to set param before DB is connected.\n");
    setError("Unable to set param before DB is connected.");
  }
  else if (NULL == m_pStmt)
  {
    v_elog(VL_ERROR, "Unable to set param before statement is prepared.\n");
    setError("Unable to set param before statement is prepared.");
  }
  else if (m_iNumParamBindVars <= p_iIndex)
  {
    v_elog(VL_ERROR, "Unable to set param for index that is greater than number of bind params %d >= %d\n", p_iIndex, m_iNumParamBindVars);
    setError("Unable to set param for index that is greater than number of bind params");
  }
  else
  {
    double *pVal = new double;
    *pVal = p_dVal;
    if (NULL != m_pParamBindVars[p_iIndex].buffer)
    {
      delete (char *) m_pParamBindVars[p_iIndex].buffer;
    }
    if (NULL != m_pParamBindVars[p_iIndex].length)
    {
      delete m_pParamBindVars[p_iIndex].length;
    }
    m_pParamBindVars[p_iIndex].buffer_type = MYSQL_TYPE_DOUBLE;
    m_pParamBindVars[p_iIndex].buffer = (char *) pVal;
    m_pParamBindVars[p_iIndex].is_null = 0;
    m_pParamBindVars[p_iIndex].length = 0;
    bRet = true;
  }


  return bRet;
}

bool VMysqlCxn::setStr(int p_iIndex, std::string &p_sVal)
{
  return setStr(p_iIndex, p_sVal.c_str(), p_sVal.size());
}

bool VMysqlCxn::setStr(int p_iIndex, const char *p_szVal, size_t p_uLen)
{
  bool bRet = false;

  if (NULL == p_szVal)
  {
    v_elog(VL_ERROR, "Unable to set NULL string, use setNULL().\n");
    setError("Unable to set NULL string, use setNULL()");
  }
  else if (!isConnected())
  {
    v_elog(VL_ERROR, "Unable to set param before DB is connected.\n");
    setError("Unable to set param before DB is connected.");
  }
  else if (NULL == m_pStmt)
  {
    v_elog(VL_ERROR, "Unable to set param before statement is prepared.\n");
    setError("Unable to set param before statement is prepared.");
  }
  else if (m_iNumParamBindVars <= p_iIndex)
  {
    v_elog(VL_ERROR, "Unable to set param for index that is greater than number of bind params %d >= %d\n", p_iIndex, m_iNumParamBindVars);
    setError("Unable to set param for index that is greater than number of bind params");
  }
  else
  {
    char *szVal = new char[p_uLen + 1];
    memset(szVal, 0, p_uLen + 1);
    if (p_uLen > 0)
    {
      memcpy(szVal, p_szVal, p_uLen);
    }
    if (NULL != m_pParamBindVars[p_iIndex].buffer)
    {
      delete[] (char *) m_pParamBindVars[p_iIndex].buffer;
    }
    if (NULL != m_pParamBindVars[p_iIndex].length)
    {
      delete m_pParamBindVars[p_iIndex].length;
    }
    m_pParamBindVars[p_iIndex].buffer_type = MYSQL_TYPE_STRING;
    m_pParamBindVars[p_iIndex].buffer = szVal;
    m_pParamBindVars[p_iIndex].buffer_length = p_uLen;
    // <EMO> This seg faults on some platforms for some reason...
    // int *pLen = new int;
    // *pLen = p_uLen;
    // m_pParamBindVars[p_iIndex].length = (unsigned long int *) pLen;
    // </EMO>
    bRet = true;
  }


  return bRet;
}

bool VMysqlCxn::setBlob(int p_iIndex, char *p_pBuff, size_t p_uLen)
{
  bool bRet = false;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Unable to set param before DB is connected.\n");
    setError("Unable to set param before DB is connected.");
  }
  else if (NULL == m_pStmt)
  {
    v_elog(VL_ERROR, "Unable to set param before statement is prepared.\n");
    setError("Unable to set param before statement is prepared.");
  }
  else if (m_iNumParamBindVars <= p_iIndex)
  {
    v_elog(VL_ERROR, "Unable to set param for index that is greater than number of bind params %d >= %d\n", p_iIndex, m_iNumParamBindVars);
    setError("Unable to set param for index that is greater than number of bind params");
  }
  else if (NULL == p_pBuff)
  {
    v_elog(VL_ERROR, "Unable to set NULL blob (use setNULL()).\n");
    setError("Unable to set NULL blob (use setNULL()).");
  }
  else if (0 == p_uLen)
  {
    v_elog(VL_ERROR, "Unable to set blob with size 0.\n");
    setError("Unable to set blob with size 0.");
  }
  else
  {
    char *szVal = new char[p_uLen];
    memset(szVal, 0, p_uLen);
    memcpy(szVal, p_pBuff, p_uLen);
    int *pLen = new int;
    *pLen = p_uLen;
    if (NULL != m_pParamBindVars[p_iIndex].buffer)
    {
      delete[] (char *) m_pParamBindVars[p_iIndex].buffer;
    }
    if (NULL != m_pParamBindVars[p_iIndex].length)
    {
      delete m_pParamBindVars[p_iIndex].length;
    }
    m_pParamBindVars[p_iIndex].buffer_type = MYSQL_TYPE_BLOB;
    m_pParamBindVars[p_iIndex].buffer = szVal;
    m_pParamBindVars[p_iIndex].buffer_length = p_uLen;
    m_pParamBindVars[p_iIndex].length = (long unsigned int *) pLen;
    bRet = true;
  }


  return bRet;
}

bool VMysqlCxn::setNULL(int p_iIndex)
{
  bool bRet = false;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Unable to set param before DB is connected.\n");
    setError("Unable to set param before DB is connected.");
  }
  else if (NULL == m_pStmt)
  {
    v_elog(VL_ERROR, "Unable to set param before statement is prepared.\n");
    setError("Unable to set param before statement is prepared.");
  }
  else if (m_iNumParamBindVars <= p_iIndex)
  {
    v_elog(VL_ERROR, "Unable to set param for index that is greater than number of bind params %d >= %d\n", p_iIndex, m_iNumParamBindVars);
    setError("Unable to set param for index that is greater than number of bind params");
  }
  else
  {
    if (NULL != m_pParamBindVars[p_iIndex].buffer)
    {
      delete (char *) m_pParamBindVars[p_iIndex].buffer;
    }
    if (NULL != m_pParamBindVars[p_iIndex].length)
    {
      delete m_pParamBindVars[p_iIndex].length;
    }
    m_pParamBindVars[p_iIndex].buffer_type = MYSQL_TYPE_NULL;
    m_pParamBindVars[p_iIndex].buffer = NULL;
    m_pParamBindVars[p_iIndex].is_null = 0;
    m_pParamBindVars[p_iIndex].length = 0;
    bRet = true;
  }

  return bRet;
}

bool VMysqlCxn::exec()
{
  bool bRet = false;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Unable to execute before DB is connected.\n");
    setError("Unable to set execute DB is connected.");
  }
  else if (NULL == m_pStmt)
  {
    v_elog(VL_ERROR, "Unable to execute before statement is prepared.\n");
    setError("Unable to set execute statement is prepared.");
  }
  else if (0 != mysql_stmt_bind_param(m_pStmt, m_pParamBindVars))
  {
    v_elog(VL_ERROR, "Unable to bind parameters to statement: %s\n", mysql_stmt_error(m_pStmt));
    setError(mysql_stmt_error(m_pStmt));
  }
  else if (0 != mysql_stmt_execute(m_pStmt))
  {
    v_elog(VL_ERROR, "Unable to execute statement: %s\n", mysql_stmt_error(m_pStmt));
    setError(mysql_stmt_error(m_pStmt));
  }
  else
  {
    v_elog(VL_DEBUG, "Successfully executes statement\n");
    setError(NULL);
    bRet = true;
  }

  return bRet;
}

bool VMysqlCxn::update()
{
  bool bRet = false;

  bRet = exec();

  return bRet;
}

bool VMysqlCxn::next()
{
  bool bRet = false;

  int iErr = 0;
  if (!isConnected())
  {
    v_elog(VL_ERROR, "DB is not connected, cannot fetch next.\n");
  }
  else if (NULL == m_pStmt)
  {
    v_elog(VL_ERROR, "No active statement prepared.\n");
  }
  else if (0 != (iErr = mysql_stmt_fetch(m_pStmt)))
  {
    if (MYSQL_NO_DATA == iErr)
    {
      v_elog(VL_DEBUG, "No more data.\n");
    }
    else if (MYSQL_DATA_TRUNCATED == iErr)
    {
      v_elog(VL_ERROR, "Next row is truncated: [%d] %s\n", iErr, mysql_stmt_error(m_pStmt));
      setError(mysql_stmt_error(m_pStmt));
    }
    else
    {
      v_elog(VL_ERROR, "Unable to fetch next row: [%d] %s\n", iErr, mysql_stmt_error(m_pStmt));
      setError(mysql_stmt_error(m_pStmt));
    }
  }
  else
  {
    bRet = true;
  }

  return bRet;
}

int VMysqlCxn::getInt(int p_iCol)
{
  int iRet = 0;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Not connected.\n");
  }
  else if (p_iCol >= m_iNumResBindVars)
  {
    v_elog(VL_ERROR, "Unable to get column %d out of %d returned columns.\n", p_iCol, m_iNumResBindVars);
  }
  else if (m_pResBindVars[p_iCol].buffer_type != MYSQL_TYPE_INT24
           && m_pResBindVars[p_iCol].buffer_type != MYSQL_TYPE_LONG)
  {
    v_elog(VL_ERROR, "Unable to cast type %d to either %d or %d (MYSQL_TYPE_INT24 and MYSQL_TYPE_LONG)\n",
                     m_pResBindVars[p_iCol].buffer_type,
                     MYSQL_TYPE_INT24,
                     MYSQL_TYPE_LONG);
  }
  else if (NULL == m_pResBindVars[p_iCol].buffer)
  {
    v_elog(VL_ERROR, "Result buffer is NULL.\n");
  }
  else if (*(m_pResBindVars[p_iCol].is_null))
  {
    v_elog(VL_DEBUG, "Column is NULL\n");
  }
  else
  {
    iRet = *((int *) m_pResBindVars[p_iCol].buffer);
  }

  return iRet;
}

long VMysqlCxn::getLongInt(int p_iCol)
{
  long iRet = 0;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Not connected.\n");
  }
  else if (p_iCol >= m_iNumResBindVars)
  {
    v_elog(VL_ERROR, "Unable to get column %d out of %d returned columns.\n", p_iCol, m_iNumResBindVars);
  }
  else if (m_pResBindVars[p_iCol].buffer_type != MYSQL_TYPE_LONGLONG)
  {
    v_elog(VL_ERROR, "Unable to cast type %d to %d (MYSQL_TYPE_LONGLONG)\n",
                     m_pResBindVars[p_iCol].buffer_type,
                     MYSQL_TYPE_LONGLONG);
  }
  else if (NULL == m_pResBindVars[p_iCol].buffer)
  {
    v_elog(VL_ERROR, "Result buffer is NULL.\n");
  }
  else if (*(m_pResBindVars[p_iCol].is_null))
  {
    v_elog(VL_DEBUG, "Column is NULL\n");
  }
  else
  {
    iRet = *((long *) m_pResBindVars[p_iCol].buffer);
  }

  return iRet;
}

double VMysqlCxn::getDouble(int p_iCol)
{
  double dRet = 0.0;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Not connected.\n");
  }
  else if (p_iCol >= m_iNumResBindVars)
  {
    v_elog(VL_ERROR, "Unable to get column %d out of %d returned columns.\n", p_iCol, m_iNumResBindVars);
  }
  else if (m_pResBindVars[p_iCol].buffer_type != MYSQL_TYPE_FLOAT
           && m_pResBindVars[p_iCol].buffer_type != MYSQL_TYPE_DOUBLE)
  {
    v_elog(VL_ERROR, "Unable to cast type %d to either %d or %d (MYSQL_TYPE_FLOAT and MYSQL_TYPE_DOUBLE)\n",
                     m_pResBindVars[p_iCol].buffer_type,
                     MYSQL_TYPE_FLOAT,
                     MYSQL_TYPE_DOUBLE);
  }
  else if (NULL == m_pResBindVars[p_iCol].buffer)
  {
    v_elog(VL_ERROR, "Result buffer is NULL.\n");
  }
  else if (*(m_pResBindVars[p_iCol].is_null))
  {
    v_elog(VL_DEBUG, "Column is NULL\n");
  }
  else
  {
    if (MYSQL_TYPE_FLOAT == m_pResBindVars[p_iCol].buffer_type)
    {
      dRet = (double) *((float *) m_pResBindVars[p_iCol].buffer);
    }
    else
    {
      dRet = *((double *) m_pResBindVars[p_iCol].buffer);
    }
  }

  return dRet;
}

bool VMysqlCxn::getStr(int p_iCol, std::string &p_sOutput)
{
  bool bRet = false;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Not connected.\n");
  }
  else if (p_iCol >= m_iNumResBindVars)
  {
    v_elog(VL_ERROR, "Unable to get column %d out of %d returned columns.\n", p_iCol, m_iNumResBindVars);
  }
  else if (MYSQL_TYPE_STRING != m_pResBindVars[p_iCol].buffer_type
           && MYSQL_TYPE_VAR_STRING != m_pResBindVars[p_iCol].buffer_type)
  {
    v_elog(VL_ERROR, "Unable to cast type %d to either %d or %d (MYSQL_TYPE_STRING and MYSQL_TYPE_VAR_STRING)\n",
                     m_pResBindVars[p_iCol].buffer_type,
                     MYSQL_TYPE_STRING,
                     MYSQL_TYPE_VAR_STRING);
  }
  else if (NULL == m_pResBindVars[p_iCol].buffer)
  {
    v_elog(VL_ERROR, "Result buffer is NULL.\n");
  }
  else if (*(m_pResBindVars[p_iCol].is_null))
  {
    v_elog(VL_DEBUG, "Column is NULL\n");
  }
  else
  {
    p_sOutput.assign((const char *) m_pResBindVars[p_iCol].buffer, *(m_pResBindVars[p_iCol].length));
    bRet = true;
  }

  return bRet;
}

bool VMysqlCxn::getBlob(int p_iCol, char **p_ppOutput, int &p_iOutLen)
{
  bool bRet = false;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Not connected.\n");
  }
  else if (p_iCol >= m_iNumResBindVars)
  {
    v_elog(VL_ERROR, "Unable to get column %d out of %d returned columns.\n", p_iCol, m_iNumResBindVars);
  }
  else if (MYSQL_TYPE_TINY_BLOB != m_pResBindVars[p_iCol].buffer_type
           && MYSQL_TYPE_BLOB != m_pResBindVars[p_iCol].buffer_type
           && MYSQL_TYPE_MEDIUM_BLOB != m_pResBindVars[p_iCol].buffer_type
           && MYSQL_TYPE_LONG_BLOB != m_pResBindVars[p_iCol].buffer_type
           && MYSQL_TYPE_BIT != m_pResBindVars[p_iCol].buffer_type)
  {
    v_elog(VL_ERROR, "Unable to cast type %d to any of (%d, %d, %d, %d, %d) (MYSQL_TYPE_*BLOB or MYSQL_TYPE_BIT)\n",
                     m_pResBindVars[p_iCol].buffer_type,
                     MYSQL_TYPE_TINY_BLOB,
                     MYSQL_TYPE_BLOB,
                     MYSQL_TYPE_MEDIUM_BLOB,
                     MYSQL_TYPE_LONG_BLOB,
                     MYSQL_TYPE_BIT);
  }
  else if (NULL == m_pResBindVars[p_iCol].buffer)
  {
    v_elog(VL_ERROR, "Result buffer is NULL.\n");
  }
  else if (*(m_pResBindVars[p_iCol].is_null))
  {
    v_elog(VL_DEBUG, "Column is NULL\n");
  }
  else
  {
    p_iOutLen = *(m_pResBindVars[p_iCol].length);
    v_elog(VL_DEBUG, "Setting blob of length %d\n", p_iOutLen);
    *(p_ppOutput) = new char[p_iOutLen];
    memset(*(p_ppOutput), 0, p_iOutLen);
    memcpy(*(p_ppOutput), m_pResBindVars[p_iCol].buffer, p_iOutLen);
    bRet = true;
  }

  return bRet;
}

bool VMysqlCxn::isNULL(int p_iCol)
{
  bool bRet = false;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Not connected.\n");
  }
  else if (p_iCol >= m_iNumResBindVars)
  {
    v_elog(VL_ERROR, "Unable to get column %d out of %d returned columns.\n", p_iCol, m_iNumResBindVars);
  }
  else if (*(m_pResBindVars[p_iCol].is_null))
  {
    v_elog(VL_DEBUG, "Column is NULL\n");
    bRet = true;
  }
  else
  {
    v_elog(VL_DEBUG, "Column is not NULL\n");
    bRet = false;
  }

  return bRet;
}

long VMysqlCxn::getLastID()
{
  long iRet = -1;

  if (!isConnected())
  {
    v_elog(VL_ERROR, "Connection is not connected.\n");
  }
  else
  {
    iRet = mysql_insert_id(m_pCxn);
  }

  return iRet;
}

bool VMysqlCxn::parseCxnString()
{
  bool bRet = false;

  size_t uPos = 0;
  size_t uHostEnd = 0;
  size_t uPortStart = 0;
  string sCxn = getConnectString();

  int iSkipLen = strlen(V_DB_MYSQL_SPECIFIER);
  if ("" == sCxn)
  {
    v_elog(VL_CRITICAL, "Unable to parse empty connection string.\n");
  }
  else if (string::npos == (uPos = sCxn.find(V_DB_MYSQL_SPECIFIER)))
  {
    v_elog(VL_CRITICAL, "Unable to parse non-mysql connection string '%s'\n", sCxn.c_str());
  }
  else if (0 != (uPos = sCxn.find(V_DB_MYSQL_SPECIFIER)))
  {
    v_elog(VL_CRITICAL, "Unable to parse non-mysql connection string '%s'\n", sCxn.c_str());
  }
  else if (string::npos == (uHostEnd = sCxn.find('/', iSkipLen)))
  {
    v_elog(VL_CRITICAL, "Unable to find DB name in connection string '%s'\n", sCxn.c_str());
  }
  else
  {
    bRet = true;
    int iPort = 3306;
    string sHost;
    string sUser;
    string sPass;
    size_t uUsrPass = 0;
    if (string::npos != (uUsrPass = sCxn.find('@', iSkipLen)))
    {
      string sTmp = sCxn.substr(iSkipLen, uUsrPass - iSkipLen);
      size_t uTmp = sTmp.find(':');
      if (string::npos == uTmp)
      {
        sUser = sTmp;
      }
      else
      {
        sUser = sTmp.substr(0, uTmp);
        sPass = sTmp.substr(uTmp + 1);
      }
      iSkipLen += sTmp.size() + 1;
    }

    if (string::npos == (uPortStart = sCxn.find(':', iSkipLen))
        || uPortStart > uHostEnd)
    {
      sHost = sCxn.substr(iSkipLen, uHostEnd - iSkipLen);
    }
    else
    {
      bRet = false;
      sHost = sCxn.substr(iSkipLen, uPortStart - iSkipLen);
      string sPort = sCxn.substr(uPortStart + 1, uHostEnd - uPortStart);

      iPort = (int) strtol(sPort.c_str(), NULL, 10);
      if (0 == iPort)
      {
        v_elog(VL_CRITICAL, "Unable to convert port string to int: '%s'\n", sPort.c_str());
      }
      else
      {
        bRet = true;
      }
    }

    string sDB = sCxn.substr(uHostEnd + 1);
    if (sDB.empty())
    {
      v_elog(VL_CRITICAL, "Unable to find database instance name in connection string '%s'\n", sCxn.c_str());
      bRet = false;
    }
    else if (bRet)
    {
      m_iPort = iPort;
      m_sHost = sHost;
      m_sDB = sDB;
      if (string::npos != uUsrPass)
      {
        m_sUser = sUser;
        m_sPass = sPass;
      }
      v_elog(VL_DEBUG, "SUCCESS: mysql://'%s':'%s'@'%s':'%d'/'%s'\n%d %u %u\n", 
             m_sUser.c_str(),
             m_sPass.c_str(),
             m_sHost.c_str(), 
             m_iPort, 
             m_sDB.c_str(), 
             iSkipLen, (unsigned)uPortStart,
             (unsigned)uHostEnd);
    }
  }

  return bRet;
}

bool VMysqlCxn::initBindVar(MYSQL_FIELD &p_tField, MYSQL_BIND &m_tBindVar)
{
  bool bRet = false;

  memset(&m_tBindVar, 0, sizeof(m_tBindVar));

  m_tBindVar.buffer_type = p_tField.type;
  m_tBindVar.is_null = new my_bool;
  *m_tBindVar.is_null = 0;
  m_tBindVar.error = new my_bool;
  *m_tBindVar.error = 0;
  m_tBindVar.length = new unsigned long;
  *m_tBindVar.length = 0;

  bRet = true;
  setError(NULL);
  switch(p_tField.type)
  {
    case MYSQL_TYPE_TINY:
      m_tBindVar.buffer = (char *) new char;
      break;
    case MYSQL_TYPE_SHORT:
      m_tBindVar.buffer = (char *) new short int;
      break;
    case MYSQL_TYPE_INT24:
    case MYSQL_TYPE_LONG:
      m_tBindVar.buffer = (char *) new int;
      break;
    case MYSQL_TYPE_LONGLONG:
      // m_tBindVar.buffer = (char *) new long long int;
      m_tBindVar.buffer = (char *) new long;
      break;
    case MYSQL_TYPE_FLOAT:
      m_tBindVar.buffer = (char *) new float;
      break;
    case MYSQL_TYPE_DOUBLE:
      m_tBindVar.buffer = (char *) new double;
      break;
    case MYSQL_TYPE_NEWDECIMAL:
      m_tBindVar.buffer = new char[p_tField.length];
      m_tBindVar.buffer_length = p_tField.length;
      break;
    case MYSQL_TYPE_TIME:
    case MYSQL_TYPE_DATE:
    case MYSQL_TYPE_DATETIME:
    case MYSQL_TYPE_TIMESTAMP:
      m_tBindVar.buffer = (char *) new MYSQL_TIME;
      break;
    case MYSQL_TYPE_STRING:
    case MYSQL_TYPE_VAR_STRING:
      v_elog(VL_DEBUG, "Allocating string/blob (type %d) with buffer_length = %d\n", p_tField.type, p_tField.length + 1);
      m_tBindVar.buffer = new char[p_tField.length + 1];
      m_tBindVar.buffer_length = p_tField.length + 1;
      break;
    case MYSQL_TYPE_TINY_BLOB:
    case MYSQL_TYPE_BLOB:
    case MYSQL_TYPE_MEDIUM_BLOB:
    case MYSQL_TYPE_LONG_BLOB:
    case MYSQL_TYPE_BIT:
      v_elog(VL_DEBUG, "Allocating string/blob (type %d) with buffer_length = %d\n", p_tField.type, p_tField.length + 1);
      m_tBindVar.buffer = new char[p_tField.length + 1];
      m_tBindVar.buffer_length = p_tField.length + 1;
      break;
    default:
      v_elog(VL_ERROR, "Unknown type %d\n", p_tField.type);
      setError("Unknown MYSQL data type");
      bRet = false;
      break;
  }

  return bRet;
}

VMysqlCxn *VMysqlCxn::dup()
{
  VMysqlCxn *pRet = new VMysqlCxn();

  return pRet;
}

#endif
