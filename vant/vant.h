/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _VANT_V_H
#define _VANT_V_H

#include <vantages/v_app.h>
#include <vantages/v_config.h>
#include <vantages/v_crypt_mgr.h>
#include <vantages/v_dao.h>
#include <vantages/v_db_cxn_factory.h>
#include <vantages/v_db_cxn.h>
#include <vantages/v_defs.h>
#include <vantages/v_factory.h>
#include <vantages/v_logger.h>
#include <vantages/v_mutex.h>
#include <vantages/v_mutex_hdlr.h>
#include <vantages/v_mysql_cxn.h>
#include <vantages/v_queue.h>
#include <vantages/v_rw_mutex.h>
#include <vantages/v_rw_mutex_hdlr.h>
#include <vantages/v_sqlite_cxn.h>
#include <vantages/v_strtok.h>
#include <vantages/v_task.h>
#include <vantages/v_thrd.h>
#include <vantages/v_thrd_pool.h>

#endif
