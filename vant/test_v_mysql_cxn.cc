#include <stdio.h>
#include <string.h>
#include <limits.h>

#include <string>
#include <sstream>

#include "v_mysql_cxn.h"
#include "v_logger.h"
#include "v_defs.h"

#define TEST_CREATE "create table this_is_a_test (val1 integer, val2 varchar(255), val3 real, val4 int unsigned, val5 bigint unsigned)"
// #define TEST_CREATE "create table this_is_a_test (val1 integer, val2 varchar(255), val3 real, val4 int unsigned, val5 int)"
#define TEST_INSERT "insert into this_is_a_test (val1, val2, val3, val4, val5) values (?, ?, ?, ?, ?)"
#define TEST_SELECT "select val2, val3, val1, val4, val5 from this_is_a_test"
#define TEST_DELETE "delete from this_is_a_test"
#define TEST_DROP "drop table if exists this_is_a_test"

using namespace std;

int main(int argc, char *argv[])
{
  int iRet = 1;

  char sz[1024];
  fgets(sz, 1024, stdin);
  //gets(sz);

  VLogger::getInstance().setLevel(VL_DEBUG);

  string sCxn = "mysql://localhost/test";
  if (argc > 1)
  {
    sCxn = argv[1];
  }
  else {
    int iLen = strnlen(sz, 1024);
    if ('\n' == sz[iLen - 1]) {
      sz[iLen - 1] = '\0';
    }
    sCxn = sz;
  }

  VMysqlCxn oCxn;
  fprintf(stdout, "Setting connect string %s\n", sCxn.c_str());
  oCxn.setConnectString(sCxn);

  fprintf(stdout, "Connecting with %s\n", sCxn.c_str());
  if (!oCxn.connect(__FILE__, __LINE__))
  {
    fprintf(stderr, "Unable to connect to DB.\n");
  }
  else if (!oCxn.prepareQuery(TEST_DROP))
  {
    fprintf(stderr, "Unable to prepare drop query.\n");
  }
  else if (!oCxn.exec())
  {
    fprintf(stderr, "Unable to exec drop query.\n");
  }
  else if (!oCxn.clearQuery())
  {
    fprintf(stderr, "Unable to clear drop query.\n");
  }
  else if (!oCxn.prepareQuery(TEST_CREATE))
  {
    fprintf(stderr, "Unable to prepare create query.\n");
  }
  else if (!oCxn.exec())
  {
    fprintf(stderr, "Unable to exec create query.\n");
  }
  else if (!oCxn.clearQuery())
  {
    fprintf(stderr, "Unable to clear create query.\n");
  }
  else if (!oCxn.prepareQuery(TEST_INSERT))
  {
    fprintf(stderr, "Unable to prepare insert query.\n");
  }
  else if (oCxn.setInt(5, 2))
  {
    fprintf(stderr, "WAS able to set int at invalid column.\n");
  }
  else
  {
    fprintf(stdout, "Connected, prepared drop, executed, cleared, prepared create, executed, cleared, prepared insert...\n");
    int i = 0;
    for (i = 0; i < 10; i++)
    {
      stringstream oSS;
      oSS << "Preamble " << i << " Posamble...";
      string s = oSS.str();
      long lBig = LONG_MAX;
      // long lBig = 2147483648;
      if (!oCxn.setInt(0, i))
      {
        fprintf(stderr, "Unable to set in at position 0\n");
      }
      else if (!oCxn.setStr(1, s))
      {
        fprintf(stderr, "Unable to set str at poisition 1\n");
      }
      else if (!oCxn.setDouble(2, (double) i))
      {
        fprintf(stderr, "Unable t set double at position 2\n");
      }
      else if (!oCxn.setNULL(3))
      {
        fprintf(stderr, "Unable to set NULL at position 3\n");
      }
      else if (!oCxn.setLongInt(4, lBig))
      {
        fprintf(stderr, "Unable to set int (signed in an unsigned row) at poisition 4\n");
      }
      else if (!oCxn.update())
      {
        fprintf(stderr, "Unable to update with insert.\n");
      }
    }

    if (10 != i)
    {
      fprintf(stderr, "Was not able to insert all 10 rows.\n");
    }
    else if (!oCxn.clearQuery())
    {
      fprintf(stderr, "Unable to clear insert query.\n");
    }
    else if (!oCxn.prepareQuery(TEST_SELECT))
    {
      fprintf(stderr, "Unable to prepare select query.\n");
    }
    else if (!oCxn.exec())
    {
      fprintf(stderr, "Unable to exec select query.\n");
    }
    else
    {
      int j = 0;
      while (oCxn.next())
      {
        string s;
        if (oCxn.isNULL(0))
        {
          fprintf(stderr, "Found NULL at column 0 on row %d, but this should not be NULL.\n", j);
          break;
        }
        else if (oCxn.isNULL(1))
        {
          fprintf(stderr, "Found NULL at column 1 on row %d, but this should not be NULL.\n", j);
          break;
        }
        else if (oCxn.isNULL(2))
        {
          fprintf(stderr, "Found NULL at column 2 on row %d, but this should not be NULL.\n", j);
          break;
        }
        else if (!oCxn.isNULL(3))
        {
          fprintf(stderr, "Did NOT find NULL at column 3 on row %d, but this SHOULD be NULL.\n", j);
          break;
        }
        else if (oCxn.isNULL(4))
        {
          fprintf(stderr, "Found NULL at column 4 on row %d, but this should not be NULL.\n", j);
          break;
        }
        else if (!oCxn.getStr(0, s))
        {
          fprintf(stderr, "Unable to get string at position 0\n");
          break;
        }
        else
        {
          float f = oCxn.getDouble(1);
          int iVal = oCxn.getInt(2);
          long lVal = oCxn.getLongInt(4);
          // int lVal = oCxn.getInt(4);

          fprintf(stdout, "Row %d: <'%s', %f, %d, %ld>\n",
          // fprintf(stdout, "Row %d: <'%s', %f, %d, %d>\n",
                  j++,
                  s.c_str(),
                  f,
                  iVal,
                  lVal);
        }
      }

      fgets(sz, 1024, stdin);
      if (10 != j)
      {
        fprintf(stderr, "Did not receive all 10 rows from select.\n");
      }
      else if (!oCxn.clearQuery())
      {
        fprintf(stderr, "Unable to clear select query.\n");
      }
      else if (!oCxn.prepareQuery(TEST_DELETE))
      {
        fprintf(stderr, "Unable to prepare delete query.\n");
      }
      else if (!oCxn.update())
      {
        fprintf(stderr, "Unable to update with delete query.\n");
      }
      else if (!oCxn.clearQuery())
      {
        fprintf(stderr, "Unable to clear delete query.\n");
      }
      else if (!oCxn.prepareQuery(TEST_DROP))
      {
        fprintf(stderr, "Unable to prepare drop query.\n");
      }
      else if (!oCxn.update())
      {
        fprintf(stderr, "Unable to update with drop query.\n");
      }
      else
      {
        fprintf(stdout, ">>>SUCCESS<<<\n");
        iRet = 0;
      }
    }
  }

  if (0 != iRet)
  {
    fprintf(stdout, ">>>FAIL<<<\n");
  }

  return iRet;
}
