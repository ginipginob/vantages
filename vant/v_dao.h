/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _V_DAO_H
#define _V_DAO_H

#include <string>
#include <list>

#include "i_v_dupable.h"
#include "v_db_cxn.h"
#include "v_defs.h"

class VDao 
{
  // Member Variables
  private:
    int m_iID;
    int m_iNsID;
    VDbCxn *m_pCxn;
    std::string m_sConnectString;

  // Methods
  public:
    VDao();
    virtual ~VDao();

    const char *getError();

    int getID();
    void setID(int p_iID);

    int getNsID();
    void setNsID(int p_iNsID);

    virtual bool serialize() = 0;
    virtual bool deserialize() = 0;
    virtual bool deserialize(DaoList_t &p_oOutputList) = 0;

    std::string getConnectString();
    void setConnectString(std::string &p_sConnectString);
    static void clearList(DaoList_t &p_oList);

  protected:

    bool connect(const char *p_szFile = __FILE__, int p_iLine = __LINE__);
    bool disconnect();
    bool clearQuery();
    bool isConnected();
    bool ping();

    bool prepareQuery(const char *p_szQuery);
    bool prepareQuery(std::string &p_sQuery);

    bool setInt(int p_iIndex, int p_iVal);
    bool setLongInt(int p_iIndex, long p_iVal);
    bool setDouble(int p_iIndex, double p_dVal);
    bool setStr(int p_iIndex, std::string &p_sVal);
    bool setStr(int p_iIndex, const char *p_szVal, size_t p_uLen);
    bool setBlob(int p_iIndex, char *p_pBuff, size_t p_uLen);
    bool setNULL(int p_iIndex);

    bool exec();

    bool update();
    bool next();

    int getInt(int p_iCol);
    long getLongInt(int p_iCol);
    double getDouble(int p_iCol);
    bool getStr(int p_iCol, std::string &p_sOutput);
    bool getBlob(int p_iCol, char **p_ppOutput, int &p_iOutLen);
    bool isNULL(int p_iCol);

    long getLastID();
};

#endif
