#include <stdio.h>

#include "v_db_cxn_factory.h"
#include "v_db_cxn.h"

#include "v_mysql_cxn.h"

using namespace std;

VDbCxnFactory VDbCxnFactory::s_oInstance;

VDbCxnFactory::VDbCxnFactory()
{
#ifndef _V_NO_MYSQL
  VDbCxn *pCxn = NULL;

  pCxn = new VMysqlCxn();
  reg(pCxn->cxnName(), *pCxn);
#endif
}

VDbCxnFactory::~VDbCxnFactory()
{
  reset();
}

VDbCxn *VDbCxnFactory::createCxn(std::string &p_sCxnString)
{
  VDbCxn *pRet = NULL;
  size_t uPos = p_sCxnString.find(':');
  if (string::npos != uPos)
  {
    string sName = p_sCxnString.substr(0, uPos);
    pRet = static_cast<VDbCxn *>(create(sName));
  }

  return pRet;
}

VDbCxnFactory &VDbCxnFactory::getInstance()
{
  return s_oInstance;
}

