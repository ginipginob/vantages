/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _V_MYSQL_CXN_H
#define _V_MYSQL_CXN_H

#ifndef _V_NO_MYSQL

#define V_MYSQL_CXN_NAME "mysql"

#include <string>
#include <vector>

#include "v_db_cxn.h"
#include "v_defs.h"

/*
typedef struct MYSQL MYSQL;
typedef struct MYSQL_STMT MYSQL_STMT;
typedef struct MYSQL_BIND MYSQL_BIND;
typedef struct MYSQL_FIELD MYSQL_FIELD;
*/
#include <mysql.h>

class VMysqlCxn : public VDbCxn
{
  // Member Variables
  private:
    MYSQL *m_pCxn;
    MYSQL_STMT *m_pStmt;
    MYSQL_RES *m_pResult;
    MYSQL_BIND *m_pResBindVars;
    int m_iNumResBindVars;
    MYSQL_BIND *m_pParamBindVars;
    int m_iNumParamBindVars;
    bool m_bConnected;
    std::string m_sQuery;
    const char *m_szConnectFile;
    int m_iConnectLine;
    std::string m_sUser;
    std::string m_sPass;
    std::string m_sDB;
    std::string m_sHost;
    int m_iPort;

  // Methods
  public:
    VMysqlCxn();
    virtual ~VMysqlCxn();

    virtual std::string cxnName();

    virtual bool connect(const char *p_szFile = __FILE__, int p_iLine = __LINE__);
    virtual bool disconnect();
    virtual bool clearQuery();
    virtual bool isConnected();
    virtual bool ping();

    virtual bool prepareQuery(const char *p_szQuery);
    virtual bool prepareQuery(std::string &p_sQuery);

    virtual bool setInt(int p_iIndex, int p_iVal);
    virtual bool setLongInt(int p_iIndex, long p_lVal);
    virtual bool setDouble(int p_iIndex, double p_dVal);
    virtual bool setStr(int p_iIndex, std::string &p_sVal);
    virtual bool setStr(int p_iIndex, const char *p_szVal, size_t p_uLen);
    virtual bool setBlob(int p_iIndex, char *p_pBuff, size_t p_uLen);
    virtual bool setNULL(int p_iIndex);

    virtual bool exec();

    virtual bool update();
    virtual bool next();

    virtual int getInt(int p_iCol);
    virtual long getLongInt(int p_iCol);
    virtual double getDouble(int p_iCol);
    virtual bool getStr(int p_iCol, std::string &p_sOutput);
    virtual bool getBlob(int p_iCol, char **p_ppOutput, int &p_iOutLen);
    virtual bool isNULL(int p_iCol);

    virtual long getLastID();

    virtual VMysqlCxn *dup();

  private:
    bool parseCxnString();
    bool initBindVar(MYSQL_FIELD &p_tField, MYSQL_BIND &m_tBindVar);

};

#endif

#endif
