/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>

#include "v_queue.h"
#include "v_task.h"
#include "v_logger.h"
#include "v_mutex_hdlr.h"
#include "v_defs.h"

using namespace std;

VQueue::VQueue()
  : m_iMax(0),
    m_iCurrent(0),
    m_uServiced(0)
{

}

VQueue::~VQueue()
{
  clear();
}

bool VQueue::init(int p_iMax)
{
  clear();
  m_iMax = p_iMax;

  return true;
}

bool VQueue::run()
{
  bool bRet = false;

  while (getRun())
  {
    bRet = true;

    VTask *pTask = dequeue();
    if (NULL == pTask)
    {
      m_oMutex.wait(5000);
    }
    else
    {
      pTask->execute();
      delete pTask;
    }
  }

  return bRet;
}

VQueue *VQueue::dup()
{
  return new VQueue();
}

bool VQueue::clear()
{
    for (VTaskIter_t tIter = m_oQueue.begin();
         m_oQueue.end() != tIter;
         tIter++)
    {
      delete (*tIter);
    }
    m_oQueue.clear();
    m_uServiced = 0;

  return true;
}

bool VQueue::enqueue(VTask &p_oTask)
{
  bool bRet = false;

  while (!bRet)
  //
  // CRITICAL SECTION BEGIN
  //
  {
    VMutexHandler oMH(m_oMutex);

    if (m_iCurrent >= m_iMax)
    {
      v_elog(VL_WARNING, "Queue full %d >= %d, serviced %u, waiting...\n", m_iCurrent, m_iMax, m_uServiced);
      m_oMutex.wait(5000);
    }
    else
    {
      m_oQueue.push_back(&p_oTask);
      m_iCurrent++;
      bRet = true;
    }

    m_oMutex.signal();
  }
  //
  // CRITICAL SECTION END
  //

  return bRet;
}

VTask *VQueue::dequeue()
{
  VTask *pRet = NULL;

  if (m_iCurrent < 0)
  {
    v_elog(VL_CRITICAL, "Queue has less than 0 entries in it: Error!\n");
  }
  else if (0 == m_iCurrent)
  {
    v_elog(VL_DEBUG, "Queue is empty.\n");
  }
  else
  //
  // BEGIN CRITICAL SECTION
  //
  {
    VMutexHandler oMH(m_oMutex);

    pRet = m_oQueue.front();
    m_oQueue.pop_front();
    m_iCurrent--;
    m_uServiced++;
    m_oMutex.signal();
  }
  //
  // CRITICAL SECTION END
  //

  return pRet;
}

VMutex &VQueue::getMutex()
{
  return m_oMutex;
}

int VQueue::getLength()
{
  return m_iCurrent;
}

int VQueue::getMaxLength()
{
  return m_iMax;
}

