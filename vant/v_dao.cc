/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <string.h>

#include "config.h"

#include "v_dao.h"
#include "v_db_cxn_factory.h"
#include "v_logger.h"
#include "v_defs.h"

using namespace std;

VDao::VDao()
  : m_iID(-1),
    m_iNsID(-1),
    m_pCxn(NULL)
{

}

VDao::~VDao()
{
  if (NULL != m_pCxn)
  {
    delete m_pCxn;
  }
}

int VDao::getID()
{
  return m_iID;
}

void VDao::setID(int p_iID)
{
  m_iID = p_iID;
}

int VDao::getNsID()
{
  return m_iNsID;
}

void VDao::setNsID(int p_iNsID)
{
  m_iNsID = p_iNsID;
}

const char *VDao::getError()
{
  const char *szRet = "NO ERROR";
  if (NULL != m_pCxn)
  {
    szRet = m_pCxn->getError();
  }
  return szRet;
}

std::string VDao::getConnectString()
{
  return m_sConnectString;
}

void VDao::setConnectString(std::string &p_sConnectString)
{
  m_sConnectString = p_sConnectString;
  if (NULL != m_pCxn)
  {
    delete m_pCxn;
    m_pCxn = NULL;
  }
  m_pCxn = VDbCxnFactory::getInstance().createCxn(m_sConnectString);
  if (NULL != m_pCxn)
  {
    m_pCxn->setConnectString(m_sConnectString);
  }
}

bool VDao::connect(const char *p_szFile, int p_iLine)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to connect without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->connect(p_szFile, p_iLine);
  }

  return bRet;
}

bool VDao::disconnect()
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to disconnect without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->disconnect();
  }

  return bRet;
}

bool VDao::isConnected()
{
  bool bRet = false;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "No connection set.\n");
  }
  else
  {
    bRet = m_pCxn->isConnected();
  }

  return bRet;
}

bool VDao::ping()
{
  bool bRet = false;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "No connection set, can't ping.\n");
  }
  else
  {
    bRet = m_pCxn->ping();
  }

  return bRet;
}

bool VDao::clearQuery()
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to clearQuery() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->clearQuery();
  }

  return bRet;
}

bool VDao::prepareQuery(std::string &p_sQuery)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to prepareQuery() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->prepareQuery(p_sQuery);
  }

  return bRet;
}

bool VDao::prepareQuery(const char *p_szQuery)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to prepareQuery() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->prepareQuery(p_szQuery);
  }

  return bRet;
}

bool VDao::setInt(int p_iIndex, int p_iVal)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to setInt() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->setInt(p_iIndex, p_iVal);
  }

  return bRet;
}

bool VDao::setLongInt(int p_iIndex, long p_lVal)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to setInt() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->setLongInt(p_iIndex, p_lVal);
  }

  return bRet;
}

bool VDao::setDouble(int p_iIndex, double p_dVal)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to setDouble() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->setDouble(p_iIndex, p_dVal);
  }

  return bRet;
}

bool VDao::setStr(int p_iIndex, std::string &p_sVal)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to setStr() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->setStr(p_iIndex, p_sVal);
  }

  return bRet;
}

bool VDao::setStr(int p_iIndex, const char *p_szVal, size_t p_uLen)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to setStr() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->setStr(p_iIndex, p_szVal, p_uLen);
  }

  return bRet;
}

bool VDao::setBlob(int p_iIndex, char *p_pBuff, size_t p_uLen)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to setBlob() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->setBlob(p_iIndex, p_pBuff, p_uLen);
  }

  return bRet;
}

bool VDao::setNULL(int p_iIndex)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to setNULL() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->setNULL(p_iIndex);
  }

  return bRet;
}

bool VDao::exec()
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to exec() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->exec();
  }

  return bRet;
}

bool VDao::update()
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to update() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->update();
  }

  return bRet;
}

bool VDao::next()
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to next() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->next();
  }

  return bRet;
}

int VDao::getInt(int p_iCol)
{
  int iRet = 0;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to getInt() without connection object.\n");
  }
  else
  {
    iRet = m_pCxn->getInt(p_iCol);
  }

  return iRet;
}

long VDao::getLongInt(int p_iCol)
{
  long lRet = 0;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to getInt() without connection object.\n");
  }
  else
  {
    lRet = m_pCxn->getLongInt(p_iCol);
  }

  return lRet;
}

double VDao::getDouble(int p_iCol)
{
  double dRet = 0.0;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to getDouble() without connection object.\n");
  }
  else
  {
    dRet = m_pCxn->getDouble(p_iCol);
  }

  return dRet;
}

bool VDao::getStr(int p_iCol, std::string &p_sOutput)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to getStr() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->getStr(p_iCol, p_sOutput);
  }

  return bRet;
}

bool VDao::getBlob(int p_iCol, char **p_ppOutput, int &p_iOutLen)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to getBlob() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->getBlob(p_iCol, p_ppOutput, p_iOutLen);
  }

  return bRet;
}

bool VDao::isNULL(int p_iCol)
{
  bool bRet = true;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to isNULL() without connection object.\n");
  }
  else
  {
    bRet = m_pCxn->isNULL(p_iCol);
  }

  return bRet;
}

long VDao::getLastID()
{
  long iRet = 0;

  if (NULL == m_pCxn)
  {
    v_elog(VL_ERROR, "Unable to getLastID() without connection object.\n");
  }
  else
  {
    iRet = m_pCxn->getLastID();
  }

  return iRet;
}

void VDao::clearList(DaoList_t &p_oList)
{
  for (DaoIter_t tIter = p_oList.begin();
       p_oList.end() != tIter;
       tIter++)
  {
    delete *tIter;
  }

  p_oList.clear();
}

