/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include "v_logger.h"
#include <stdio.h>
#include <fcntl.h>
#include <stdarg.h>
#include <syslog.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

#include <iostream>
#include <sstream>

#include "v_logger.h"
#include "v_defs.h"

using namespace std;

VLogger VLogger::s_oInstance;

VLogger::VLogger()
  : m_iLevel(VL_CRITICAL),
    m_pFile(stdout),
    m_pLevelNames(NULL)
{
  m_pLevelNames = new string[VL_COUNT];
  m_pLevelNames[V_LOG_NONE] = string("NONE");
  m_pLevelNames[V_LOG_CRITICAL] = string("CRITICAL");
  m_pLevelNames[V_LOG_ERROR] = string("ERROR");
  m_pLevelNames[V_LOG_WARNING] = string("WARNING");
  m_pLevelNames[V_LOG_INFO] = string("INFO");
  m_pLevelNames[V_LOG_DEBUG] = string("DEBUG");
  m_pLevelNames[VL_CRITICAL] = string("L_CRITICAL");
  m_pLevelNames[VL_ERROR] = string("L_ERROR");
  m_pLevelNames[VL_WARNING] = string("L_WARNING");
  m_pLevelNames[VL_INFO] = string("L_INFO");
  m_pLevelNames[VL_DEBUG] = string("L_DEBUG");

  for (int i = 0; i < VL_COUNT; i++)
  {
    m_oNameMap[(m_pLevelNames[i])] = i;
  }

  setlogmask(LOG_UPTO(LOG_NOTICE));
  openlog("vantaged", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
}

VLogger::~VLogger()
{
  close();
  closelog();

  delete[] m_pLevelNames;
}

void VLogger::close()
{
  if (NULL != m_pFile && stdout != m_pFile)
  {
    fclose(m_pFile);
    m_pFile = NULL;
  }
}

int VLogger::getLevel()
{
  return m_iLevel;
}

void VLogger::setLevel(int p_iLevel)
{
  m_iLevel = p_iLevel;
}

const std::string &VLogger::getFileName()
{
  return m_sFile;
}

bool VLogger::setFileName(const char *p_szFile)
{
  bool bRet = false;

  if (NULL != p_szFile && '\0' != p_szFile[0])
  {
    if (NULL != m_pFile && stdout != m_pFile)
    {
      fclose(m_pFile);
      m_pFile = NULL;
    }

    m_sFile = p_szFile;

    m_pFile = fopen(m_sFile.c_str(), "w");
    if (NULL == m_pFile)
    {
#ifdef _V_DEBUG
      fprintf(stderr, "%s [%d]: Unable to open file '%s': %s\n",
              __FILE__,
              __LINE__,
              m_sFile.c_str(),
              strerror(errno));
#endif
      m_pFile = stdout;
      m_sFile = "";
    }
    else
    {
      bRet = true;
    }
  }

  return bRet;
}

inline FILE *VLogger::getFile()
{
  return m_pFile;
}

inline std::string VLogger::levelToStr(int p_iLevel)
{
  string sRet;
  if (p_iLevel <= VL_DEBUG)
  {
    sRet = m_pLevelNames[p_iLevel];
  }

  return sRet;
}

int VLogger::strToLevel(std::string &p_sLevel)
{
  int iRet = -1;
  LogLevelIter_t tIter = m_oNameMap.find(p_sLevel);
  if (m_oNameMap.end() != tIter)
  {
    iRet = tIter->second;
  }

  return iRet;
}

VLogger &VLogger::getInstance()
{
  return s_oInstance;
}

void VLogger::log(const char *p_szFile, int p_iLine, int p_iLevel, const char * p_szFmt, ...)
{
  va_list tBlah;
  va_start(tBlah, p_szFmt);

  VLogger &oLogger = getInstance();
  int iLevel = oLogger.getLevel();
  FILE *pFile = oLogger.getFile();

  if (p_iLevel <= iLevel && NULL != pFile)
  {
    time_t tNow = time(NULL);
    ostringstream oSS;
    oSS << (int) tNow
         << " "
         << p_szFile
         << " ["
         << p_iLine
         << "]: "
         << oLogger.levelToStr(p_iLevel)
         << ": ";
    fprintf(pFile, "%s", oSS.str().c_str());
    vfprintf(pFile, p_szFmt, tBlah);
fflush(pFile);
  }
}

void VLogger::sysLog(const char *p_szFile, int p_iLine, const char * p_szFmt, ...)
{
  va_list tBlah;
  va_start(tBlah, p_szFmt);

  ostringstream oSS;
  oSS << p_szFile
      << " ["
      << p_iLine
      << "]: "
      << p_szFmt;
  vsyslog(LOG_NOTICE, oSS.str().c_str(), tBlah);
}

