/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _V_DB_CXN_H
#define _V_DB_CXN_H

#include <string>
#include <list>

#include "i_v_dupable.h"
#include "v_mutex.h"
#include "v_rw_mutex.h"
#include "v_defs.h"

class VDbCxn : public IVDupable
{
  // Member Variables
  private:
    std::string m_sQuery;
    std::string m_sError;

    std::string m_sConnectString;

  // Methods
  public:
    VDbCxn();
    virtual ~VDbCxn();

    const char *getError();
    void setError(const char *p_szError);

    virtual std::string cxnName() = 0;

    std::string getConnectString();
    void setConnectString(std::string &p_sConnectString);

    virtual bool connect(const char *p_szFile = __FILE__, int p_iLine = __LINE__) = 0;
    virtual bool disconnect() = 0;
    virtual bool clearQuery() = 0;
    virtual bool isConnected() = 0;
    virtual bool ping() = 0;

    virtual bool prepareQuery(const char *p_szQuery) = 0;
    virtual bool prepareQuery(std::string &p_sQuery) = 0;

    virtual bool setInt(int p_iIndex, int p_iVal) = 0;
    virtual bool setLongInt(int p_iIndex, long p_iVal) = 0;
    virtual bool setDouble(int p_iIndex, double p_dVal) = 0;
    virtual bool setStr(int p_iIndex, std::string &p_sVal) = 0;
    virtual bool setStr(int p_iIndex, const char *p_szVal, size_t p_uLen) = 0;
    virtual bool setBlob(int p_iIndex, char *p_pBuff, size_t p_uLen) = 0;
    virtual bool setNULL(int p_iIndex) = 0;

    virtual bool exec() = 0;

    virtual bool update() = 0;
    virtual bool next() = 0;

    virtual int getInt(int p_iCol) = 0;
    virtual long getLongInt(int p_iCol) = 0;
    virtual double getDouble(int p_iCol) = 0;
    virtual bool getStr(int p_iCol, std::string &p_sOutput) = 0;
    virtual bool getBlob(int p_iCol, char **p_ppOutput, int &p_iOutLen) = 0;
    virtual bool isNULL(int p_iCol) = 0;

    virtual long getLastID() = 0;

};

#endif
