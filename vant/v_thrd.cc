/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "v_thrd.h"
#include "v_thrd_pool.h"
#include "v_task.h"
#include "v_logger.h"
#include "v_mutex_hdlr.h"
#include "v_defs.h"

using namespace std;

void *_run(void *p_pParam)
{
  if (NULL == p_pParam)
  {
    v_elog(VL_CRITICAL, "Callback got NULL paramerer.\n");
  }
  else
  {
    ((VThread *) p_pParam)->run();
    v_elog(VL_CRITICAL, "Thread done.\n");
  }

  return NULL;
}

VThread::VThread(VThreadPool *p_pPool /*= NULL*/)
  : m_bRun(false),
    m_pTask(NULL),
    m_pPool(p_pPool)
{

}

VThread::~VThread()
{

}

bool VThread::start()
{
  bool bRet = false;

  int iErr = 0;
  if (m_bRun)
  {
    v_elog(VL_CRITICAL, "Thread already running.\n");
  }
  else
  {
    m_bRun = true;
    if (0 != (iErr = pthread_create(&m_tID, NULL, _run, this)))
    {
      v_elog(VL_CRITICAL, "Unable to start thread: %s\n", strerror(iErr));
    }
    else
    {
      v_elog(VL_INFO, "STARTED!\n");
      bRet = true;
    }
  }

  return bRet;
}

bool VThread::run()
{
  bool bRet = false;

  while (m_bRun)
  {
    bRet = true;

    VTask *pTask = getTask();

    if (NULL == pTask)
    //
    // BEGIN CRITICAL SECTION
    //
    {
      VMutexHandler oMH(m_oMutex);

      m_oMutex.wait(1000);
    }
    //
    // END CRITICAL SECTION
    //
    else
    {
      v_elog(VL_CRITICAL, "Executing tasn...\n");
      if (!pTask->execute())
      {
        v_elog(VL_CRITICAL, "Unable to execute task.\n");
      }

      setTask(NULL);

      if (NULL != m_pPool && !m_pPool->checkIn(*this))
      {
        v_elog(VL_CRITICAL, "Unable to check this thread in\n");
      }
    }
  }

  return bRet;
}

bool VThread::kill()
{
  m_bRun = false;

  //
  // BEGIN CRITICAL SECTION
  //
  {
    VMutexHandler oMh(m_oMutex);
    m_oMutex.signalAll();
  }
  //
  // END CRITICAL SECTION
  //

  return !m_bRun;
}

bool VThread::join()
{
  int iErr = pthread_join(m_tID, NULL);
  if (0 != iErr)
  {
    v_elog(VL_CRITICAL, "Unable to join thread: %s\n", strerror(iErr));
  }

  return (0 == iErr);
}

bool VThread::getRun()
{
  return m_bRun;
}

VTask *VThread::getTask()
{
  return m_pTask;
}

void VThread::setTask(VTask *p_pTask)
{
  // If this is a thread in a pool
  // Assume that the task is not being
  // managed elsewhere and delete it here
  // Otherwise, the use may be managing
  // the task's memory and must be sure to
  // delete it.
  if (NULL != m_pTask && NULL != m_pPool)
  {
    delete m_pTask;
  }

  m_pTask = p_pTask;

  /*
  //
  // BEGIN CRITICAL SECTION
  //
  if (NULL != m_pTask)
  {
    VMutexHandler oMh(m_oMutex);
    m_oMutex.signalAll();
  }
  //
  // END CRITICAL SECTION
  //
  */
}

VThreadPool *VThread::getPool()
{
  return m_pPool;
}

void VThread::setPool(VThreadPool *p_pPool)
{
  m_pPool = p_pPool;
}

VThread *VThread::dup()
{
  return new VThread(m_pPool);
}
