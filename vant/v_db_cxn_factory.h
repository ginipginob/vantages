#ifndef _V_DB_CXN_FACTORY_H
#define _V_DB_CXN_FACTORY_H

#include <string>

#include "v_factory.h"

class VDbCxn;

class VDbCxnFactory : public VFactory<std::string>
{
  // Member Variables
  private:
    static VDbCxnFactory s_oInstance;

  // Methods
  public:
    VDbCxnFactory();
    virtual ~VDbCxnFactory();

    VDbCxn *createCxn(std::string &p_sCxnString);

    static VDbCxnFactory &getInstance();
};

#endif
