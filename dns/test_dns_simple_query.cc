#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>

#include <iostream>
#include <string>

#include "dns_rrset.h"
#include "dns_a.h"
#include "dns_soa.h"
#include "dns_rrsig.h"
#include "dns_name.h"
#include "dns_err.h"
#include "dns_rr_fact.h"
#include "dns_resolver.h"
#include "dns_packet.h"

using namespace std;

int main(int argc, char *argv[])
{
  int iRet = 1;

  string sName = "verisignlabs.com.";
  string sType = "A";

  if (argc > 1)
  {
    sName = argv[1];
  }

  if (argc > 2)
  {
    sType = argv[2];
  }

  DnsResolver oRes;
  DnsPacket oResp;

  if (argc > 3)
  {
    struct in_addr tAddr;
    memset(&tAddr, 0, sizeof(tAddr));
    if (1 != inet_pton(AF_INET, argv[3], &tAddr))
    {
      fprintf(stderr, "Unable to convert '%s' to IP, using local resover: %s\n", argv[3], strerror(errno));
    }
    else
    {
      tAddr.s_addr = ntohl(tAddr.s_addr);
      oRes.setNameserver((uint32_t) tAddr.s_addr);
    }
  }

  int iType = DnsRrFactory::getInstance().getTypeNum(sType);

  oRes.setDO(true);

  if (!oRes.send(sName, iType, oResp))
  {
    fprintf(stderr, "Unable to send message type '%s' [%d] to name '%s': %s\n",
                    sType.c_str(),
                    iType,
                    sName.c_str(),
                    DnsError::getInstance().getError().c_str());
  }
  else
  {
    oResp.print();

    DnsRRset oSet;
    DnsRRset oSecondarySet;
    if (!oResp.getAnswers(oSet)) {
      fprintf(stderr, "Unable to get answer set: %s\n", DnsError::getInstance().getError().c_str());
    }
    else if (oResp.getHeader().ns_count() > 0 && !oResp.getAuthority(oSecondarySet)) {
      fprintf(stderr, "Unable to get authority set: %s\n", DnsError::getInstance().getError().c_str());
    }
    else if (oResp.getHeader().ar_count() > 0 && !oResp.getAdditional(oSecondarySet)) {
      fprintf(stderr, "Unable to get additional set: %s\n", DnsError::getInstance().getError().c_str());
    }
    else {
      fprintf(stdout, "Got response type %d\n", oSet.getType());
    }

    fprintf(stdout, ">>>SUCCESS<<<\n");
  }

  return iRet;
}
