#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <string>

#include "dns_resolver.h"
#include "dns_task.h"
#include "dns_packet.h"
#include "dns_rr.h"
#include "dns_name.h"
#include "dns_opt.h"
#include "dns_err.h"
#include "dns_defs.h"

using namespace std;

int main(int argc, char *argv[])
{
  int iRet = 1;

  const char *szDomain = "secspider.cs.ucla.edu";

  if (argc > 1)
  {
    szDomain = argv[1];
    if (0 == strcmp(szDomain, "-h"))
    {
      printf("%s <domain> | -h\n", argv[0]);
      exit(0);
    }
  }

  DnsTask oTask;
  string sDomain(szDomain);
  DnsName oName(sDomain);
  DnsRR *pQuestionRR = DnsRR::question(oName, DNS_RR_DS);
  if (NULL == pQuestionRR)
  {
    fprintf(stderr, "Unable to crete question for type: %d\n", DNS_RR_DS);
  }
  else
  {
    DnsPacket *pPkt = new DnsPacket(true, -1);
    pQuestionRR->set_class(DNS_CLASS_IN);
    pPkt->addQuestion(*pQuestionRR);
    pPkt->getHeader().set_cd(true);
    oTask.setQuery(pPkt);
    oTask.setResponse(NULL);

    DnsResolver oRes;

    fprintf(stdout, "Sending UDP message...\n");

    if (!oRes.send(&oTask))
    {
      fprintf(stderr, "Unable to send task.\n");
    }
    else
    {
      DnsTask *pResp = NULL;
      while (NULL == pResp)
      {
        sleep(1);
        pResp = oRes.recv();

        DnsPacket *pRespPkt = NULL;
        if (NULL != pResp)
        {
          pRespPkt = pResp->getResponse();
          if (NULL != pRespPkt)
          {
            fprintf(stdout, "Got response:\n");
            pRespPkt->print();
          }
        }
      }
    }

    fprintf(stdout, "Sending TCP message...\n");

    oTask.setResponse(NULL);
    oTask.useTcp(true);
    if (!oRes.send(&oTask))
    {
      fprintf(stderr, "Unable to send task: %s\n", DnsError::getInstance().getError().c_str());
    }
    else
    {
      DnsTask *pResp = NULL;
      while (NULL == pResp)
      {
        sleep(1);
        pResp = oRes.recv();

        DnsPacket *pRespPkt = NULL;
        if (NULL != pResp)
        {
          pRespPkt = pResp->getResponse();
          if (NULL != pRespPkt)
          {
            fprintf(stdout, "Got response:\n");
            pRespPkt->print();
          }
        }
      }
    }

    fprintf(stdout, "Sending TCP message for DNSKEY...\n");

    pPkt = new DnsPacket(true);
    pQuestionRR = DnsRR::question(oName, DNS_RR_DNSKEY);
    pQuestionRR->set_class(DNS_CLASS_IN);
    pPkt->addQuestion(*pQuestionRR);
    oTask.setQuery(pPkt);
    oTask.setResponse(NULL);
    oTask.useTcp(true);
    oTask.setResponse(NULL);
    DnsOpt *pOpt = new DnsOpt(4096, true);
    oTask.setOpt(pOpt);

    if (!oRes.send(&oTask))
    {
      fprintf(stderr, "Unable to send task: %s\n", DnsError::getInstance().getError().c_str());
    }
    else
    {
      DnsTask *pResp = NULL;
      while (NULL == pResp)
      {
        sleep(1);
        pResp = oRes.recv();

        DnsPacket *pRespPkt = NULL;
        if (NULL != pResp)
        {
          pRespPkt = pResp->getResponse();
          if (NULL != pRespPkt)
          {
            fprintf(stdout, "Got response:\n");
            pRespPkt->print();
          }
        }
      }
    }

    iRet = 0;
  }

  return iRet;
}
