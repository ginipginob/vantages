#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>

#include "dns_resolver.h"
#include "dns_rr.h"
#include "dns_packet.h"
#include "dns_name.h"
#include "dns_err.h"
#include "dns_rrset.h"

using namespace std;

int main(int argc, char *argv[])
{
  int iRet = 1;

  int iType = DNS_RR_NS;
  const char *szName = "nanog.org.";
  if (argc > 1)
  {
    szName = argv[1];
  }
  if (argc > 2)
  {
    int iTmp = atoi(argv[2]);
    if (iTmp > 0)
    {
      iType = iTmp;
    }
  }

  DnsResolver oRes;
  DnsPacket oQuery(true);
  DnsName oName(szName);
  DnsRR *pRR = DnsRR::question(oName, iType);
  oQuery.addQuestion(*pRR);
  DnsPacket oResp;
  if (!oRes.send(oQuery, oResp))
  {
    cerr << "Unable to get respons from '" << szName << "': " << DnsError::getInstance().getError() << "\n";
    oQuery.print();
  }
  else
  {
    DnsPacket oDeser;
    int iPktLen = 0;
    u_char pBuff[DNS_UDP_MAX_PACKET_SIZE];
    memset(pBuff, 0, DNS_UDP_MAX_PACKET_SIZE);
    if (0 >= (iPktLen = oResp.toWire(pBuff, DNS_UDP_MAX_PACKET_SIZE)))
    {
      cerr << "Couldn't convert packet to wire: " << DnsError::getInstance().getError() << "\n";
    }
    else if (!oDeser.fromWire(pBuff, iPktLen))
    {
      cerr << "Unable to reconvert packet fromWire(): " << DnsError::getInstance().getError() << "\n";
    }
    else
    {
        cout << "Original:\n";
        oResp.print();
        cout << "\nRebuilt:\n";
        oDeser.print();
        iRet = 0;
    }
  }

  return iRet;
}
