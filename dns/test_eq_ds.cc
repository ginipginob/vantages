#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>

#include "dns_resolver.h"
#include "dns_rr.h"
#include "dns_packet.h"
#include "dns_name.h"
#include "dns_err.h"
#include "dns_rrset.h"
#include "dns_ds.h"
#include "dns_cds.h"

using namespace std;

int main(int argc, char *argv[])
{
  int iRet = 1;

  int iType = DNS_RR_DS;
  const char *szName = "_443._tcp.isc.org.";
  if (argc > 1)
  {
    szName = argv[1];
  }

  DnsResolver oRes;
  DnsPacket oQuery(true);
  DnsName oName(szName);
  DnsRR *pRR = DnsRR::question(oName, iType);
  oQuery.addQuestion(*pRR);
  DnsPacket oResp;
  if (!oRes.send(oQuery, oResp))
  {
    cerr << "Unable to get respons from '" << szName << "': " << DnsError::getInstance().getError() << "\n";
    oQuery.print();
  }
  else
  {
    bool bGood = true;
    DnsPacket oDeser;
    int iPktLen = 0;
    u_char pBuff[DNS_TCP_MAX_PACKET_SIZE];
    memset(pBuff, 0, DNS_TCP_MAX_PACKET_SIZE);
    if (0 >= (iPktLen = oResp.toWire(pBuff, DNS_TCP_MAX_PACKET_SIZE)))
    {
      cerr << "Couldn't convert packet to wire: " << DnsError::getInstance().getError() << "\n";
    }
    else if (!oDeser.fromWire(pBuff, iPktLen))
    {
      cerr << "Unable to reconvert packet fromWire(): " << DnsError::getInstance().getError() << "\n";
    }
    else
    {
      DnsRRset oSet;
      if (!oResp.getAnswers(oSet))
      {
        cerr << "Unable to get answer set from response: " << DnsError::getInstance().getError() << "\n";
      }
      else
      {
        for (RRIter_t tIter = oSet.beginData();
             bGood && oSet.endData() != tIter;
             tIter++)
        {
          // DnsTlsa *pRR = dynamic_cast<DnsTlsa *>(*tIter);
          DnsRR *pRR = *tIter;

          DnsDs oRR;
          string sRR;
          sRR = pRR->toString();
          if (sRR == "")
          {
            cerr << "Unable to get string from answer: " << DnsError::getInstance().getError() << "\n";
            bGood = false;
          }
          else if (!oRR.fromString(sRR))
          {
            cerr << "Unable to conver string " << sRR << " to set: " << DnsError::getInstance().getError() << "\n";
            bGood = false;
          }
          else if (!(*pRR == oRR))
          {
            cerr << "Same DS is not equal??  '" << oRR.toString() << "' ---- '" << pRR->toString() << "'\n";
            bGood = false;
          }
          else
          {
            cout << "Converted to '" << sRR << "' and back again.\n";

            DnsDs oRR2;
            oRR2.setKeyTag(oRR.getKeyTag());
            oRR2.setAlgo(oRR.getAlgo());
            oRR2.setDigType(oRR.getDigType());
            oRR2.setDig(oRR.getDig());
            oRR2.setBinDig(oRR.getBinDig(), oRR.getBinDigLen());

            if (!(oRR == oRR2)) {
              cerr << "Built from components DS is not equal??  " << oRR.toString() << " ---- " << pRR->toString() << "\n";
              bGood = false;
            }
          }
        }
      }

      if (bGood)
      {
        cout << "Original:\n";
        oResp.print();
        cout << "\nRebuilt:\n";
        oDeser.print();
        cout << ">>>SUCCESS<<<\n";
        iRet = 0;
      }
    }
  }

  return iRet;
}
