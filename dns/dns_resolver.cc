/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <fcntl.h>
#include <poll.h>
#include <stdlib.h>
#include <sys/select.h>
#include <unistd.h>

#include <algorithm>
#include <iostream>
#include <sstream>

#include "dns_resolver.h"
#include "dns_packet.h"
#include "dns_rr.h"
#include "dns_opt.h"
#include "dns_name.h"
#include "dns_task.h"
#include "dns_tsig.h"
#include "dns_err.h"
#include "dns_task.h"

using namespace std;

typedef map<int, DnsTask *> FdMap_t;
typedef FdMap_t::iterator FdIter_t;

struct sockaddr_in DnsResolver::s_tDefaultResolverIP = {0};

DnsResolver::DnsResolver()
  : m_iSocket(-1),
    m_iRetries(3),
    m_iTimeout(10),
    m_uBuffSize(4096),
    m_bDO(false),
    m_pTsig(NULL),
    m_eTcp(DNS_RES_FALLBACK),
    m_iConc(10),
    m_iTcpConc(10)
{
  if (0 == s_tDefaultResolverIP.sin_addr.s_addr)
  {
    if (!init())
    {
      DnsError::getInstance().setError("Unable to initialize resolver.");
      dns_log("Unable to initialize resolver.\n");
    }
    srand(time(NULL));
  }

  if (-1 == (m_iSocket = socket(AF_INET, SOCK_DGRAM, 0)))
  {
    dns_log("Unable to create socket: %s\n", strerror(errno));
    DnsError::getInstance().setError("Unable to create socket");
  }
  else if (!setNonblocking(m_iSocket))
  {
    dns_log("Unable to make socket non-blaocking: %s\n", strerror(errno));
    DnsError::getInstance().setError("Unable to make socket non-blocking");
  }
  memcpy(&m_tRemoteAddr, &s_tDefaultResolverIP, sizeof(m_tRemoteAddr));
}

DnsResolver::~DnsResolver()
{
  if (-1 != m_iSocket)
  {
    close(m_iSocket);
    m_iSocket = -1;
  }

  clearTsig();
}

uint32_t DnsResolver::getNameserver()
{
  return ntohl(m_tRemoteAddr.sin_addr.s_addr);
}

void DnsResolver::setNameserver(uint32_t p_uIP)
{
  m_tRemoteAddr.sin_addr.s_addr = htonl(p_uIP);
}

uint16_t DnsResolver::getPort()
{
  return ntohs(m_tRemoteAddr.sin_port);
}

void DnsResolver::setPort(uint16_t p_uPort)
{
  m_tRemoteAddr.sin_port = htons(p_uPort);
}

int DnsResolver::getRetries()
{
  return m_iRetries;
}

void DnsResolver::setRetries(int p_iRetries)
{
  m_iRetries = p_iRetries;
}

int DnsResolver::getTimeout()
{
  return m_iTimeout;
}

void DnsResolver::setTimeout(int p_iSeconds)
{
  m_iTimeout = p_iSeconds;
}

uint16_t DnsResolver::getBuffSize()
{
  return m_uBuffSize;
}

void DnsResolver::setBuffSize(uint16_t p_uBuffSize)
{
  m_uBuffSize = p_uBuffSize;
}

bool DnsResolver::getDO()
{
  return m_bDO;
}

void DnsResolver::setDO(bool p_bDO)
{
  m_bDO = p_bDO;
}

dns_res_tcp_e DnsResolver::getTcp()
{
  return m_eTcp;
}

void DnsResolver::setTcp(dns_res_tcp_e p_eTcp)
{
  m_eTcp = p_eTcp;
}

bool DnsResolver::usingTsig()
{
  return (NULL != m_pTsig);
}

bool DnsResolver::setTsig(char *p_szKeyFile)
{
  return setTsig((const char *) p_szKeyFile);
}

bool DnsResolver::setTsig(const char *p_kszKeyFile)
{
  bool bRet = false;

  if (NULL == p_kszKeyFile)
  {
    clearTsig();
  }
  else
  {
    string sFile = p_kszKeyFile;
    bRet = setTsig(sFile);
  }

  return bRet;
}

bool DnsResolver::setTsig(std::string &p_sKeyFile)
{
  bool bRet = false;

  DnsTsig *pTsig = new DnsTsig;
  if (!pTsig->loadKeyFromFile(p_sKeyFile))
  {
    dns_log("Unable to set TSIG in resolver because could not load key from file.\n");
    delete pTsig;
  }
  else
  {
    if (NULL != m_pTsig)
    {
      delete m_pTsig;
      m_pTsig = NULL;
    }

    m_pTsig = pTsig;
    bRet = true;
  }

  return bRet;
}

bool DnsResolver::setTsig(std::string &p_sKeyName, u_char *p_pKey, size_t p_uKeyLen)
{
  bool bRet = false;

  if (NULL == p_pKey)
  {
    dns_log("Unable to set NULL key.\n");
  }
  else if (0 == p_uKeyLen)
  {
    dns_log("Unable to set key with 0 len\n");
  }
  else
  {
    DnsTsig *pTsig = new DnsTsig();
    DnsName oName(p_sKeyName);
    pTsig->set_name(oName);
    pTsig->setKey(p_pKey, p_uKeyLen);

    if (NULL != m_pTsig)
    {
      delete m_pTsig;
      m_pTsig = NULL;
    }

    m_pTsig = pTsig;
    bRet = true;
  }

  return bRet;
}

void DnsResolver::clearTsig()
{
  if (NULL != m_pTsig)
  {
    delete m_pTsig;
    m_pTsig = NULL;
  }
}

bool DnsResolver::send(std::string &p_sName, DnsPacket &p_oAnswer)
{
  return send(p_sName, DNS_RR_A, p_oAnswer);
}

bool DnsResolver::send(std::string &p_sName, int p_iType, DnsPacket &p_oAnswer)
{
  return send(p_sName, p_iType, DNS_CLASS_IN, p_oAnswer);
}

bool DnsResolver::send(std::string &p_sName, int p_iType, int p_iClass, DnsPacket &p_oAnswer)
{
  bool bRet = false;

  DnsPacket oQuestionPkt(true, -1);
  DnsName oName(p_sName);
  DnsRR *pQuestionRR = DnsRR::question(oName, p_iType);
  if (NULL == pQuestionRR)
  {
    dns_log("Unable to create question for name: '%s' of type: %d\n", p_sName.c_str(), p_iType);
    DnsError::getInstance().setError("Unable to create question for name");
  }
  else if (getBuffSize() > DNS_QUERY_MAX_PACKET_SIZE)
  {
    dns_log("Unable to send packet size %u (larger that max size: %u)\n", getBuffSize(), DNS_QUERY_MAX_PACKET_SIZE);
    DnsError::getInstance().setError("Unable to send packet size ");
  }
  else
  {
    pQuestionRR->set_class(p_iClass);
    oQuestionPkt.addQuestion(*pQuestionRR);

    bRet = send(oQuestionPkt, p_oAnswer);
  }

  return bRet;
}

bool DnsResolver::send(DnsPacket &p_oQueryPkt, DnsPacket &p_oAnswer)
{
  bool bRet = false;

  bool bTsig = false;

  if (usingTsig() && NULL == p_oQueryPkt.getTsig())
  {
    DnsTsig *pTsig = new DnsTsig(*m_pTsig);
    p_oQueryPkt.setTsig(pTsig);
    bTsig = true;
  }

  if (getBuffSize() > 512 || getDO() || bTsig)
  {
    DnsOpt *pOptRR = new DnsOpt();
    pOptRR->setMax(getBuffSize());
    pOptRR->setDO(getDO());
    p_oQueryPkt.addAdditional(*pOptRR);
  }

  int iLen = 0;
  size_t uQuestionLen = 0;
  u_char pFullBuff[DNS_TCP_MAX_PACKET_SIZE + 2];
  memset(pFullBuff, 0, DNS_TCP_MAX_PACKET_SIZE + 2);

  u_char pQuestionBuff[DNS_QUERY_MAX_PACKET_SIZE + 2];
  u_char *pBuff = pQuestionBuff + 2;
  memset(pQuestionBuff, 0, DNS_QUERY_MAX_PACKET_SIZE + 2);


  struct sockaddr_in tRespAddr;
  socklen_t tRespLen = sizeof(tRespAddr);
  memset(&tRespAddr, 0, sizeof(tRespAddr));
  p_oAnswer.clear();

  if (0 == (uQuestionLen = p_oQueryPkt.toWire(pBuff, DNS_QUERY_MAX_PACKET_SIZE)))
  {
    dns_log("Unable to convert packet to wire format.\n");
    DnsError::getInstance().setError("Unable to convert packet to wire format.");
  }
  else
  {
    uint16_t uLen = htons(uQuestionLen);
    dns_log("Question length is %u bytes (%u in network byte order).\n", (unsigned) uQuestionLen, (unsigned) uLen);
    memcpy(pQuestionBuff, &uLen, sizeof(uint16_t));

    struct timeval tSleep;
    fd_set tReads;

    bool bSend = true;

    bool bUseTCP = (getTcp() == DNS_RES_FORCE) ? true : false;

    for (int i = 0; i < m_iRetries; i++)
    {
      int iSending = uQuestionLen + sizeof(uint16_t);
      int iSocket = m_iSocket;
      if (bUseTCP)
      {
        dns_log("Opening TCP connection...\n");
        iSocket = openTCP(m_tRemoteAddr);
      }

      if (-1 == iSocket)
      {
        dns_log("Unable to use unassigned socket: %s\n", strerror(errno));
        DnsError::getInstance().setError("Unable to use unassigned socket ");
      }
      else if (bSend
               && !bUseTCP
               && -1 == sendto(iSocket, pBuff, uQuestionLen, 0, (struct sockaddr *) &m_tRemoteAddr, sizeof(m_tRemoteAddr)))
      {
        string sErr = "Unable to send UDP query to socket: ";
        char sz[11] = {0};
        sprintf(sz, "%d", iSocket);
        sErr += sz;
        sErr += ": ";
        sErr += strerror(errno);
        dns_log("%s\n", sErr.c_str());
        DnsError::getInstance().setError(sErr.c_str());
      }
      else if (bSend
               && bUseTCP
               && !sendAll(iSocket, pQuestionBuff, &iSending))
      {
        string sErr = "Unable to send TCP query: ";
        sErr += strerror(errno);
        dns_log("%s\n", sErr.c_str());
        DnsError::getInstance().setError(sErr.c_str());
      }
      else
      {
        bSend = true;
        tSleep.tv_sec = m_iTimeout;
        tSleep.tv_usec = 0;

        FD_ZERO(&tReads);
        FD_SET(iSocket, &tReads);

        pBuff = pFullBuff + sizeof(uint16_t);
        int iReady = select(iSocket + 1, &tReads, NULL, NULL, &tSleep);
        if (iReady < 0 && EINTR != errno)
        {
          dns_log("Unable to select: '%s'\n", strerror(errno));
          DnsError::getInstance().setError("Unable to select");
          break;
        }
        else if (FD_ISSET(iSocket, &tReads))
        {
          dns_log("Socket %d says it's ready to be read from...\n", iSocket);
          // p_oQueryPkt.print();
          if (!bUseTCP
              && 0 >= (iLen = recvfrom(iSocket, pBuff, DNS_UDP_MAX_PACKET_SIZE, 0, (struct sockaddr *) &tRespAddr, &tRespLen)))
          {
            dns_log("Unable to recvfrom: %s\n", strerror(errno));
            DnsError::getInstance().setError("Unable to recvfrom");
          }
          else if (!bUseTCP
                   && tRespAddr.sin_addr.s_addr != m_tRemoteAddr.sin_addr.s_addr)
          {
            dns_log("Received response from >different< nameserver than queries! (%u != %u)\n",
                    tRespAddr.sin_addr.s_addr,
                    m_tRemoteAddr.sin_addr.s_addr);
            DnsError::getInstance().setError("Received response from >different< nameserver than queries!");
            i--;
            bSend = false;
          }
          else if (bUseTCP
                   && 0 >= (iLen = recvAll(iSocket, pFullBuff, DNS_TCP_MAX_PACKET_SIZE + 2)))
          {
            dns_log("Unable to recv %d bytes from socket %d: %s\n", iLen, iSocket, strerror(errno));
            DnsError::getInstance().setError("Unable to recv");
          }
          else if (!p_oAnswer.fromWire(pBuff, iLen))
          {
            dns_log("Unable to construct reponse packet from buffer.\n");
            DnsError::getInstance().setError("Unable to construct reponse packet from buffer.");
          }
          else if (p_oAnswer.getHeader().id() != p_oQueryPkt.getHeader().id())
          {
            dns_log("Received response with >different< query ID! (%d != %d)\n", p_oAnswer.getHeader().id(), p_oQueryPkt.getHeader().id());
            DnsError::getInstance().setError("Received response with >different< query ID!");
            i--;
            bSend = false;
          }
          else
          {
            dns_log("DnsResolver::send() is processing the answer.\n");
            if (NULL != p_oQueryPkt.getTsig())
            {
              if (NULL == p_oAnswer.getTsig())
              {
                dns_log("Query had TSIG, but answer does not.\n");
                DnsError::getInstance().setError("Query had TSIG, but answer does not.");
              }
              else if (!p_oAnswer.getTsig()->verify(p_oAnswer, p_oQueryPkt.getTsig()->getMac(), p_oQueryPkt.getTsig()->getMacSize()))
              {
                dns_log("TSIG in answer does not verify.\n");
              }
              else
              {
                dns_log("Successfully verified TSIG.\n");
                bRet = true;
              }
            }
            else
            {
              bRet = true;
            }

            // If the packet was received successfully
            // and we were not using TCP
            // and we got a TC bit
            // and we are allowed to fallback to TCP...
            if (bRet
                && !bUseTCP
                && p_oAnswer.getHeader().get_tc()
                && getTcp() == DNS_RES_FALLBACK)
            {
              // Set the TCP bit and reset 1 retry
              dns_log("Got TC bit, retrying with TCP.\n");
              bUseTCP = true;
              iSocket = -1;
              i--;
            }
            else
            {
              break;
            }
          }
        }
      }

      if (bUseTCP && -1 != iSocket)
      {
        dns_log("Closing TCP socket %d\n", iSocket);
        close(iSocket);
      }
    }
  }

  return bRet;
}

int DnsResolver::getConcurrency()
{
  return m_iConc;
}

void DnsResolver::setConcurrency(int p_iConc)
{
  m_iConc = p_iConc;
}

int DnsResolver::getTcpConcurrency()
{
  return m_iTcpConc;
}

void DnsResolver::setTcpConcurrency(int p_iConc)
{
  m_iTcpConc = p_iConc;
}

bool DnsResolver::hasRoomToSend()
{
  bool bRet = false;


  if (m_oTaskMap.size() < (unsigned) m_iConc)
  {
    struct pollfd tPoll;
    tPoll.fd = m_iSocket;
    tPoll.events = POLLOUT;
    tPoll.revents = 0;
    int iErr = poll(&tPoll, 1, 200);
    if (tPoll.revents & POLLERR)
    {
      dns_log("Error on socket");
      DnsError::getInstance().setError("Error on socket");
    }
    else if (tPoll.revents & POLLHUP)
    {
      dns_log("Socket hang up");
      DnsError::getInstance().setError("Socket hang up");
    }
    else if (tPoll.revents & POLLNVAL)
    {
      dns_log("Socket not open");
      DnsError::getInstance().setError("Socket not open");
    }
    else if (EINTR != errno && 1 == iErr)
    {
      bRet = true;
    }
  }

  return bRet;
}

bool DnsResolver::hasRoomToSendTcp()
{
  bool bRet = false;

  if (m_oTcpTaskMap.size() < (unsigned) m_iTcpConc)
  {
    bRet = true;
  }

  return bRet;
}

bool DnsResolver::hasTasks()
{
  return !m_oTaskMap.empty();
}

bool DnsResolver::hasTcpTasks()
{
  return !m_oTcpTaskMap.empty();
}

size_t DnsResolver::numTasks()
{
  return m_oTaskMap.size();
}

size_t DnsResolver::numTcpTasks()
{
  return m_oTcpTaskMap.size();
}

// <EMO> Need proper OPT and TSIG logic here
bool DnsResolver::send(DnsTask *p_pTask)
{
  bool bRet = false;

  size_t uQuestionLen = 0;
  u_char pBuff[DNS_QUERY_MAX_PACKET_SIZE];

  DnsPacket *pQuery = NULL;

  struct sockaddr_in tAddr;
  memcpy(&tAddr, &m_tRemoteAddr, sizeof(tAddr));

  if (NULL == p_pTask)
  {
    dns_log("Task cannot be NULL\n");
    DnsError::getInstance().setError("Task cannot be NULL");
  }
  else if (NULL == (pQuery = p_pTask->getQuery()))
  {
    dns_log("Query packet cannot be NULL\n");
    DnsError::getInstance().setError("Query packet cannot be NULL");
  }
  else if (!p_pTask->usingTcp() && !hasRoomToSend())
  {
    dns_log("No room left for new UDP query.  Size is: %u\n", (unsigned) m_oTaskMap.size());
    DnsError::getInstance().setError("No room left for new UDP query");
  }
  else if (p_pTask->usingTcp() && !hasRoomToSendTcp())
  {
    dns_log("No room left for new TCP query.  Size is: %u\n", (unsigned) m_oTcpTaskMap.size());
    DnsError::getInstance().setError("No room left for new TCP query");
  }
  else
  {
    if (0 != p_pTask->getNameserver())
    {
      tAddr.sin_addr.s_addr = htonl(p_pTask->getNameserver());
    }

    if (0 != p_pTask->getPort())
    {
      tAddr.sin_port = htons(p_pTask->getPort());
    }

    if (NULL != p_pTask->getOpt())
    {
      DnsOpt *pOpt = new DnsOpt(*p_pTask->getOpt());
      pQuery->addAdditional(*pOpt);
    }
    else if (getBuffSize() > 512 || getDO())
    {
      DnsOpt *pOptRR = new DnsOpt();
      pOptRR->setMax(getBuffSize());
      pOptRR->setDO(getDO());
      pQuery->addAdditional(*pOptRR);
    }

    if (NULL == pQuery->getTsig())
    {
      if (NULL != p_pTask->getTsig())
      {
        DnsTsig *pTsig = new DnsTsig(*(p_pTask->getTsig()));
        pQuery->setTsig(pTsig);
      }
      else if (usingTsig())
      {
        DnsTsig *pTsig = new DnsTsig(*m_pTsig);
        pQuery->setTsig(pTsig);
      }
    }

    bool bUseTCP = p_pTask->usingTcp();
    bool bErr = true;

    // By default, use our UDP socket.
    int iSocket = m_iSocket;

    // If we are using TCP, don't use our default UDP socket
    if (bUseTCP)
    {
      // If this task has not started a TCP transaction yet, open a new
      // TCP connection, otherwise, use the socket that's already open.
      iSocket = (-1 == p_pTask->getTcpSocket()) ? openTCP(tAddr) : p_pTask->getTcpSocket();
    }

    if (-1 == iSocket)
    {
      dns_log("Unable to create socket: %s\n", strerror(errno));
      string sErr = "send(): Unable to create socket: ";
      sErr += DnsError::getInstance().getError();
      DnsError::getInstance().setError(sErr.c_str());
    }
    else if (!bUseTCP)
    {
      // First enqueue the task to make sure we have room, and to remap the 
      // qid if there is a conflict.
      if (!enqueueTask(*p_pTask))
      {
        dns_log("Unable to enqueue task.\n");
        DnsError::getInstance().setError("Unable to enqueue task.");
      }
      // Now that we have the task all set, convert it to wire format
      // and send it off.
      else if (0 == (uQuestionLen = pQuery->toWire(pBuff, DNS_QUERY_MAX_PACKET_SIZE)))
      {
        dns_log("Unable to convert packet to wire format.\n");
      bRet = true;
      }
      else if (-1 == sendto(iSocket, pBuff, uQuestionLen, 0, (struct sockaddr *) &tAddr, sizeof(tAddr)))
      {
        dns_log("Unable to send UDP query: %s\n", strerror(errno));
        DnsError::getInstance().setError("Unable to send UDP query");
      bRet = true;
      }
      else
      {
        bErr = false;
      }
    }
    else
    {
      // This task owns this socket now.
      p_pTask->setTcpSocket(iSocket);
      if (!enqueueTask(*p_pTask))
      {
        dns_log("Unable to enqueue task.\n");
        DnsError::getInstance().setError("Unable to enqueue task.");
      }
      else if (!p_pTask->initSendTcp())
      {
        dns_log("Unable to init-send TCP query: %s\n", strerror(errno));
        DnsError::getInstance().setError("Unable to send TCP query");
        bRet = true;
      }
      else
      {
        bRet = true;
        bErr = false;
      }
    }

    if (!bErr)
    {
      time_t tTimeout = time(NULL) + m_iTimeout;
      p_pTask->setTimeout(tTimeout);
      bRet = true;
    }
  }

  return bRet;
}

DnsTask *DnsResolver::recv()
{
  DnsTask *pRet = NULL;

  if (NULL != (pRet = checkUdp()))
  {
    dns_log("Got UDP task.\n");
  }
  else if (NULL != (pRet = checkTcp()))
  {
    dns_log("Got TCP task.\n");
  }
  else if (NULL != (pRet = checkForTimeouts()))
  {
#ifdef _DNS_DEBUG
    uint32_t uIP = pRet->getNameserver();
    if (0 == uIP)
    {
      uIP = ntohl(m_tRemoteAddr.sin_addr.s_addr);
    }
    string sKey = makeKey(*(pRet->getQuery()), uIP);
    dns_log("Timing out task with key: '%s'\n", sKey.c_str());
#endif
  }

  return pRet;
}

DnsTask *DnsResolver::checkUdp()
{
  DnsTask *pRet = NULL;

  int iLen = 0;

  struct pollfd tPoll;
  tPoll.fd = m_iSocket;
  tPoll.events = POLLIN;
  tPoll.revents = 0;
  int iErr = 0;

  struct sockaddr_in tRespAddr;
  socklen_t tRespLen = sizeof(tRespAddr);
  memset(&tRespAddr, 0, sizeof(tRespAddr));

  errno = 0;
  if (-1 == (iErr = poll(&tPoll, 1, 5)))
  {
    dns_log("Unable to poll FD: %d: [%d] %s\n", m_iSocket, iErr, strerror(errno));
    DnsError::getInstance().setError("Unable to poll FD");
  }
  else if (tPoll.revents & POLLERR)
  {
    dns_log("Error on socket\n");
    DnsError::getInstance().setError("Error on socket");
  }
  else if (tPoll.revents & POLLHUP)
  {
    dns_log("Socket hang up\n");
    DnsError::getInstance().setError("Socket hang up");
  }
  else if (tPoll.revents & POLLNVAL)
  {
    dns_log("Socket not open\n");
    DnsError::getInstance().setError("Socket not open");
  }
  else if (EINTR != errno && 1 == iErr)
  {
    DnsPacket *pRespPkt = new DnsPacket();
    u_char pBuff[DNS_UDP_MAX_PACKET_SIZE];

    if (0 >= (iLen = recvfrom(m_iSocket, pBuff, DNS_UDP_MAX_PACKET_SIZE, 0, (struct sockaddr *) &tRespAddr, &tRespLen)))
    {
      dns_log("Unable to recvfrom: %s\n", strerror(errno));
      DnsError::getInstance().setError("Unable to recvfrom\n");
      delete pRespPkt;
      pRespPkt = NULL;
    }
    else if (!pRespPkt->fromWire(pBuff, iLen))
    {
      dns_log("Unable to construct reponse packet from buffer.\n");
      DnsError::getInstance().setError("Unable to construct reponse packet from buffer.");
      delete pRespPkt;
      pRespPkt = NULL;
    }
    else if (NULL == (pRet = dequeueTask(*pRespPkt, ntohl(tRespAddr.sin_addr.s_addr), false)))
    {
#ifdef _DNS_DEBUG
      string sBlahKey = makeKey(*pRespPkt, ntohl(tRespAddr.sin_addr.s_addr));
      dns_log("Unable to locate task for packet with key '%s'\n", sBlahKey.c_str());
#endif
      DnsError::getInstance().setError("Unable to locate task for packet with key");
      delete pRespPkt;
      pRespPkt = NULL;
    }
    else
    {
      pRet->setResponse(pRespPkt);
      pRespPkt = NULL;
    }
  }

  return pRet;
}

DnsTask *DnsResolver::checkTcp()
{
  DnsTask *pRet = NULL;

  size_t uPollSize = m_oTcpTaskMap.size();

  struct pollfd *pPoll = new struct pollfd[uPollSize];
  memset(pPoll, 0, sizeof(struct pollfd) * uPollSize);

  FdMap_t oFdMap;

  dns_log("checkTcp()...\n");
  int iCount = 0;
  DnsTaskIter_t tIter;
  for (tIter = m_oTcpTaskMap.begin();
       m_oTcpTaskMap.end() != tIter;
       tIter++)
  {
    DnsTask *pTask = tIter->second;

    short iEvents = 0;
    int iSocket = pTask->getTcpSocket();
    DnsTaskStatus_e eStatus = pTask->getTcpStatus();
    if (TASK_SENDING == eStatus)
    {
      dns_log("Sending task on socket %d ...\n", iSocket);
      iEvents = POLLOUT;
    }
    else if (TASK_RECEIVING == eStatus)
    {
      dns_log("Receving task from socket %d ...\n", iSocket);
      iEvents = POLLIN;
    }
    else
    {
      dns_log("Found task in state %d using socket %d, not polling.\n", eStatus, iSocket);
    }

    if (0 != iEvents)
    {
      struct pollfd &tPoll = pPoll[iCount++];

      tPoll.fd = iSocket;
      tPoll.events = iEvents;
      tPoll.revents = 0;

      oFdMap[tPoll.fd] = pTask;

      dns_log("Polling task in state %d using socket %d...\n", eStatus, iSocket);
    }
  }

  errno = 0;
  int iErr = poll(pPoll, iCount, 5);
  if (-1 == iErr)
  {
    dns_log("Unable to poll: [%d] %s\n", iErr, strerror(errno));
    DnsError::getInstance().setError("Unable to poll FD");
  }
  else if (EINTR == errno)
  {
    dns_log("Interrupted poll...\n");
  }
  else
  {
    for (int i = 0; i < iCount; i++)
    {
      struct pollfd &tPoll = pPoll[i];
      DnsTask *pTask = oFdMap[tPoll.fd];

      if (tPoll.revents & POLLERR)
      {
        int iErr = 0;
        socklen_t tErrLen = sizeof(iErr);
        getsockopt(tPoll.fd, SOL_SOCKET, SO_ERROR, (void *)&iErr, &tErrLen);
        dns_log("Error on TCP socket %d : [%d] %s\n", tPoll.fd, iErr, strerror(iErr));
        DnsError::getInstance().setError("Error on TCP socket");
        pTask->setTcpStatus(TASK_ERROR);
        pTask->closeTcpSocket();
      }
      else if (tPoll.revents & POLLNVAL)
      {
        dns_log("TCP Socket %d not open\n", tPoll.fd);
        DnsError::getInstance().setError("TCP Socket not open");
        pTask->setTcpStatus(TASK_ERROR);
        pTask->closeTcpSocket();
      }
      else if (tPoll.revents & (POLLIN | POLLOUT))
      {
        dns_log("Checking TCP readiness from socket %d .\n", tPoll.fd);

        if (!pTask->checkTcpReady())
        {
          dns_log("Unable to chck TCP for task.\n");
        }
      }
      else if (tPoll.revents & POLLHUP)
      {
        dns_log("TCP Socket %d hang up\n", tPoll.fd);
        DnsError::getInstance().setError("TCP Socket hang up");
        pTask->setTcpStatus(TASK_ERROR);
        pTask->closeTcpSocket();
      }
      else if (0 == tPoll.revents)
      {
        dns_log("Socket %d is not ready.\n", tPoll.fd);
      }
      else
      {
        dns_log("Did not understand revents: %d (not POLLIN or POLLOUT %d, %d)\n", tPoll.revents, POLLIN, POLLOUT);
      }
    }

    for (tIter = m_oTcpTaskMap.begin();
         m_oTcpTaskMap.end() != tIter;
         tIter++)
    {
      DnsTask *pTask = tIter->second;
      int iSocket = pTask->getTcpSocket();
      (void) iSocket;
      DnsTaskStatus_e eStatus = PRE_INIT;
      if (NULL == pTask)
      {
        dns_log("Found NULL in TCP task map from socket %d for key: '%s'\n", iSocket, tIter->first.c_str());
        DnsError::getInstance().setError("Found NULL in TCP task map.");
      }
      else if (TASK_DONE == (eStatus = pTask->getTcpStatus()))
      {
        dns_log("Task done on socket %d ...\n", iSocket);
//        pRet = dequeueTask(*(pTask->getResponse()), getNameserver(), true);
        pTask->closeTcpSocket();
        pRet = dequeueTask(*(pTask->getQuery()), getNameserver(), true);
        break;
      }
      else if (PRE_INIT > eStatus)
      {
        string sBlahKey = makeKey(*(pTask->getQuery()), getNameserver(), true);
        dns_log("Task '%s' using socket %d in error state...\n", sBlahKey.c_str(), iSocket);
        pTask->closeTcpSocket();
        pRet = dequeueTask(*(pTask->getQuery()), getNameserver(), true);
        if (NULL == pRet)
        {
          dns_log("Unable to find error task: %s\n", sBlahKey.c_str());
        }
        break;
      }
    }
  }

  delete[] pPoll;

  return pRet;
}

bool DnsResolver::enqueueTask(DnsTask &p_oTask)
{
  bool bRet = false;

  DnsPacket *pQuery = p_oTask.getQuery();
  if (!p_oTask.usingTcp() && !hasRoomToSend())
  {
    dns_log("Unable to enqueueTask() when queue has no room.\n");
    DnsError::getInstance().setError("Unable to enqueueTask() when queue has no room.");
  }
  else if (p_oTask.usingTcp() && !hasRoomToSendTcp())
  {
    dns_log("Unable to enqueueTask() when TCP queue has no room.\n");
    DnsError::getInstance().setError("Unable to enqueueTask() when TCP queue has no room.");
  }
  else if (NULL == pQuery)
  {
    dns_log("Cannot enqueue task with NULL query.\n");
    DnsError::getInstance().setError("Cannot enqueue task with NULL query.");
  }
  else
  {
    uint32_t uIP = p_oTask.getNameserver();
    if (0 == uIP)
    {
      uIP = ntohl(m_tRemoteAddr.sin_addr.s_addr);
    }

    DnsTaskMap_t &oTaskMap = (p_oTask.usingTcp()) ? m_oTcpTaskMap : m_oTaskMap;

    for (int i = 0; i < 3 && !bRet; i++)
    {
      string sKey = makeKey(*pQuery, uIP, p_oTask.usingTcp());
      DnsTaskIter_t tIter = oTaskMap.find(sKey);
      if (oTaskMap.end() != tIter)
      {
        if (i + 1 >= 3)
        {
          dns_log("Entry in queue already exists for key '%s'!  This is an error!\n", sKey.c_str());
          DnsError::getInstance().setError("Entry in queue already exists for key!  This is an error!");
        }
        p_oTask.getQuery()->getHeader().assignID();
      }
      else
      {
        oTaskMap[sKey] = &p_oTask;
        bRet = true;
      }
    }
  }

  return bRet;
}

DnsTask *DnsResolver::dequeueTask(DnsPacket &p_oPkt, uint32_t p_uIP, bool p_bTcp)
{
  DnsTask *pRet = NULL;

  string sKey = makeKey(p_oPkt, p_uIP, p_bTcp);

  DnsTaskMap_t &oTaskMap = (p_bTcp) ? m_oTcpTaskMap : m_oTaskMap;

  DnsTaskIter_t tIter = oTaskMap.find(sKey);
  if (oTaskMap.end() != tIter)
  {
    pRet = tIter->second;
    oTaskMap.erase(tIter);
  }

  return pRet;
}

DnsTask *DnsResolver::checkForTimeouts()
{
  DnsTask *pRet = NULL;

  time_t tNow = time(NULL);

  DnsTaskMap_t *pMaps[2] = { &m_oTaskMap, &m_oTcpTaskMap };

  for (int i = 0; NULL == pRet && i < 2; i++)
  {
    DnsTaskMap_t &oTaskMap = *(pMaps[i]);

    for (DnsTaskIter_t tIter = oTaskMap.begin();
         oTaskMap.end() != tIter;
         tIter++)
    {
      DnsTask *pTask = tIter->second;
      if (pTask->getTimeout() < tNow)
      {
        pRet = pTask;
        // pRet->setResponse(NULL);
        pRet->timeoutTask();
        oTaskMap.erase(tIter);
        break;
      }
    }
  }

  return pRet;
}

std::string DnsResolver::makeKey(DnsPacket &p_oPkt, uint32_t p_uIP, bool p_bTcp /*= false*/)
{
  string sRet;

  ostringstream oSS;
  RRList_t tQs;
  if (p_oPkt.getHeader().qd_count() != 1)
  {
    dns_log("Improper number of questions: %d\n", p_oPkt.getHeader().qd_count());
    DnsError::getInstance().setError("Improper number of questions");
  }
  else if (!p_oPkt.getQuestions(tQs))
  {
    dns_log("Unable to get question RRs.\n");
    DnsError::getInstance().setError("Unable to get question RRs.");
  }
  else if (tQs.empty())
  {
    dns_log("Question list is empty, but QD count is == 1\n");
    DnsError::getInstance().setError("Question list is empty, but QD count is == 1");
  }
  else
  {
    DnsRR *pRR = tQs.front();
    string sName = pRR->get_name()->toString();
    transform(sName.begin(), sName.end(), sName.begin(), ::tolower);
    oSS << sName
        << "|"
        << pRR->type()
        << "|"
        << pRR->get_class()
        << "|"
        << p_oPkt.getHeader().id()
        << "|"
        << (unsigned) p_uIP
        << "|"
        << p_bTcp;
    sRet = oSS.str();
  }

  return sRet;
}

bool DnsResolver::setNonblocking(int p_iFD)
{
  bool bRet = false;
  int iFlags = 0;

#if defined(O_NONBLOCK)
  if (-1 == (iFlags = fcntl(p_iFD, F_GETFL, 0)))
  {
    iFlags = 0;
  }

  bRet = (0 == fcntl(p_iFD, F_SETFL, iFlags | O_NONBLOCK));
#else
  iFlags = 1;
  bRet = (0 == ioctl(p_iFD, FIOBIO, &iFlags));
#endif

  return bRet;
}

int DnsResolver::recvAll(int p_iSocket, u_char *p_pBuff, int p_iBuffLen)
{
  int iRet = -1;

  uint16_t uLen = 0;
  int iResult = 0;
  socklen_t tResLen = sizeof(iResult);

  memset(p_pBuff, 0, p_iBuffLen);

  if (-1 == p_iSocket)
  {
    dns_log("Socket DNE\n");
    DnsError::getInstance().setError("Socket DNE");
  }
  else if (NULL == p_pBuff)
  {
    dns_log("Must specifyu buffer.\n");
    DnsError::getInstance().setError("Must specifyu buffer.");
  }
  else if (p_iBuffLen <= 0)
  {
    dns_log("Buffer len must be greater than 0 (not %d).\n", p_iBuffLen);
    DnsError::getInstance().setError("Buffer len must be greater than 0");
  }
  else if (0 > getsockopt(p_iSocket, SOL_SOCKET, SO_ERROR, &iResult, &tResLen))
  {
    dns_log("Error on socket %d connect: %s\n", p_iSocket, strerror(errno));
  }
  else if (0 != iResult)
  {
    dns_log("Unable to connect on socket %d: %s\n", p_iSocket, strerror(errno));
  }
  else
  {
    dns_log("TCP socket %d ready...\n", p_iSocket);
    errno = 0;
    int iLen = 0;
    uint16_t uRespLen = 0;

    iRet = 0;
    dns_log("Getting buffer length from TCP socket %d\n", p_iSocket);
    iLen = ::recv(p_iSocket, p_pBuff, sizeof(uint16_t), 0);
    if (0 >= iLen)
    {
      dns_log("Unable to get response length (ret == %d): %s\n", iLen, strerror(errno));
    }
    else
    {
      iRet = iLen;
      memcpy(&uRespLen, p_pBuff, sizeof(uint16_t));
      uLen = ntohs(uRespLen);

      dns_log("Starting loop over TCP receive on socket %d...\n", p_iSocket);
      while (0 == uLen || uLen > (iRet - sizeof(uint16_t)))
      {
        // <EMO> 2015-08-04
        // dns_log("iRet is %d, buff len is %d\n", iRet, p_iBuffLen);
        // iLen = ::recv(p_iSocket, &(p_pBuff[iRet]), p_iBuffLen - iRet, 0);
        iLen = ::recv(p_iSocket, &(p_pBuff[iRet]), uRespLen, 0);

        dns_log("Looping got back %d bytes and errno '%s'...\n", iLen, strerror(errno));
        dns_log("Total is %d and expected is %u\n", iRet, (unsigned) uLen);
        if (0 == iLen)
        {
          dns_log("Server has gone away (returned 0)\n");
          break;
        }
        else if (iLen < 0 )
        {
          if (EAGAIN == errno || EWOULDBLOCK == errno)
          {
            fd_set tReads;
            struct timeval tSleep;
            tSleep.tv_sec = 0;
            tSleep.tv_usec = 200;

            FD_ZERO(&tReads);
            FD_SET(p_iSocket, &tReads);

            select(p_iSocket + 1, &tReads, NULL, NULL, &tSleep);
          }
          else if (EINTR != errno)
          {
            dns_log("Got error on socket %d: %s\n", p_iSocket, strerror(errno));
            DnsError::getInstance().setError("Got error on socket");
            iRet = -1;
            break;
          }
        }
        else if (iLen > 0)
        {
          iRet += iLen;
          if (0 == uLen)
          {
            uLen = *(uint16_t *) p_pBuff;
            uLen = ntohs(uLen);
          }
          dns_log("Got back %d bytes, total is %d and expected is %u\n", iLen, iRet, (unsigned) uLen + 2);
        }

        fd_set tReads;
        struct timeval tSleep;
        tSleep.tv_sec = 0;
        tSleep.tv_usec = 200;

        FD_ZERO(&tReads);
        FD_SET(p_iSocket, &tReads);

        select(p_iSocket + 1, &tReads, NULL, NULL, &tSleep);
      }
    }
  }

  if (uLen != (iRet - sizeof(uint16_t)))
  {
    dns_log("Error receving all of the message (%lu != %lu): %s\n", (long unsigned) uLen, iRet - sizeof(uint16_t), strerror(errno));
    DnsError::getInstance().setError("Error receving all of the message.");
    iRet = -1;
  }

  return iRet;
}

bool DnsResolver::init()
{
  bool bRet = false;

  FILE *pFile = NULL;

  if (NULL == (pFile = fopen("/etc/resolv.conf", "r")))
  {
    dns_log("Unable to open '/etc/resolv.conf': %s\n", strerror(errno));
  }
  else
  {
    unsigned uA = 0;
    unsigned uB = 0;
    unsigned uC = 0;
    unsigned uD = 0;

    s_tDefaultResolverIP.sin_addr.s_addr = 0;

    char pBuff[BUFSIZ];
    memset(pBuff, 0, BUFSIZ);
    while (fgets(pBuff, BUFSIZ, pFile))
    {
      if (sscanf(pBuff, "nameserver %u.%u.%u.%u", &uA, &uB, &uC, &uD))
      {
        s_tDefaultResolverIP.sin_addr.s_addr = htonl(
                                             (uA << 24)
                                             + (uB << 16)
                                             + (uC << 8)
                                             + uD
                                           );
        s_tDefaultResolverIP.sin_port = htons(53);
        s_tDefaultResolverIP.sin_family = AF_INET;
        break;
      }
    }
    fclose(pFile);
    dns_log("Setting default resolver to: '%s'\n", inet_ntoa(s_tDefaultResolverIP.sin_addr));
    bRet = true;
  }

  return bRet;
}

int DnsResolver::openTCP(struct sockaddr_in &p_tAddr)
{
  int iRet = -1;

  errno = 0;
  int iErr = 0;
#ifdef SOCK_NONBLOCK
  int iTmp = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
#else
  int iTmp = socket(AF_INET, SOCK_STREAM, 0);
#endif
  if (-1 == iTmp)
  {
    dns_log("Unable to open stream socket: %s\n", strerror(errno));
    DnsError::getInstance().setError("Unable to open stream socket");
  }
#ifdef SOCK_NONBLOCK
  else if (0 != (iErr = connect(iTmp, (struct sockaddr *) &p_tAddr, sizeof(p_tAddr)))
           && errno != EINPROGRESS)
#else
  else if (0 != (iErr = connect(iTmp, (struct sockaddr *) &p_tAddr, sizeof(p_tAddr))))
#endif
  {
    dns_log("Unable to connect to remote name server at %s:%d %s\n", inet_ntoa(p_tAddr.sin_addr), ntohs(p_tAddr.sin_port), strerror(errno));
    stringstream oSS;
    oSS << "Unable to connect to remote name server: ["
        << errno
        << "] "
        << strerror(errno);
    DnsError::getInstance().setError(oSS.str().c_str());
  }
#ifndef SOCK_NONBLOCK
  else if (!setNonblocking(iTmp))
  {
    dns_log("Unable to set socket %d to non-blocking: %s\n", iTmp, strerror(errno));
    DnsError::getInstance().setError("Unable to set socket %d to non-blocking.");
  }
#endif
  else
  {
    iRet = iTmp;
    dns_log("Opened TCP socket %d to %s:%d\n", iRet, inet_ntoa(p_tAddr.sin_addr), ntohs(p_tAddr.sin_port));
  }

  return iRet;
}

int DnsResolver::openBlockingTCP(struct sockaddr_in &p_tAddr)
{
  int iRet = -1;

  errno = 0;
  int iErr = 0;
  int iTmp = socket(AF_INET, SOCK_STREAM, 0);
  if (-1 == iTmp)
  {
    dns_log("Unable to open stream socket: %s\n", strerror(errno));
    DnsError::getInstance().setError("Unable to open stream socket");
  }
  else if (0 != (iErr = connect(iTmp, (struct sockaddr *) &p_tAddr, sizeof(p_tAddr))))
  {
    dns_log("Unable to connect to remote name server at %s:%d %s\n", inet_ntoa(p_tAddr.sin_addr), ntohs(p_tAddr.sin_port), strerror(errno));
    stringstream oSS;
    oSS << "Unable to connect to remote name server: ["
        << errno
        << "] "
        << strerror(errno);
    DnsError::getInstance().setError(oSS.str().c_str());
  }
  else
  {
    iRet = iTmp;
    dns_log("Opened Blocking TCP socket %d to %s:%d\n", iRet, inet_ntoa(p_tAddr.sin_addr), ntohs(p_tAddr.sin_port));
  }

  return iRet;
}


bool DnsResolver::sendAll(int p_iSocket, u_char *p_pBuf, int *p_pLen)
{
  int iTotal = 0;        // how many bytes we've sent
  int iBytesleft = *p_pLen; // how many we have left to send
  int n = 0;

  struct timeval tSleep;
  fd_set tWrites;

  int i = 0;
  for (i = 0; iTotal < *p_pLen && i < m_iRetries; i++)
  {
    tSleep.tv_sec = m_iTimeout;
    tSleep.tv_usec = 0;

    FD_ZERO(&tWrites);
    FD_SET(p_iSocket, &tWrites);

    int iResult = 0;
    socklen_t tResLen = sizeof(iResult);
    int iReady = select(p_iSocket + 1, NULL, &tWrites, NULL, &tSleep);
    if (iReady < 0 && EINTR != errno)
    {
      dns_log("Unable to select: '%s'\n", strerror(errno));
      DnsError::getInstance().setError("Unable to select");
    }
    else if (!FD_ISSET(p_iSocket, &tWrites))
    {
      dns_log("Socket not ready...\n");
    }
    else if (0 > getsockopt(p_iSocket, SOL_SOCKET, SO_ERROR, &iResult, &tResLen))
    {
      dns_log("Error on socket %d connect: %s\n", p_iSocket, strerror(errno));
    }
    else if (0 != iResult)
    {
      dns_log("Unable to connect on socket %d: %s\n", p_iSocket, strerror(errno));
    }
    else
    {
      dns_log("TCP socket %d successfully connected...\n", p_iSocket);

      while(iTotal < *p_pLen) {
        dns_log("Sending incremental %d (already sent %d)\n", iBytesleft, iTotal);
        n = ::send(p_iSocket, p_pBuf+iTotal, iBytesleft, 0);
        if (n == -1)
        {
          dns_log("Got back errno %d\n", errno);
          if (EAGAIN != errno)
          {
            dns_log("Breaking...\n");
            break;
          }
          sleep(1);
        }
        else
        {
          dns_log("Sent %d of %d bytes\n", n, iBytesleft);
          iTotal += n;
          iBytesleft -= n;
        }
      }
    }
  }

  if (i >= m_iRetries)
  {
    dns_log("Unable to send query to TCP socket for socket %d: %s\n", p_iSocket, strerror(errno));
  }

  *p_pLen = iTotal; // return number actually sent here

  return n != -1;
} 
