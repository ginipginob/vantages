#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <string>

#include "dns_resolver.h"
#include "dns_task.h"
#include "dns_packet.h"
#include "dns_rr.h"
#include "dns_name.h"
#include "dns_opt.h"
#include "dns_err.h"
#include "dns_defs.h"

using namespace std;

int main(int argc, char *argv[])
{
  int iRet = 1;

  struct in_addr tAddr;
  memset(&tAddr, 0, sizeof(tAddr));
  uint32_t p_uIP = 0;
  inet_aton("8.8.8.8", &tAddr);
  uIP = tAddr.s_addr;

  const char *szDomain = "secspider.cs.ucla.edu";

  if (argc < 2) {
    printf("%s <IP> [ <domain>  | -h\n", argv[0]);
    exit(1);
  }
  else
  {
    szDomain = argv[1];
    if (0 == strcmp(szDomain, "-h"))
    {
      printf("%s <IP> [ <domain>  | -h\n", argv[0]);
      exit(0);
    }

    if (argc > 2) {
      szDomain = argv[2];
    }
  }

  unsigned char pCookie[] = { 72, 61, 77, 62, 79, 74, 65, 73 };
  DnsTask oTask;
  string sDomain(szDomain);
  DnsName oName(sDomain);
  DnsRR *pQuestionRR = DnsRR::question(oName, DNS_RR_A);
  if (NULL == pQuestionRR)
  {
    fprintf(stderr, "Unable to crete question for type: %d\n", DNS_RR_A);
  }
  else
  {
    DnsPacket *pPkt = new DnsPacket(true, -1);
    DnsOpt *pOpt = new DsnOpt();
    pOpt->addOption(10, sizeof(pCookie), pCookie);
    pPkt->addOpt(pOpt);

    pQuestionRR->set_class(DNS_CLASS_IN);
    pPkt->addQuestion(*pQuestionRR);
    pPkt->getHeader().set_cd(true);
    oTask.setQuery(pPkt);
    oTask.setResponse(NULL);

    DnsResolver oRes;

    fprintf(stdout, "Sending UDP message...\n");

    if (!oRes.send(&oTask))
    {
      fprintf(stderr, "Unable to send task.\n");
    }
    else
    {
      DnsTask *pResp = NULL;
      while (NULL == pResp)
      {
        sleep(1);
        pResp = oRes.recv();

        DnsPacket *pRespPkt = NULL;
        if (NULL != pResp)
        {
          pRespPkt = pResp->getResponse();
          if (NULL != pRespPkt)
          {
            fprintf(stdout, "Got response:\n");
            pRespPkt->print();
          }
        }
      }
    }

    iRet = 0;
  }

  return iRet;
}
