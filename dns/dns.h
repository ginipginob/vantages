/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _VANT_DNS_H
#define _VANT_DNS_H

#include <vantages/dns_a.h>
#include <vantages/dns_aaaa.h>
#include <vantages/dns_cdnskey.h>
#include <vantages/dns_cds.h>
#include <vantages/dns_cname.h>
#include <vantages/dns_compression.h>
#include <vantages/dns_defs.h>
#include <vantages/dns_dnskey.h>
#include <vantages/dns_ds.h>
#include <vantages/dns_err.h>
#include <vantages/dns_factory.h>
#include <vantages/dns_header.h>
#include <vantages/dns_info.h>
#include <vantages/dns_mutex.h>
#include <vantages/dns_mutex_hdlr.h>
#include <vantages/dns_mx.h>
#include <vantages/dns_name.h>
#include <vantages/dns_ns.h>
#include <vantages/dns_nsec.h>
#include <vantages/dns_opt.h>
#include <vantages/dns_packet.h>
#include <vantages/dns_resolver.h>
#include <vantages/dns_rr.h>
#include <vantages/dns_rr_fact.h>
#include <vantages/dns_rrset.h>
#include <vantages/dns_rrsig.h>
#include <vantages/dns_soa.h>
#include <vantages/dns_srv.h>
#include <vantages/dns_task.h>
#include <vantages/dns_tlsa.h>
#include <vantages/dns_tsig.h>
#include <vantages/dns_txt.h>
#include <vantages/dns_verifier.h>

#endif
