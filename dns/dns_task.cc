/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "dns_task.h"
#include "dns_packet.h"
#include "dns_opt.h"
#include "dns_tsig.h"
#include "dns_err.h"

DnsTask::DnsTask()
  : m_pQuery(NULL),
    m_pResponse(NULL),
    m_pOpt(NULL),
    m_pTsig(NULL),
    m_tTimeout(0),
    m_uIP(0),
    m_uPort(53),
    m_bTcp(false),
    m_iTcpSocket(-1),
    m_eTcpStatus(PRE_INIT),
    m_pTcpBuff(NULL),
    m_uTcpBuffLen(0),
    m_iTcpOffset(0)
{

}

DnsTask::~DnsTask()
{
  setQuery(NULL);
  setResponse(NULL);
  setOpt(NULL);
  setTsig(NULL);
  closeTcpSocket();

  setTcpBuff(NULL, 0);
}

DnsPacket *DnsTask::getQuery()
{
  return m_pQuery;
}

void DnsTask::setQuery(DnsPacket *p_pQuery)
{
  if (NULL != m_pQuery)
  {
    delete m_pQuery;
    m_pQuery = NULL;
  }

  m_pQuery = p_pQuery;
}

DnsPacket *DnsTask::getResponse()
{
  return m_pResponse;
}

void DnsTask::setResponse(DnsPacket *p_pResp)
{
  if (NULL != m_pResponse)
  {
    delete m_pResponse;
    m_pResponse = NULL;
  }

  m_pResponse = p_pResp;
}

time_t DnsTask::getTimeout()
{
  return m_tTimeout;
}

void DnsTask::setTimeout(time_t p_tTimeout)
{
  m_tTimeout = p_tTimeout;
}

uint32_t DnsTask::getNameserver()
{
  return m_uIP;
}

void DnsTask::setNameserver(uint32_t p_uIP)
{
  m_uIP = p_uIP;
}

uint16_t DnsTask::getPort()
{
  return m_uPort;
}

void DnsTask::setPort(uint16_t p_uPort)
{
  m_uPort = p_uPort;
}

DnsOpt *DnsTask::getOpt()
{
  return m_pOpt;
}

bool DnsTask::usingTcp()
{
  return m_bTcp;
}

void DnsTask::useTcp(bool p_bTcp)
{
  m_bTcp = p_bTcp;
}

int DnsTask::getTcpSocket()
{
  return m_iTcpSocket;
}

void DnsTask::setTcpSocket(int p_iSocket)
{
  if (-1 != p_iSocket && -1 != m_iTcpSocket)
  {
    dns_log("DnsTask::setTcpSocket(): Rolling from %d to %d\n", m_iTcpSocket, p_iSocket);
  }
  m_iTcpSocket = p_iSocket;
}

void DnsTask::closeTcpSocket()
{
  if (-1 != m_iTcpSocket)
  {
    close(m_iTcpSocket);
    m_iTcpSocket = -1;
  }
}

DnsTaskStatus_e DnsTask::getTcpStatus()
{
  return m_eTcpStatus;
}

void DnsTask::setTcpStatus(DnsTaskStatus_e p_eStatus)
{
  m_eTcpStatus = p_eStatus;
}

void DnsTask::setOpt(DnsOpt *p_pOpt)
{
  if (NULL != m_pOpt)
  {
    delete m_pOpt;
    m_pOpt = NULL;
  }

  if (NULL != p_pOpt)
  {
    m_pOpt = new DnsOpt(*p_pOpt);
  }
}

bool DnsTask::initSendTcp(int p_iSocket /*= -1*/)
{
  bool bRet = false;

  size_t uLen = 0;
  DnsPacket *pQuery = getQuery();
  u_char pBuff[DNS_TCP_MAX_PACKET_SIZE + 2];
  memset(pBuff, 0, DNS_TCP_MAX_PACKET_SIZE + 2);

  if (TASK_ERROR > getTcpStatus()
      || TASK_DONE < getTcpStatus())
  {
    dns_log("Unable to send task when it is in state %d\n", getTcpStatus());
    DnsError::getInstance().setError("Unable to send task when it is in process.");
  }
  else if (-1 >= p_iSocket && -1 >= getTcpSocket())
  {
    dns_log("Unable to send to TCP input socket number %d and pre-set socket number %d\n", 
            p_iSocket,
            getTcpSocket());
    DnsError::getInstance().setError("Unable to send to TCP socket < 0");
  }
  else if (NULL == pQuery)
  {
    dns_log("Unable to send TCP with a NULL query.\n");
    DnsError::getInstance().setError("Unable to send TCP with a NULL query.");
  }
  else if (0 == (uLen = pQuery->toWire(pBuff + 2, DNS_QUERY_MAX_PACKET_SIZE)))
  {
    dns_log("Unable to convert query to wire format.\n");
  }
  else
  {
    // If this method is called with a socket,
    // it should override any previously set
    // socket.
    if (-1 != p_iSocket)
    {
      int iOldSock = getTcpSocket();
      if (-1 != iOldSock && iOldSock != p_iSocket)
      {
        dns_log("Rolling socket over from %d to %d for TCP.\n", iOldSock, p_iSocket);
        closeTcpSocket();
      }
      setTcpSocket(p_iSocket);
    }

    uint16_t uWireLen = uLen;
    uWireLen = htons(uWireLen);
    memcpy(pBuff, &uWireLen, sizeof(uint16_t));

    setTcpBuff(pBuff, uLen + sizeof(uint16_t));
    setTcpStatus(TASK_SENDING);
    setTcpOffset(0);

    bRet = true;
  }

  return bRet;
}

bool DnsTask::sendTcp(int p_iSocket /*= -1*/)
{
  bool bRet = false;

  size_t uLen = 0;
  DnsPacket *pQuery = getQuery();
  u_char pBuff[DNS_QUERY_MAX_PACKET_SIZE + 2];
  memset(pBuff, 0, DNS_QUERY_MAX_PACKET_SIZE + 2);

  if (PRE_INIT > getTcpStatus()
      || TASK_DONE < getTcpStatus())
  {
    dns_log("Unable to send task when it is in state %d\n", getTcpStatus());
    DnsError::getInstance().setError("Unable to send task when it is in process.");
  }
  else if (-1 >= p_iSocket && -1 >= getTcpSocket())
  {
    dns_log("Unable to send to TCP input socket number %d and pre-set socket number %d\n", 
            p_iSocket,
            getTcpSocket());
    DnsError::getInstance().setError("Unable to send to TCP socket < 0");
  }
  else if (NULL == pQuery)
  {
    dns_log("Unable to send TCP with a NULL query.\n");
    DnsError::getInstance().setError("Unable to send TCP with a NULL query.");
  }
  else if (0 == (uLen = pQuery->toWire(pBuff + 2, DNS_QUERY_MAX_PACKET_SIZE)))
  {
    dns_log("Unable to convert query to wire format.\n");
  }
  else
  {
    // If this method is called with a socket,
    // it should override any previously set
    // socket.
    if (-1 != p_iSocket)
    {
      int iOldSock = getTcpSocket();
      if (-1 != iOldSock && iOldSock != p_iSocket)
      {
        dns_log("sendTcp(): Rolling socket over from %d to %d for TCP.\n", iOldSock, p_iSocket);
        closeTcpSocket();
      }
      setTcpSocket(p_iSocket);
    }

    uint16_t uWireLen = uLen;
    uWireLen = htons(uWireLen);
    memcpy(pBuff, &uWireLen, sizeof(uint16_t));

    setTcpBuff(pBuff, uLen + sizeof(uint16_t));
    setTcpStatus(TASK_SENDING);
    int iNum = send(p_iSocket, pBuff, uLen + sizeof(uint16_t), 0);
    if (-1 == iNum)
    {
      dns_log("Unable to send TCP query: %s\n", strerror(errno));
      DnsError::getInstance().setError("Unable to send TCP query");
    }
    else if (iNum > (int) (uLen + sizeof(uint16_t)))
    {
      dns_log("Sent too many TCP bytes %i > %u\n", iNum, (unsigned int) (uLen + sizeof(uint16_t)));
      DnsError::getInstance().setError("Sent too many TCP bytes.");
    }
    else
    {
      if (iNum == (int) (uLen + sizeof(uint16_t)))
      {
        setTcpStatus(TASK_RECEIVING);
        setTcpBuff(NULL, 0);
      }
      else
      {
        setTcpOffset(iNum);
      }
      bRet = true;
    }
  }

  return bRet;
}

bool DnsTask::checkTcpReady()
{
  bool bRet = false;

  u_char *pBuff = getTcpBuff();
  int iOffset = getTcpOffset();
  size_t uLen = getTcpBuffLen();
  int iSocket = getTcpSocket();
  if (0 > iSocket)
  {
    dns_log("Unable to check TCP socket that is less than 0: %d\n", iSocket);
    DnsError::getInstance().setError("Unable to check TCP socket that is less than 0.");
  }
  else if (PRE_INIT >= getTcpStatus())
  {
    dns_log("Task using socket %d is not in a valid state to check TCP: %d\n", iSocket, getTcpStatus());
    DnsError::getInstance().setError("Task is not in a valid state to check TCP.");
    closeTcpSocket();
  }
  else if (TASK_DONE <= getTcpStatus())
  {
    dns_log("Task is done.\n");
    closeTcpSocket();
    bRet = true;
  }
/*
  else if (NULL == pBuff)
  {
    dns_log("TCP buffer is NULL, unable to check.\n");
    DnsError::getInstance().setError("TCP buffer is NULL, unable to check.");
  }
*/
  else if (TASK_SENDING == getTcpStatus())
  {
    dns_log("Incremental TCP send on socket %d (offset = %d, len = %u)...\n", iSocket, iOffset, (unsigned) uLen);

    int iNum = send(iSocket, &(pBuff[iOffset]), uLen - iOffset, 0);
    if (-1 == iNum)
    {
      dns_log("Unable to re-send TCP query: %s\n", strerror(errno));
      DnsError::getInstance().setError("Unable to re-send TCP query");
    }
    else if (iNum > (int) uLen - iOffset)
    {
      dns_log("Re-sent too many TCP bytes %i > %u\n", iNum, (unsigned) uLen - iOffset);
      DnsError::getInstance().setError("Re-sent too many TCP bytes.");
    }
    else
    {
      if (iNum == (int) uLen - iOffset)
      {
        setTcpStatus(TASK_RECEIVING);
        setTcpBuff(NULL, 0);
      }
      else
      {
        setTcpOffset(iNum + iOffset);
      }
      bRet = true;
    }
  }
  else if (TASK_RECEIVING == getTcpStatus())
  {
    dns_log("Task receiving TCP on socket %d ...\n", iSocket);
    int iNum = 0;
    if (0 == uLen)
    {
      dns_log("Getting the TCP length...\n");
      uint16_t uMsgLen = 0;
      iNum = recv(iSocket, &uMsgLen, sizeof(uint16_t), 0);
      if (0 == iNum)
      {
        dns_log("No data read from socket %d, will rety next poll.\n", iSocket);
      }
      else if ((int) sizeof(uint16_t) > iNum)
      {
        dns_log("Unable to get buffer length from new TCP response: %u < 2 : %s\n", iNum, strerror(errno));
        DnsError::getInstance().setError("Unable to get buffer length from new TCP response.");
      }
      else
      {
        uLen = ntohs(uMsgLen);
        setTcpBuff(NULL, uLen);
        pBuff = getTcpBuff();
        dns_log("Getting TCP response of size: %u\n", (unsigned) uLen);
        bRet = true;
      }
    }
    else
    {
      dns_log("Incremental TCP recv (offset = %d, len = %u)...\n", iOffset, (unsigned) uLen);
      iNum = recv(iSocket, &(pBuff[iOffset]), uLen - iOffset, 0);
      if (-1 == iNum)
      {
        dns_log("Unable to recv TCP query: %s\n", strerror(errno));
        DnsError::getInstance().setError("Unable to recv TCP query");
      }
      else if (iNum > (int) uLen - iOffset)
      {
        dns_log("Received too many TCP bytes %i > %u\n", iNum, (unsigned) uLen - iOffset);
        DnsError::getInstance().setError("Received too many TCP bytes.");
      }
      else
      {
        if (iNum == (int) uLen - iOffset)
        {
          setTcpStatus(TASK_DONE);
          setTcpOffset(0);
          closeTcpSocket();

          DnsPacket *pRespPkt = new DnsPacket();
          if (!pRespPkt->fromWire(pBuff, (int) uLen))
          {
            dns_log("Unable to construct reponse packet from TCP buffer.\n");
            DnsError::getInstance().setError("Unable to construct reponse packet from TCP buffer.");
            delete pRespPkt;
            pRespPkt = NULL;
          }
          else
          {
            setResponse(pRespPkt);
          }
        }
        else
        {
          setTcpOffset(iNum + iOffset);
        }
        bRet = true;
      }
    }
  }
  else
  {
    dns_log("Task in unknown TCP state: %d\n", getTcpStatus());
    DnsError::getInstance().setError("Task in unknown TCP state.");
  }

  return bRet;
}

DnsTsig *DnsTask::getTsig()
{
  return m_pTsig;
}

void DnsTask::setTsig(DnsTsig *p_pTsig)
{
  if (NULL != m_pTsig)
  {
    delete m_pTsig;
    m_pTsig = NULL;
  }

  if (NULL != p_pTsig)
  {
    m_pTsig = new DnsTsig(*p_pTsig);
  }
}

u_char *DnsTask::getTcpBuff()
{
  return m_pTcpBuff;
}

size_t DnsTask::getTcpBuffLen()
{
  return m_uTcpBuffLen;
}

int DnsTask::getTcpOffset()
{
  return m_iTcpOffset;
}

void DnsTask::setTcpBuff(u_char *p_pBuff, size_t p_uBuffLen)
{
  if (NULL != m_pTcpBuff)
  {
    delete[] m_pTcpBuff;
    m_pTcpBuff = NULL;
  }

  m_uTcpBuffLen = p_uBuffLen;
  m_iTcpOffset = 0;

  if (0 < m_uTcpBuffLen)
  {
    m_pTcpBuff = new u_char[m_uTcpBuffLen];
    memset(m_pTcpBuff, 0, m_uTcpBuffLen);
  }

  if (NULL != p_pBuff)
  {
    memcpy(m_pTcpBuff, p_pBuff, m_uTcpBuffLen);
  }
}

void DnsTask::setTcpOffset(int p_iOffset)
{
  m_iTcpOffset = p_iOffset;
}

void DnsTask::timeoutTask()
{
  setResponse(NULL);
  if (usingTcp())
  {
    setTcpStatus(TASK_DONE);
    closeTcpSocket();
  }
}
