/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _DNS_TASK_H
#define _DNS_TASK_H

#include "dns_defs.h"
#include <sys/types.h>

class DnsPacket;
class DnsOpt;
class DnsTsig;

class DnsTask
{
  // Types and enums
  public:

  // Member Variables
  private:
    DnsPacket *m_pQuery;
    DnsPacket *m_pResponse;
    DnsOpt *m_pOpt;
    DnsTsig *m_pTsig;
    time_t m_tTimeout;
    uint32_t m_uIP;
    uint16_t m_uPort;

    bool m_bTcp;
    int m_iTcpSocket;
    DnsTaskStatus_e m_eTcpStatus;
    u_char *m_pTcpBuff;
    size_t m_uTcpBuffLen;
    int m_iTcpOffset;

  // Methods
  public:
    DnsTask();
    virtual ~DnsTask();

    virtual DnsPacket *getQuery();
    virtual void setQuery(DnsPacket *p_pQuery);
    virtual DnsPacket *getResponse();
    virtual void setResponse(DnsPacket *p_pResp);

    time_t getTimeout();
    void setTimeout(time_t p_tTimeout);

    uint32_t getNameserver();
    void setNameserver(uint32_t p_uIP);

    uint16_t getPort();
    void setPort(uint16_t p_uIP);

    bool usingTcp();
    void useTcp(bool p_bTcp);

    int getTcpSocket();
    void setTcpSocket(int p_iSocket);

    DnsTaskStatus_e getTcpStatus();
    void setTcpStatus(DnsTaskStatus_e p_eStatus);
    void closeTcpSocket();

    virtual bool initSendTcp(int p_iSocket = -1);
    virtual bool sendTcp(int p_iSocket = -1);
    virtual bool checkTcpReady();

    DnsOpt *getOpt();
    void setOpt(DnsOpt *p_pOpt);

    DnsTsig *getTsig();
    void setTsig(DnsTsig *p_pTsig);

    void timeoutTask();

  protected:
    u_char *getTcpBuff();
    size_t getTcpBuffLen();
    int getTcpOffset();
    void setTcpBuff(u_char *p_pBuff, size_t p_uBuffLen);
    void setTcpOffset(int p_iOffset);
};

#endif
