/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _PHAS_DEFS_H
#define _PHAS_DEFS_H

#include <list>
#include <string>
#include <map>

#define PHAS_BGPMON_ADDR "phas_bgpmon_addr"
#define PHAS_BGPMON_PORT "phas_bgpmon_port"
#define PHAS_CONFIG_WITHDRAW_WAIT "phas_withdraw_wait"
#define PHAS_CONFIG_EVENT_LOG_ROOT_DIR "phas_event_log_root_dir"
#define PHAS_CONFIG_PROMISC_ALERT "phas_promisc_alert"
#define PHAS_CONFIG_APP_PASS "phas_app_pass"

#define BGPMON_ASCII_TAG "ASCII_MSG"
#define BGPMON_AS_PATH_TAG "AS_PATH"
#define BGPMON_AS_TAG "AS"
#define BGPMON_BGP_MESSAGE_TAG "BGP_MESSAGE"
#define BGPMON_DST_ADDR_TAG "DST_ADDR"
#define BGPMON_DST_AS_TAG "DST_AS"
#define BGPMON_DST_PORT_TAG "DST_PORT"
#define BGPMON_PEERING_TAG "PEERING"
#define BGPMON_PREFIX_TAG "PREFIX"
#define BGPMON_SRC_ADDR_TAG "SRC_ADDR"
#define BGPMON_SRC_AS_TAG "SRC_AS"
#define BGPMON_SRC_PORT_TAG "SRC_PORT"
#define BGPMON_UPDATE_TAG "UPDATE"
#define BGPMON_TIMESTAMP_TAG "TIMESTAMP"

#define _PHAS_XML_BUFF_SIZE 4096
// #define _PHAS_XML_BUFF_SIZE 4

#define PHAS_CONFIG_APP_ENABLED "phas_app_enabled"

#define PHAS_EVENT_QUEUE_DEFAULT_MAX 100000

#define PHAS_WITHDRAW_WAIT_DEFAULT 86400

typedef enum
{
  PHAS_ORIGIN_PRE_INIT = 0,
  PHAS_ORIGIN_ORIGIN,
  PHAS_ORIGIN_NEXT_HOP
} phas_origin_type_e;

typedef enum
{
  PHAS_SET_CHANGE_PRE_INIT = 0,
  PHAS_SET_CHANGE_WITHDRAW,
  PHAS_SET_CHANGE_ANNOUNCE
} phas_set_change_type_e;

class PhasBgpMsg;
class PhasPrefix;
class PhasEvent;

typedef struct phas_origin_s
{
  unsigned int m_uAS;
  time_t       m_tWhen;
} phas_origin_t;

typedef std::list<std::string> PrefixList_t;
typedef PrefixList_t::iterator PrefixIter_t;

typedef std::list<std::string> OriginList_t;
typedef PrefixList_t::iterator OriginIter_t;
typedef OriginList_t OriginSet_t;

typedef std::map<std::string, std::string> XmlAttrMap_t;
typedef XmlAttrMap_t::iterator XmlAttrIter_t;

typedef std::list<PhasBgpMsg *> BgpMsgList_t;
typedef BgpMsgList_t::iterator BgpMsgIter_t;

typedef std::map<std::string, phas_origin_t> PeerOriginMap_t;
typedef PeerOriginMap_t::iterator PeerOriginIter_t;

typedef std::map<std::string, PeerOriginMap_t> PrefixPeerMap_t;
typedef PrefixPeerMap_t::iterator PrefixPeerIter_t;

typedef std::list<PhasPrefix *> PhasPrefixList_t;
typedef PhasPrefixList_t::iterator PhasPrefixIter_t;

typedef std::list<PhasEvent *> PhasEventList_t;
typedef PhasEventList_t::iterator PhasEventIter_t;

typedef std::map<time_t, PhasEventList_t> PhasEventMap_t;
typedef PhasEventMap_t::iterator PhasEventMapIter_t;

#endif
