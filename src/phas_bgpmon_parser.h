/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _PHAS_BGPMON_PARSER_H
#define _PHAS_BGPMON_PARSER_H

#include <expat.h>

#include "phas_defs.h"

class PhasBgpMsg;

class PhasBgpmonParser
{
  // Types and enums
  private:
    typedef enum 
    {
      PHAS_MESSAGE_PRE_INIT = 0,
      PHAS_MESSAGE_START,
      PHAS_MESSAGE_ROOT,
      PHAS_MESSAGE_TIMESTAMP,
      PHAS_MESSAGE_ASCII,
      PHAS_MESSAGE_UPDATE,
      PHAS_MESSAGE_PREFIX,
      PHAS_MESSAGE_AS_PATH,
      PHAS_MESSAGE_PEERING,
      PHAS_MESSAGE_SRC_ADDR,
      PHAS_MESSAGE_SRC_AS,
      PHAS_MESSAGE_SRC_PORT,
      PHAS_MESSAGE_DST_ADDR,
      PHAS_MESSAGE_DST_AS,
      PHAS_MESSAGE_DST_PORT,
      PHAS_MESSAGE_WITHDRAW_PREFIX,
      PHAS_MESSAGE_ANNOUNCE_PREFIX,
      PHAS_MESSAGE_DONE
    } phas_msg_state_e;
  // Member Variables
  private:
    bool m_bTextCapture;
    int m_iFD;
    phas_msg_state_e m_eState;
    XML_Parser m_pParser;
    PhasBgpMsg *m_pMessage;
    BgpMsgList_t m_oMsgList;
    std::string m_sText;

  // Methods
  public:
    PhasBgpmonParser();
    virtual ~PhasBgpmonParser();

    bool init(int p_iFD);

    XML_Parser getParser();
    void setParser(XML_Parser p_pParser);

    bool getTextCapture();
    void setTextCapture(bool p_bTextCapture);

    phas_msg_state_e getState();
    void setState(phas_msg_state_e p_eState);

    bool start(std::string &p_sTag, XmlAttrMap_t &p_oAttrMap);
    bool end(std::string &p_sTag);

    bool processText(std::string &p_sText);

    bool isValidWithdraw(XmlAttrMap_t &p_oAttrMap);
    bool isValidAnnounce(XmlAttrMap_t &p_oAttrMap);

    bool getNextUpdate(PhasBgpMsg &p_oMsg);
    bool getNextMessage(PhasBgpMsg &p_oMsg);

    static bool makeAttrMap(const char **p_ppAttrs, XmlAttrMap_t &p_oAttrMap);
};

#endif
