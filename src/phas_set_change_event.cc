/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <sstream>

#include "phas_set_change_event.h"
#include "phas_defs.h"
#include "ps_logger.h"
#include "ps_defs.h"

using namespace std;

const string PhasSetChangeEvent::s_sEventTypeName("PHAS_SET_CHANGE_EVENT");

PhasSetChangeEvent::PhasSetChangeEvent(std::string &p_sPrefix,
                                       time_t p_tMsgTime,
                                       phas_origin_type_e p_eOriginType,
                                       phas_set_change_type_e p_eChangeType,
                                       unsigned int p_uASN,
                                       OriginSet_t &p_oNewOriginSet)
  : PhasEvent(p_sPrefix, p_tMsgTime),
    m_eOriginType(p_eOriginType),
    m_eChangeType(p_eChangeType),
    m_uASN(p_uASN),
    m_oNewOriginSet(p_oNewOriginSet)
{

}

PhasSetChangeEvent::~PhasSetChangeEvent()
{

}

phas_origin_type_e PhasSetChangeEvent::getOriginType()
{
  return m_eOriginType;
}

phas_set_change_type_e PhasSetChangeEvent::getChangeType()
{
  return m_eChangeType;
}

unsigned int PhasSetChangeEvent::getASN()
{
  return m_uASN;
}

const std::string &PhasSetChangeEvent::getEventType()
{
  return s_sEventTypeName;
}

std::string PhasSetChangeEvent::toString()
{
  return "";
}

std::string PhasSetChangeEvent::toLegacyString()
{
  ostringstream oSS;
  oSS << msgTime()
      << "|"
      << whenDetected()
      << "|"
      << getDelay()
      << "|"
      << getPrefix()
      << "|";
  oSS << ((PHAS_ORIGIN_ORIGIN == getOriginType()) ? "Origin" : "LastHop");
  oSS << "|";
  oSS << ((PHAS_SET_CHANGE_ANNOUNCE == getChangeType()) ? "Added" : "Removed");
  oSS << "|";
  if (0 == getASN())
  {
    oSS << "";
  }
  else
  {
    oSS << getASN();
  }

  oSS << "|";
  bool bFirst = true;
  for (OriginIter_t tIter = m_oNewOriginSet.begin();
       m_oNewOriginSet.end() != tIter;
       tIter++)
  {
    if (!bFirst)
    {
      oSS << ",";
    }
    oSS << *tIter;
    bFirst = false;
  }

  return oSS.str();
}

bool PhasSetChangeEvent::execute()
{
  return false;
}
