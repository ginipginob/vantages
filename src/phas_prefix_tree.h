/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _PHAS_PREFIX_TREE_H
#define _PHAS_PREFIX_TREE_H

#include "phas_prefix.h"
#include "phas_defs.h"

class PhasPrefixTree
{
  // Member Variables
  private:
//    PhasPrefixList_t m_oRoots;
    PhasPrefix m_oRoot;

  // Methods
  public:
    PhasPrefixTree();
    virtual ~PhasPrefixTree();

    bool initTopRoots();

    bool add(std::string &p_sPrefix);
    bool add(PhasPrefix &p_oPrefix);

    PhasPrefix *find(std::string &p_sPrefix, PhasPrefix *p_pRoot = NULL);
    PhasPrefix *find(PhasPrefix &p_oPrefix, PhasPrefix *p_pRoot = NULL);

    PhasPrefix *findParent(std::string &p_sPrefix, PhasPrefix *p_pRoot = NULL);
    PhasPrefix *findParent(PhasPrefix &p_oPrefix, PhasPrefix *p_pRoot = NULL);

    bool remove(std::string &p_sPrefix, PhasPrefix *p_pRoot = NULL);
    bool remove(PhasPrefix &p_oPrefix, PhasPrefix *p_pRoot = NULL);

    bool getPredList(std::string &p_sPrefix,
                     PrefixList_t &p_oOut,
                     bool p_bExcludeFake = true);
    bool getPredList(PhasPrefix &p_oPrefix,
                     PrefixList_t &p_oOut,
                     bool p_bExcludeFake = true);

    void printTree(bool p_bIncludeFake = false);
    void printSubTree(PhasPrefix &p_oRoot, int p_iDepth, bool p_bIncludeFake);
};

#endif
