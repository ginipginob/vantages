/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _PHAS_APP_H
#define _PHAS_APP_H

#include "phas_prefix_tree.h"
#include "phas_event_queue.h"
#include "phas_event_mgr.h"
#include "phas_bgpmon_parser.h"
#include "ps_app.h"
#include "phas_defs.h"

class HttpListenerCtx;
class PsCryptMgr;
class PhasSubAllocEvent;

class PhasApp : public PsApp
{
  // Member Variables
  private:
    static const char *s_szHttpPath;

    int m_iFD;
    bool m_bInit;
    bool m_bPromiscAlert;
    PrefixPeerMap_t m_oOriginPrefixMap;
    PrefixPeerMap_t m_oNextHopPrefixMap;
    PhasPrefixTree m_oPrefixTree;
    PhasEventMgr m_oEventMgr;
    PhasEventQueue m_oEventQueue;
    PhasBgpmonParser m_oParser;

  // Methods
  public:
    PhasApp();
    virtual ~PhasApp();

    virtual const char *getHttpPath();
    virtual const char *getHttpPass();
    virtual HttpListenerCtx *createCtx();
    virtual bool enabled();

    bool connect();
    bool disconnect();
    bool isConnected();

    virtual bool init();
    virtual bool execute();

    bool applyUpdate(PhasBgpMsg &p_oMsg);
    bool applyUpdate(PhasBgpMsg &p_oMsg, unsigned int p_uAS, PrefixPeerMap_t &p_oPrefixMap, phas_origin_type_e p_eType);

    bool isInteresting(std::string &p_sPrefix);

    bool getOriginSet(std::string &p_sPrefix,
                      PrefixPeerMap_t &p_oPrefixMap,
                      OriginSet_t &p_oOut);
    bool getPredList(std::string &p_sPrefix,
                     PrefixList_t &p_oOut);
    bool subAllocAlarm(std::string &p_sPrefix,
                       time_t p_tMsgTime,
                       int p_iWait,
                       bool p_bAnnouncement);
    bool subAllocAlarmOld(std::string &p_sPrefix,
                       time_t p_tMsgTime,
                       int p_iWait,
                       bool p_bAnnouncement);
};

#endif

