/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <expat.h>

#include "phas_bgpmon_parser.h"
#include "phas_bgp_msg.h"
#include "ps_config.h"
#include "ps_defs.h"
#include "ps_logger.h"

using namespace std;

void _start_hndl(void *data, const char *el, const char **attr)
{
  PhasBgpmonParser *pParser = (PhasBgpmonParser *) data;
  XmlAttrMap_t oAttrMap;
  string sTag = el;

  if (NULL == pParser)
  {
    ps_elog(PSL_CRITICAL, "Unable to find parser.\n");
  }
  else if (!PhasBgpmonParser::makeAttrMap(attr, oAttrMap))
  {
    ps_elog(PSL_ERROR, "Unable to create attr map.\n");
  }
  else if (!pParser->start(sTag, oAttrMap))
  {
    ps_elog(PSL_DEBUG, "Unable to parse tag: '%s'\n", sTag.c_str());
  }
}

void _end_hndl(void *data, const char *el)
{
  PhasBgpmonParser *pParser = (PhasBgpmonParser *) data;
  string sTag = el;

  if (NULL == pParser)
  {
    ps_elog(PSL_CRITICAL, "Unable to find parser.\n");
  }
  else if (!pParser->end(sTag))
  {
    ps_elog(PSL_DEBUG, "Unable to parse tag: '%s'\n", sTag.c_str());
  }
}

void _char_hndl(void *data, const char *txt, int txtlen)
{
  PhasBgpmonParser *pParser = (PhasBgpmonParser *) data;

  if (NULL == pParser)
  {
    ps_elog(PSL_CRITICAL, "Unable to find parser.\n");
  }
  else if (pParser->getTextCapture())
  {
    string sText(txt, txtlen);
    if (!pParser->processText(sText))
    {
      ps_elog(PSL_ERROR, "Unable to capture text: '%s'\n", sText.c_str());
    }
  }
}

void _proc_hndl(void *data, const char *target, const char *pidata)
{
/*
  fprintf(stderr, "\n%4d: Processing Instruction - ", Eventcnt++);
  printcurrent((XML_Parser) data);
*/
}

PhasBgpmonParser::PhasBgpmonParser()
  : m_bTextCapture(false),
    m_iFD(-1),
    m_eState(PHAS_MESSAGE_PRE_INIT),
    m_pParser(NULL),
    m_pMessage(NULL)
{

}

PhasBgpmonParser::~PhasBgpmonParser()
{
  if (-1 != m_iFD)
  {
//    close(m_iFD);
    m_iFD = -1;
  }

  setParser(NULL);

  while (!m_oMsgList.empty())
  {
    PhasBgpMsg *pMsg = m_oMsgList.front();
    m_oMsgList.pop_front();
    delete pMsg;
  }

  if (NULL != m_pMessage)
  {
    delete m_pMessage;
    m_pMessage = NULL;
  }
}

bool PhasBgpmonParser::init(int p_iFD)
{
  bool bRet = false;

/*
  if (-1 != m_iFD)
  {
    close(m_iFD);
    m_iFD = -1;
  }
*/
  m_iFD = p_iFD;

  XML_Parser pParser = NULL;
  setParser(NULL);
  if (NULL == (pParser = XML_ParserCreate(NULL)))
  {
    ps_elog(PSL_CRITICAL, "Unable to create XML parser.\n");
//    close(m_iFD);
    m_iFD = -1;
  }
  else
  {
    setParser(pParser);
//    XML_UseParserAsHandlerArg(m_pParser);
    XML_SetUserData(m_pParser, this);
    XML_SetElementHandler(m_pParser, _start_hndl, _end_hndl);
    XML_SetCharacterDataHandler(m_pParser, _char_hndl);
//    XML_SetProcessingInstructionHandler(m_pParser, _proc_hndl);

    setState(PHAS_MESSAGE_START);

    bRet = true;
#ifdef _BGPMON_XML_WRAP
    int iDone = 0;
    if (!XML_Parse(m_pParser, "<xml>", strlen("<xml>"), iDone))
    {
      ps_elog(PSL_CRITICAL, "Parse error at line %d:\n%s\n",
              XML_GetCurrentLineNumber(m_pParser),
              XML_ErrorString(XML_GetErrorCode(m_pParser)));
//      close(m_iFD);
      m_iFD = -1;
      setParser(NULL);
      bRet = false;
    }
#endif

  }

  return bRet;
}

XML_Parser PhasBgpmonParser::getParser()
{
  return m_pParser;
}

void PhasBgpmonParser::setParser(XML_Parser p_pParser)
{
  if (NULL != m_pParser)
  {
    XML_ParserFree(m_pParser);
    m_pParser = NULL;
  }

  m_pParser = p_pParser;
}

inline PhasBgpmonParser::phas_msg_state_e PhasBgpmonParser::getState()
{
  return m_eState;
}

inline void PhasBgpmonParser::setState(PhasBgpmonParser::phas_msg_state_e p_eState)
{
  m_eState = p_eState;
}

bool PhasBgpmonParser::start(string &p_sTag, XmlAttrMap_t &p_oAttrMap)
{
  bool bRet = false;

  // Note, this is a switch (instead of a map lookup) so we can 
  // check state transitions
  switch(getState())
  {
    case PHAS_MESSAGE_START:
    case PHAS_MESSAGE_DONE:
      if (p_sTag == BGPMON_BGP_MESSAGE_TAG)
      {
        setState(PHAS_MESSAGE_ROOT);
        m_pMessage = new PhasBgpMsg();
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_ROOT:
      if (p_sTag == BGPMON_ASCII_TAG)
      {
        setState(PHAS_MESSAGE_ASCII);
        bRet = true;
      }
      else if (p_sTag == BGPMON_PEERING_TAG)
      {
        setState(PHAS_MESSAGE_PEERING);
        bRet = true;
      }
      else if (p_sTag == BGPMON_TIMESTAMP_TAG)
      {
        setState(PHAS_MESSAGE_TIMESTAMP);
        setTextCapture(true);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_PEERING:
      if (p_sTag == BGPMON_SRC_ADDR_TAG)
      {
        setState(PHAS_MESSAGE_SRC_ADDR);
        setTextCapture(true);
        bRet = true;
      }
      else if (p_sTag == BGPMON_SRC_AS_TAG)
      {
        setState(PHAS_MESSAGE_SRC_AS);
        setTextCapture(true);
        bRet = true;
      }
      else if (p_sTag == BGPMON_SRC_PORT_TAG)
      {
        setState(PHAS_MESSAGE_SRC_PORT);
        setTextCapture(true);
        bRet = true;
      }
      else if (p_sTag == BGPMON_DST_ADDR_TAG)
      {
        setState(PHAS_MESSAGE_DST_ADDR);
        setTextCapture(true);
        bRet = true;
      }
      else if (p_sTag == BGPMON_DST_AS_TAG)
      {
        setState(PHAS_MESSAGE_DST_AS);
        setTextCapture(true);
        bRet = true;
      }
      else if (p_sTag == BGPMON_DST_PORT_TAG)
      {
        setState(PHAS_MESSAGE_DST_PORT);
        setTextCapture(true);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_ASCII:
      if (p_sTag == BGPMON_UPDATE_TAG)
      {
        setState(PHAS_MESSAGE_UPDATE);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_UPDATE:
      if (p_sTag == BGPMON_PREFIX_TAG)
      {
        if (isValidWithdraw(p_oAttrMap))
        {
          setState(PHAS_MESSAGE_WITHDRAW_PREFIX);
          setTextCapture(true);
          bRet = true;
        }
        else if (isValidAnnounce(p_oAttrMap))
        {
          setState(PHAS_MESSAGE_ANNOUNCE_PREFIX);
          setTextCapture(true);
          bRet = true;
        }
      }
      else if (p_sTag == BGPMON_AS_PATH_TAG)
      {
        setState(PHAS_MESSAGE_AS_PATH);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_AS_PATH:
      if (p_sTag == BGPMON_AS_TAG)
      {
        setTextCapture(true);
        bRet = true;
      }
      break;
    default:
      break;
  }

  if (!bRet)
  {
    ps_elog(PSL_DEBUG, "Invalid state transition from state: '%d' with tag '%s'\n", getState(), p_sTag.c_str());
  }

  return bRet;
}

bool PhasBgpmonParser::end(string &p_sTag)
{
  bool bRet = false;

  // Note, this is a switch (instead of a map lookup) so we can 
  // check state transitions
  switch(getState())
  {
    case PHAS_MESSAGE_ROOT:
      if (p_sTag == BGPMON_BGP_MESSAGE_TAG)
      {
        setState(PHAS_MESSAGE_DONE);
        m_oMsgList.push_back(m_pMessage);
        m_pMessage = NULL;
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_PEERING:
      if (p_sTag == BGPMON_PEERING_TAG)
      {
        setState(PHAS_MESSAGE_ROOT);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_TIMESTAMP:
      if (p_sTag == BGPMON_TIMESTAMP_TAG)
      {
        setTextCapture(false);
        setState(PHAS_MESSAGE_ROOT);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_SRC_ADDR:
      if (p_sTag == BGPMON_SRC_ADDR_TAG)
      {
        setTextCapture(false);
        setState(PHAS_MESSAGE_PEERING);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_SRC_AS:
      if (p_sTag == BGPMON_SRC_AS_TAG)
      {
        setTextCapture(false);
        setState(PHAS_MESSAGE_PEERING);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_SRC_PORT:
      if (p_sTag == BGPMON_SRC_PORT_TAG)
      {
        setTextCapture(false);
        setState(PHAS_MESSAGE_PEERING);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_DST_ADDR:
      if (p_sTag == BGPMON_DST_ADDR_TAG)
      {
        setTextCapture(false);
        setState(PHAS_MESSAGE_PEERING);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_DST_AS:
      if (p_sTag == BGPMON_DST_AS_TAG)
      {
        setTextCapture(false);
        setState(PHAS_MESSAGE_PEERING);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_DST_PORT:
      if (p_sTag == BGPMON_DST_PORT_TAG)
      {
        setTextCapture(false);
        setState(PHAS_MESSAGE_PEERING);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_ASCII:
      if (p_sTag == BGPMON_ASCII_TAG)
      {
        setState(PHAS_MESSAGE_ROOT);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_UPDATE:
      if (p_sTag == BGPMON_UPDATE_TAG)
      {
        setState(PHAS_MESSAGE_ASCII);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_WITHDRAW_PREFIX:
    case PHAS_MESSAGE_ANNOUNCE_PREFIX:
      if (p_sTag == BGPMON_PREFIX_TAG)
      {
        setTextCapture(false);
        setState(PHAS_MESSAGE_UPDATE);
        bRet = true;
      }
      break;
    case PHAS_MESSAGE_AS_PATH:
      if (p_sTag == BGPMON_AS_TAG)
      {
        setTextCapture(false);
        bRet = true;
      }
      else if (p_sTag == BGPMON_AS_PATH_TAG)
      {
        setState(PHAS_MESSAGE_UPDATE);
        bRet = true;
      }
      break;
    default:
      break;
  }

  if (!bRet)
  {
    ps_elog(PSL_DEBUG, "Invalid state transition from state: '%d' with tag '%s'\n", getState(), p_sTag.c_str());
  }

  return bRet;
}

bool PhasBgpmonParser::processText(std::string &p_sText)
{
  bool bRet = false;

  if (!getTextCapture())
  {
    ps_elog(PSL_ERROR, "Not able to capture text in this mode.\n");
  }
  else if (NULL == m_pMessage)
  {
    ps_elog(PSL_ERROR, "No message set.\n");
  }
  else
  {
    m_sText += p_sText;
    bRet = true;
  }

  return bRet;
}

bool PhasBgpmonParser::isValidWithdraw(XmlAttrMap_t &p_oAttrMap)
{
  bool bRet = false;

  XmlAttrIter_t tLabel = p_oAttrMap.find("label");
  XmlAttrIter_t tAfi = p_oAttrMap.find("afi_value");
  XmlAttrIter_t tSafi = p_oAttrMap.find("safi_value");
  if (p_oAttrMap.end() != tLabel
      && (tLabel->second) == "WITH"
      && p_oAttrMap.end() != tAfi
      && (tAfi->second) == "1"
      && p_oAttrMap.end() != tSafi
      && (tSafi->second) == "1")
  {
    bRet = true;
  }

  return bRet;
}

bool PhasBgpmonParser::isValidAnnounce(XmlAttrMap_t &p_oAttrMap)
{
  bool bRet = false;

  XmlAttrIter_t tLabel = p_oAttrMap.find("label");
  XmlAttrIter_t tAfi = p_oAttrMap.find("afi_value");
  XmlAttrIter_t tSafi = p_oAttrMap.find("safi_value");
  if (p_oAttrMap.end() != tLabel
      && ((tLabel->second) == "DPATH"
          || (tLabel->second) == "NANN")
      && p_oAttrMap.end() != tAfi
      && (tAfi->second) == "1"
      && p_oAttrMap.end() != tSafi
      && (tSafi->second) == "1")
  {
    bRet = true;
  }

  return bRet;
}

bool PhasBgpmonParser::getNextUpdate(PhasBgpMsg &p_oMsg)
{
  bool bRet = false;

  while ((bRet = getNextMessage(p_oMsg)) && !p_oMsg.isUpdate())
  {
  }

  return bRet;
}

bool PhasBgpmonParser::getNextMessage(PhasBgpMsg &p_oMsg)
{
  bool bRet = false;

  if (NULL == m_pParser)
  {
    ps_elog(PSL_CRITICAL, "Unable to get message without initialized parser (call init()).\n");
  }
  else if (-1 == m_iFD)
  {
    ps_elog(PSL_CRITICAL, "Unable to get message without initialized FD (call init()).\n");
  }
  else if (!m_oMsgList.empty())
  {
    PhasBgpMsg *pMsg = m_oMsgList.front();
    m_oMsgList.pop_front();
    p_oMsg = *pMsg;
    delete pMsg;
    bRet = true;
  }
  else
  {
    int iDone = 0;
    char pBuff[_PHAS_XML_BUFF_SIZE];

    while (!iDone && m_oMsgList.empty())
    {
      memset(pBuff, 0, _PHAS_XML_BUFF_SIZE);
      ssize_t iLen = read(m_iFD, pBuff, _PHAS_XML_BUFF_SIZE - 1);
      if (iLen < 0)
      {
        ps_elog(PSL_CRITICAL, "Read error: '%s'\n", strerror(errno));
        iDone = 1;
        bRet = false;
      }
      else if (iLen == 0)
      {
        iDone = 1;
#ifdef _BGPMON_XML_WRAP
        if (! XML_Parse(m_pParser, "</xml>", strlen("</xml>"), iDone))
        {
          ps_elog(PSL_ERROR, "Parse error at line %d:\n%s\n",
                  XML_GetCurrentLineNumber(m_pParser),
                  XML_ErrorString(XML_GetErrorCode(m_pParser)));
        }
#endif
      }
      else if (!XML_Parse(m_pParser, pBuff, iLen, iDone))
      {
        ps_elog(PSL_ERROR, "Parse error at line %d:\n%s\n",
                XML_GetCurrentLineNumber(m_pParser),
                XML_ErrorString(XML_GetErrorCode(m_pParser)));
      }
    }

    if (!m_oMsgList.empty())
    {
      PhasBgpMsg *pMsg = m_oMsgList.front();
      m_oMsgList.pop_front();
      p_oMsg = *pMsg;
      delete pMsg;
      bRet = true;
    }
  }

  return bRet;
}

bool PhasBgpmonParser::makeAttrMap(const char **p_ppAttrs, XmlAttrMap_t &p_oAttrMap)
{
  bool bRet = false;

  if (NULL == p_ppAttrs)
  {
    ps_elog(PSL_ERROR, "Unable to convert NULL attr list.\n");
  }
  else
  {
    const char *szLast = NULL;
    const char *szNext = p_ppAttrs[0];
    for (int i = 0;
         NULL != szNext;
         i++, szNext = p_ppAttrs[i])
    {
      if (NULL == szLast)
      {
        szLast = szNext;
      }
      else
      {
        string sKey = szLast;
        string sValue = szNext;
        p_oAttrMap[sKey] = sValue;

        szLast = NULL;
        szNext = NULL;
      }
    }

    if (NULL == szLast
        && NULL == szNext)
    {
      bRet = true;
    }
    else
    {
      ps_elog(PSL_ERROR, "Partial attr list.\n");
    }
  }

  return bRet;
}

bool PhasBgpmonParser::getTextCapture()
{
  return m_bTextCapture;
}

void PhasBgpmonParser::setTextCapture(bool p_bTextCapture)
{
  if (m_bTextCapture && !p_bTextCapture)
  {
    switch (getState())
    {
      case PHAS_MESSAGE_WITHDRAW_PREFIX:
        m_pMessage->addWithdrawn(m_sText);
        break;
      case PHAS_MESSAGE_ANNOUNCE_PREFIX:
        m_pMessage->addAnnounced(m_sText);
        break;
      case PHAS_MESSAGE_AS_PATH:
        m_pMessage->addNextAS(m_sText);
        break;
      case PHAS_MESSAGE_SRC_ADDR:
        m_pMessage->setSrcAddr(m_sText);
        break;
      case PHAS_MESSAGE_SRC_AS:
        m_pMessage->setSrcAS(m_sText);
        break;
      case PHAS_MESSAGE_SRC_PORT:
        m_pMessage->setSrcPort(m_sText);
        break;
      case PHAS_MESSAGE_DST_ADDR:
        m_pMessage->setDstAddr(m_sText);
        break;
      case PHAS_MESSAGE_DST_AS:
        m_pMessage->setDstAS(m_sText);
        break;
      case PHAS_MESSAGE_DST_PORT:
        m_pMessage->setDstPort(m_sText);
        break;
      case PHAS_MESSAGE_TIMESTAMP:
        m_pMessage->setTimestamp((time_t) strtol(m_sText.c_str(), NULL, 10));
        break;
      default:
        ps_elog(PSL_ERROR, "Unknown state: '%d'\n", getState());
        break;
    }
  }

  m_bTextCapture = p_bTextCapture;
  m_sText = "";
}


