/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>

#include "phas_bgp_msg.h"
#include "ps_defs.h"

using namespace std;

PhasBgpMsg::PhasBgpMsg()
  : m_tTimestamp(0)
{

}

PhasBgpMsg::~PhasBgpMsg()
{

}

time_t PhasBgpMsg::getTimestamp()
{
  return m_tTimestamp;
}

void PhasBgpMsg::setTimestamp(time_t p_tTimestamp)
{
  m_tTimestamp = p_tTimestamp;
}

std::string PhasBgpMsg::getMonitorID()
{
  return m_sSrcAddr
         + "-"
         + m_sSrcAS
         + "-"
         + m_sSrcPort
         + "-"
         + m_sDstAddr
         + "-"
         + m_sDstAS
         + "-"
         + m_sDstPort;
}

std::string &PhasBgpMsg::getSrcAddr()
{
  return m_sSrcAddr;
}

void PhasBgpMsg::setSrcAddr(std::string &p_sSrcAddr)
{
  m_sSrcAddr = p_sSrcAddr;
}

std::string &PhasBgpMsg::getSrcAS()
{
  return m_sSrcAS;
}

void PhasBgpMsg::setSrcAS(std::string &p_sSrcAS)
{
  m_sSrcAS = p_sSrcAS;
}

std::string &PhasBgpMsg::getSrcPort()
{
  return m_sSrcPort;
}

void PhasBgpMsg::setSrcPort(std::string &p_sSrcPort)
{
  m_sSrcPort = p_sSrcPort;
}

std::string &PhasBgpMsg::getDstAddr()
{
  return m_sDstAddr;
}

void PhasBgpMsg::setDstAddr(std::string &p_sDstAddr)
{
  m_sDstAddr = p_sDstAddr;
}

std::string &PhasBgpMsg::getDstAS()
{
  return m_sDstAS;
}

void PhasBgpMsg::setDstAS(std::string &p_sDstAS)
{
  m_sDstAS = p_sDstAS;
}

std::string &PhasBgpMsg::getDstPort()
{
  return m_sDstPort;
}

void PhasBgpMsg::setDstPort(std::string &p_sDstPort)
{
  m_sDstPort = p_sDstPort;
}

void PhasBgpMsg::addNextAS(std::string &p_sAS)
{
  if (m_sOrigin == "")
  {
    setOrigin(p_sAS);
  }
  else
  {
    setNextHop(getOrigin());
    setOrigin(p_sAS);
  }
}

std::string &PhasBgpMsg::getOrigin()
{
  return m_sOrigin;
}

void PhasBgpMsg::setOrigin(std::string &p_sOrigin)
{
  m_sOrigin = p_sOrigin;
}

std::string &PhasBgpMsg::getNextHop()
{
  return m_sNextHop;
}

void PhasBgpMsg::setNextHop(std::string &p_sNextHop)
{
  m_sNextHop = p_sNextHop;
}

PrefixIter_t PhasBgpMsg::beginWithdrawn()
{
  return m_oWithdrawnList.begin();
}

PrefixIter_t PhasBgpMsg::endWithdrawn()
{
  return m_oWithdrawnList.end();
}

size_t PhasBgpMsg::sizeWithdrawn()
{
  return m_oWithdrawnList.size();
}

void PhasBgpMsg::addWithdrawn(std::string &p_sPrefix)
{
  m_oWithdrawnList.push_back(p_sPrefix);
}

PrefixIter_t PhasBgpMsg::beginAnnounced()
{
  return m_oAnnouncedList.begin();
}

PrefixIter_t PhasBgpMsg::endAnnounced()
{
  return m_oAnnouncedList.end();
}

size_t PhasBgpMsg::sizeAnnounced()
{
  return m_oAnnouncedList.size();
}

void PhasBgpMsg::addAnnounced(std::string &p_sPrefix)
{
  m_oAnnouncedList.push_back(p_sPrefix);
}

bool PhasBgpMsg::isUpdate()
{
  return (sizeWithdrawn() > 0) || (sizeAnnounced() > 0);
}

