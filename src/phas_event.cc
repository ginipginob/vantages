/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>

#include "phas_event.h"
#include "phas_defs.h"
#include "ps_logger.h"
#include "ps_defs.h"

using namespace std;

PhasEvent::PhasEvent(std::string &p_sPrefix,
                     time_t p_tMsgTime,
                     time_t p_tWhenDetected /*= 0*/)
  : m_tMsgTime(p_tMsgTime),
    m_tWhenDetected(p_tWhenDetected),
    m_tDelay(0),
    m_sPrefix(p_sPrefix)
{
  if (0 == m_tWhenDetected)
  {
    m_tWhenDetected = time(NULL);
  }
}

PhasEvent::~PhasEvent()
{

}

std::string &PhasEvent::getPrefix()
{
  return m_sPrefix;
}

time_t PhasEvent::msgTime()
{
  return m_tMsgTime;
}

time_t PhasEvent::whenDetected()
{
  return m_tWhenDetected;
}

time_t PhasEvent::getDelay()
{
  return m_tDelay;
}

void PhasEvent::setDelay(time_t p_tDelay)
{
  m_tDelay = p_tDelay;
}

