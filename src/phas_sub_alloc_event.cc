/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>

#include <sstream>

#include "phas_sub_alloc_event.h"
#include "phas_app.h"
#include "phas_defs.h"
#include "ps_logger.h"
#include "ps_defs.h"

using namespace std;

const std::string PhasSubAllocEvent::s_sEventTypeName("PHAS_SUB_ALLOC_EVENT");

PhasSubAllocEvent::PhasSubAllocEvent(std::string &p_sProblemPrefix,
                                     std::string &p_sOwnerPrefix,
                                     time_t p_tMsgTime,
                                     bool p_bAnnounce,
                                     PrefixList_t &p_oPredList)
  : PhasEvent(p_sProblemPrefix, p_tMsgTime),
    m_bAnnounce(p_bAnnounce),
    m_sOwnerPrefix(p_sOwnerPrefix),
    m_oPredList(p_oPredList)
{

}

PhasSubAllocEvent::~PhasSubAllocEvent()
{

}

std::string &PhasSubAllocEvent::getProblemPrefix()
{
  return getPrefix();
}

std::string &PhasSubAllocEvent::getOwnerPrefix()
{
  return m_sOwnerPrefix;
}

const std::string &PhasSubAllocEvent::getEventType()
{
  return s_sEventTypeName;
}

std::string PhasSubAllocEvent::toString()
{
  return "";
}

std::string PhasSubAllocEvent::toLegacyString()
{
  ostringstream oSS;
  oSS << msgTime()
      << "|"
      << whenDetected()
      << "|"
      << getDelay()
      << "|"
      << getProblemPrefix()
      << "|"
      << "SubAllocation"
      << "|";

  oSS << ((m_bAnnounce) ? "Added" : "Removed");

  oSS << "|"
      << getOwnerPrefix()
      << "|";
  bool bSent = false;
  for (PrefixIter_t tIter = m_oPredList.begin();
       m_oPredList.end() != tIter;
       tIter++)
  {
    if (bSent)
    {
      oSS << ",";
    }
    bSent = true;
    oSS << *tIter;
  }

  return oSS.str();
}

bool PhasSubAllocEvent::execute()
{
  return false;
}
