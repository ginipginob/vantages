/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <string>

#include "phas_prefix.h"
#include "phas_defs.h"
#include "ps_logger.h"
#include "ps_defs.h"

using namespace std;

PhasPrefix::PhasPrefix()
  : m_bReal(true),
    m_iRefCount(0),
    m_uAddr(0),
    m_uMask(0),
    m_pParentPrefix(NULL)
{

}

PhasPrefix::PhasPrefix(std::string &p_sPrefix, bool p_bReal /*= true*/)
  : m_bReal(p_bReal),
    m_iRefCount(0),
    m_uAddr(0),
    m_uMask(0),
    m_pParentPrefix(NULL)
{
  init(p_sPrefix, p_bReal);
}

PhasPrefix::~PhasPrefix()
{
  for (PhasPrefixIter_t tIter = beginChildren();
       endChildren() != tIter;
       tIter++)
  {
    delete *tIter;
  }
  m_oSubPrefixList.clear();
}

bool PhasPrefix::init(std::string &p_sPrefix, bool p_bReal /*= true*/)
{
  bool bRet = false;

  size_t u = p_sPrefix.find('/');
  if (string::npos == u)
  {
    ps_elog(PSL_ERROR, "Unable to convert prefix '%s' because no '/' separator.\n", p_sPrefix.c_str());
  }
  else
  {
    uint8_t uMask = 0;
    struct in_addr tAddr;
    memset(&tAddr, 0, sizeof(tAddr));

    string sAddr = p_sPrefix.substr(0, u);
    string sMask = p_sPrefix.substr(u + 1);

    int iCount = 0;
    for (u = 0; u != string::npos; iCount++)
    {
      u = sAddr.find('.', u + 1);
    }

    for (int i = iCount; i < 4; i++)
    {
      sAddr += ".0";
    }

    if (1 != inet_pton(AF_INET, sAddr.c_str(), &tAddr))
    {
      ps_elog(PSL_ERROR, "Unable to parse addr portion: '%s'\n", sAddr.c_str());
    }
    else if (0 == (uMask = (uint8_t) strtol(sMask.c_str(), NULL, 10)) && sMask != "0")
    {
      ps_elog(PSL_ERROR, "Unable to parse mask portion: '%s'\n", sMask.c_str());
    }
    else
    {
      m_uAddr = ntohl(tAddr.s_addr);
      m_uMask = uMask;
      m_bReal = p_bReal;
      bRet = true;
    }
  }

  return bRet;
}

uint32_t PhasPrefix::getAddr()
{
  return m_uAddr;
}

uint8_t PhasPrefix::getMask()
{
  return m_uMask;
}


bool PhasPrefix::isValid()
{
  return (0 != m_uAddr && 0 != m_uMask);
}

PhasPrefix *PhasPrefix::getParent()
{
  return m_pParentPrefix;
}

bool PhasPrefix::setParent(PhasPrefix *p_pParentPrefix)
{
  bool bRet = false;

  if (NULL != p_pParentPrefix && !p_pParentPrefix->covers(*this))
  {
    ps_elog(PSL_CRITICAL, "Trying to add parent '%s' that does NOT cover this prefix: '%s'... aborting and setting NULL\n",
            p_pParentPrefix->str().c_str(),
            str().c_str());
    m_pParentPrefix = NULL;
  }
  else
  {
    m_pParentPrefix = p_pParentPrefix;
    bRet = true;
  }

  return bRet;
}

PhasPrefixIter_t PhasPrefix::beginChildren()
{
  return m_oSubPrefixList.begin();
}

PhasPrefixIter_t PhasPrefix::endChildren()
{
  return m_oSubPrefixList.end();
}

size_t PhasPrefix::sizeChildren()
{
  return m_oSubPrefixList.size();
}

bool PhasPrefix::addChild(PhasPrefix &p_oPrefix)
{
  bool bRet = false;

  if (!p_oPrefix.setParent(this))
  {
    ps_elog(PSL_CRITICAL, "Unable to add child because '%s' is not covered by use ('%s')\n", p_oPrefix.str().c_str(), str().c_str());
  }
  else
  {
    PhasPrefixList_t oSubList;

    PhasPrefix *pChild = NULL;
    PhasPrefixIter_t tIter;
    for (tIter = beginChildren();
         endChildren() != tIter;
         tIter++)
    {
      pChild = *tIter;
      if (p_oPrefix.covers(*pChild))
      {
        oSubList.push_back(pChild);
      }
    }

    m_oSubPrefixList.push_back(&p_oPrefix);
    p_oPrefix.addRef();

    bRet = true;
    for (tIter = oSubList.begin();
         oSubList.end() != tIter;
         tIter++)
    {
      pChild = *tIter;
      bRet = pChild->setParent(&p_oPrefix);
      p_oPrefix.addChild(*pChild);
      removeChild(*pChild, true);
    }
  }

  return bRet;
}

bool PhasPrefix::removeChild(PhasPrefix &p_oChildPrefix, bool p_bOverride /*= false*/)
{
  bool bRet = false;

bool bSent = false;
  for (PhasPrefixIter_t tIter = beginChildren();
       endChildren() != tIter;
       tIter++)
  {
    PhasPrefix *pChild = *tIter;
    if (NULL == pChild)
    {
      ps_elog(PSL_CRITICAL, "NULL in child list of prefix! Will try to save data structure by deleting!\n");
      m_oSubPrefixList.erase(tIter);
      break;
    }
    else if (pChild->equals(p_oChildPrefix))
    {
bSent = true;
      bRet = removeChild(tIter, p_bOverride);
      break;
    }
  }
/*
if (!bSent)
{
fprintf(stderr, "UNABLE TO FIND PREFIX '%s' TO REMOVE UNDER PREFIX: '%s'.\n", p_oChildPrefix.str().c_str(), str().c_str());
}
*/

  return bRet;
}

bool PhasPrefix::removeChild(PhasPrefixIter_t p_tIter, bool p_bOverride /*= false*/)
{
  bool bRet = false;

  PhasPrefix *pChild = NULL;

  if (endChildren() == p_tIter)
  {
    ps_elog(PSL_ERROR, "Unable to remove invlaid iterator (is at end of list).\n");
  }
  else if (NULL == (pChild = *p_tIter))
  {
    ps_elog(PSL_ERROR, "Unable to find child prefix (iterator returned NULL).\n");
  }
  else
  {
    pChild->subtractRef();

    if (p_bOverride)
    {
      m_oSubPrefixList.erase(p_tIter);
    }
    else if (0 >= pChild->getRefCount() && pChild->getReal())
    {
//      PhasPrefix *pParent = getParent();
      for (PhasPrefixIter_t tIter = pChild->beginChildren();
           pChild->endChildren() != tIter;
           tIter++)
      {
//        (*tIter)->setParent(pParent);
        (*tIter)->setParent(NULL);
        addChild(*(*tIter));
        (*tIter)->subtractRef();
      }
      pChild->flush();
      m_oSubPrefixList.erase(p_tIter);

//      if (!p_bOverride)
//      {
        delete pChild;
//      }

    }
    bRet = true;
  }

  return bRet;
}

bool PhasPrefix::flush()
{
  m_oSubPrefixList.clear();

  return true;
}

bool PhasPrefix::getReal()
{
  return (m_bReal || (getRefCount() > 1));
}

void PhasPrefix::setReal(bool p_bReal)
{
  m_bReal = p_bReal;
}

int PhasPrefix::getRefCount()
{
  return m_iRefCount;
}

void PhasPrefix::addRef()
{
  m_iRefCount++;
}

void PhasPrefix::subtractRef()
{
  m_iRefCount--;
}

bool PhasPrefix::equals(PhasPrefix &p_oPrefix)
{
  return (getAddr() == p_oPrefix.getAddr()
          && getMask() == p_oPrefix.getMask());
}

bool PhasPrefix::covers(PhasPrefix &p_oPrefix)
{
  bool bRet = false;

  if (getMask() > p_oPrefix.getMask())
  {
    ps_elog(PSL_DEBUG, "Candidate prefix has a shorter mask, so this prefix cannot cover it: %u >= %u\n", getMask(),  p_oPrefix.getMask());
  }
  else
  {
    uint32_t uNboAddr = (getAddr());
    uint32_t uNboMask = 0xffffffff;
    if (0 == getMask())
    {
      uNboMask = 0;
    }
    else
    {
      uNboMask = uNboMask << (32 - getMask());
    }

    uint32_t uNboSubAddr = (p_oPrefix.getAddr());
    uNboSubAddr = uNboSubAddr & uNboMask;

    ps_elog(PSL_DEBUG, "Comparing masks: %u < %u using 0x%x\n", getMask(),  p_oPrefix.getMask(), uNboMask);
    ps_elog(PSL_DEBUG, "Comparing addresses:\n\t%x\n\t%x\n", uNboAddr, uNboSubAddr);

    bRet = (uNboAddr == uNboSubAddr);
  }

  return bRet;
}

std::string PhasPrefix::str()
{
  string sRet;

  struct in_addr tAddr;
  char szAddr[17];
  char szMask[3];
  memset(szAddr, 0, 17);
  memset(szMask, 0, 3);

  memset(&tAddr, 0, sizeof(tAddr));
  tAddr.s_addr = htonl(m_uAddr);

  if (NULL == inet_ntop(AF_INET, &tAddr, szAddr, 16))
  {
    ps_elog(PSL_ERROR, "Unable to convert address: '%u': '%s'\n", (unsigned) m_uAddr, strerror(errno));
  }
  else
  {
    sprintf(szMask, "%u", (unsigned) m_uMask);
    sRet = szAddr;

    size_t u = 0;
    if (m_uMask < 25)
    {
      u = sRet.rfind('.');
      sRet.erase(u);
    }

    if (m_uMask < 17)
    {
      u = sRet.rfind('.');
      sRet.erase(u);
    }

    if (m_uMask < 9)
    {
      u = sRet.rfind('.');
      sRet.erase(u);
    }

    sRet += "/";
    sRet += szMask;
  }

  return sRet;
}

