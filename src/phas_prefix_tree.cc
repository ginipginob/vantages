/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <string.h>

#include <string>

#include "phas_prefix_tree.h"
#include "phas_prefix.h"
#include "phas_defs.h"
#include "ps_logger.h"
#include "ps_defs.h"

using namespace std;

PhasPrefixTree::PhasPrefixTree()
{
  string sRoot = "0/0";
  if (!m_oRoot.init(sRoot, false))
  {
    ps_elog(PSL_CRITICAL, "Unable to init false root w/ prefix: '%s'\n", sRoot.c_str());
  }
}

PhasPrefixTree::~PhasPrefixTree()
{
/*
  for (PhasPrefixIter_t tIter = m_oRoots.begin();
       m_oRoots.end() != tIter;
       tIter++)
  {
    delete *tIter;
  }
  m_oRoots.clear();
*/
}

bool PhasPrefixTree::initTopRoots()
{
  for (int i= 0; i < 256; i++)
  {
    char sz[13];
    memset(sz, 0, 13);
    sprintf(sz, "%d.0.0.0/8", i);
    string sTop = sz;
    PhasPrefix *pTop = new PhasPrefix(sTop);
    pTop->setReal(false);
    m_oRoot.addChild(*pTop);
    pTop->subtractRef();
  }

  return true;
}

bool PhasPrefixTree::add(std::string &p_sPrefix)
{
  bool bRet = false;

  PhasPrefix *pPrefix = new PhasPrefix(p_sPrefix);
  if (!pPrefix->isValid())
  {
    ps_elog(PSL_ERROR, "Unable to add invalid prefix: '%s'\n", p_sPrefix.c_str());
  }
  else
  {
    bRet = add(*pPrefix);
  }

  return bRet;
}

bool PhasPrefixTree::add(PhasPrefix &p_oPrefix)
{
  bool bRet = false;

  PhasPrefix *pParent = NULL;
  PhasPrefix *pExisting = find(p_oPrefix);
  if (NULL != pExisting)
  {
    pExisting->addRef();
    delete &p_oPrefix;
    bRet = true;
  }
  else if (NULL == (pParent = findParent(p_oPrefix)))
  {
    ps_elog(PSL_ERROR, "Unable to find parent for prefix: '%s'\n", p_oPrefix.str().c_str());
  }
  else
  {
    pParent->addChild(p_oPrefix);
    bRet = true;
  }

  return bRet;
}

PhasPrefix *PhasPrefixTree::find(std::string &p_sPrefix, PhasPrefix *p_pRoot /*= NULL*/)
{
  PhasPrefix *pRet = NULL;

  PhasPrefix *pPrefix = new PhasPrefix(p_sPrefix);
  if (!pPrefix->isValid())
  {
    ps_elog(PSL_ERROR, "Unable to add invalid prefix: '%s'\n", p_sPrefix.c_str());
  }
  else
  {
    pRet = find(*pPrefix, p_pRoot);
  }

  return pRet;
}

PhasPrefix *PhasPrefixTree::find(PhasPrefix &p_oPrefix, PhasPrefix *p_pRoot /*= NULL*/)
{
  PhasPrefix *pRet = NULL;

  PhasPrefixIter_t tBegin;
  PhasPrefixIter_t tEnd;
  if (NULL == p_pRoot)
  {
/*
    tBegin = m_oRoots.begin();
    tEnd = m_oRoots.end();
*/
    tBegin = m_oRoot.beginChildren();
    tEnd = m_oRoot.endChildren();
  }
  else if (p_pRoot->equals(p_oPrefix))
  {
    pRet = p_pRoot;
  }
  else
  {
    tBegin = p_pRoot->beginChildren();
    tEnd = p_pRoot->endChildren();
  }

  for (PhasPrefixIter_t tIter = tBegin;
       tEnd != tIter
       && NULL == pRet;
       tIter++)
  {
    pRet = find(p_oPrefix, *tIter);
  }

  return pRet;
}

PhasPrefix *PhasPrefixTree::findParent(std::string &p_sPrefix, PhasPrefix *p_pRoot /*= NULL*/)
{
  PhasPrefix *pRet = NULL;

  PhasPrefix *pPrefix = new PhasPrefix(p_sPrefix);
  if (!pPrefix->isValid())
  {
    ps_elog(PSL_ERROR, "Unable to add invalid prefix: '%s'\n", p_sPrefix.c_str());
  }
  else
  {
    pRet = findParent(*pPrefix, p_pRoot);
  }

  return pRet;
}

PhasPrefix *PhasPrefixTree::findParent(PhasPrefix &p_oPrefix, PhasPrefix *p_pRoot /*= NULL*/)
{
  PhasPrefix *pRet = NULL;

  PhasPrefixIter_t tBegin;
  PhasPrefixIter_t tEnd;
/*
  if (NULL == p_pRoot)
  {
    tBegin = m_oRoot.beginChildren();
    tEnd = m_oRoot.endChildren();
  }
  else
  {
    tBegin = p_pRoot->beginChildren();
    tEnd = p_pRoot->endChildren();
  }
*/
  if (NULL == p_pRoot)
  {
    p_pRoot = &m_oRoot;
  }
  tBegin = p_pRoot->beginChildren();
  tEnd = p_pRoot->endChildren();

  for (PhasPrefixIter_t tIter = tBegin;
       tEnd != tIter;
       tIter++)
  {
    PhasPrefix *pRoot = *tIter;
    if (pRoot->equals(p_oPrefix))
    {
      pRet = p_pRoot;
// fprintf(stderr, "-> -> The parent of '%s' is '%s'\n", p_oPrefix.str().c_str(), p_pRoot->str().c_str());
      break;
    }
    else if (pRoot->covers(p_oPrefix))
    {
// fprintf(stderr, "-> Found subtree for '%s' under '%s'\n", p_oPrefix.str().c_str(), pRoot->str().c_str());
      PhasPrefix *pSubRoot = findParent(p_oPrefix, *tIter);
      pRet = (NULL != pSubRoot) ? pSubRoot : pRoot;
/*
if (NULL == pSubRoot)
{
fprintf(stderr, "-> -> The parent of '%s' is '%s'\n", p_oPrefix.str().c_str(), pRoot->str().c_str());
}
*/
      break;
    }
  }

  return pRet;
}

bool PhasPrefixTree::remove(std::string &p_sPrefix, PhasPrefix *p_pRoot /*= NULL*/)
{
  bool bRet = false;

  PhasPrefix oPrefix(p_sPrefix);
  if (!oPrefix.isValid())
  {
    ps_elog(PSL_ERROR, "Unable to remove invalid prefix: '%s'\n", p_sPrefix.c_str());
  }
  else
  {
    bRet = remove(oPrefix, p_pRoot);
  }

  return bRet;
}

bool PhasPrefixTree::remove(PhasPrefix &p_oPrefix, PhasPrefix *p_pRoot /*= NULL*/)
{
  bool bRet = false;

  PhasPrefix *pParent = findParent(p_oPrefix, p_pRoot);
  if (NULL == pParent)
  {
    ps_elog(PSL_ERROR, "Unable to find parent of prefix: '%s'\n", p_oPrefix.str().c_str());
  }
  else if (!pParent->removeChild(p_oPrefix))
  {
    ps_elog(PSL_INFO, "Unable to remove child: '%s' of parent: '%s'\n", p_oPrefix.str().c_str(), pParent->str().c_str());
  }
  else
  {
    bRet = true;
  }

  return bRet;
}

bool PhasPrefixTree::getPredList(std::string &p_sPrefix,
                                 PrefixList_t &p_oOut,
                                 bool p_bExcludeFake /*= true*/)
{
  bool bRet = false;

  PhasPrefix oPrefix(p_sPrefix);
  if (!oPrefix.isValid())
  {
    ps_elog(PSL_ERROR, "Unable to create prefix from string: '%s'\n", p_sPrefix.c_str());
  }
  else
  {
    bRet = getPredList(oPrefix, p_oOut, p_bExcludeFake);
  }

  return bRet;
}

bool PhasPrefixTree::getPredList(PhasPrefix &p_oPrefix,
                                 PrefixList_t &p_oOut,
                                 bool p_bExcludeFake /*= true*/)
{
  bool bRet = false;

  PhasPrefix *pTarget = find(p_oPrefix);
  PhasPrefix *pTmp = NULL;

  if (NULL == pTarget)
  {
    ps_elog(PSL_INFO, "Unable to find prefix: '%s'\n", p_oPrefix.str().c_str());
    bRet = true;
  }
  else
  {
    p_oOut.clear();
    while (NULL != (pTmp = pTarget->getParent()))
    {
      if (!p_bExcludeFake || pTmp->getReal())
      {
        p_oOut.push_front(pTmp->str());
      }
      pTarget = pTmp;
    }
    bRet = true;
  }

  return bRet;
}

void PhasPrefixTree::printTree(bool p_bIncludeFake /*= false*/)
{
/*
  for (PhasPrefixIter_t tIter = m_oRoots.begin();
       m_oRoots.end() != tIter;
       tIter++)
  {
    printSubTree(*(*tIter), 0, p_bIncludeFake);
  }
*/
  printSubTree(m_oRoot, 0, p_bIncludeFake);
}

void PhasPrefixTree::printSubTree(PhasPrefix &p_oPrefix, int p_iDepth, bool p_bIncludeFake)
{
  if (p_bIncludeFake || p_oPrefix.getReal())
  {
    for (int i = 0; i < p_iDepth; i++)
    {
      fprintf(stdout, "  ");
    }
    fprintf(stdout, "-> [%d] %s\n", p_oPrefix.getRefCount(), p_oPrefix.str().c_str());
  }

  for (PhasPrefixIter_t tIter = p_oPrefix.beginChildren();
       p_oPrefix.endChildren() != tIter;
       tIter++)
  {
    printSubTree(*(*tIter), p_iDepth + 1, p_bIncludeFake);
  }
}
