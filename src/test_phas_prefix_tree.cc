/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>

#include <string>

#include "phas_prefix_tree.h"
#include "phas_prefix.h"
#include "ps_logger.h"

using namespace std;

int main(int argc, char *argv[])
{
  string sPrefix1 = "131.179/16";
  string sPrefix2 = "131.179.96/24";
  string sPrefix3 = "192.168/16";
  string sPrefix4 = "192.168.1.1/32";
  string sPrefix5 = "192.168.1/24";
  string sPrefix6 = "192.168.9.128/25";
  string sPrefix7 = "10.168/16";
  string sPrefix8 = "10.168/16";
  string sPrefix9 = "10.168/16";
  string sPrefix10 = "10.168/16";
  string sPrefix11 = "10/8";
  string sPrefix12 = "131.179.96.128/25";

//  PsLogger::getInstance().setLevel(PSL_DEBUG);
  PsLogger::getInstance().setLevel(PSL_ERROR);

  PhasPrefixTree oTree;

  if (!oTree.initTopRoots())
  {
    fprintf(stderr, "Unable to init roots.\n");
    exit(1);
  }

  PhasPrefix *p = new PhasPrefix(sPrefix1);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix2);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix3);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix4);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix5);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix6);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix7);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix8);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix9);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix10);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix11);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  p = new PhasPrefix(sPrefix12);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  oTree.printTree();

  fprintf(stdout, "Removing: '%s'\n", sPrefix5.c_str());
  PhasPrefix oPrefix5(sPrefix5);
  if (!oTree.remove(oPrefix5))
  {
    fprintf(stderr, "Unable to remove prefix: '%s'\n", oPrefix5.str().c_str());
    exit(1);
  }

  fprintf(stdout, "Removing: '%s'\n", sPrefix1.c_str());
  PhasPrefix oPrefixR(sPrefix1);
  if (!oTree.remove(oPrefixR))
  {
    fprintf(stderr, "Unable to remove prefix: '%s'\n", oPrefixR.str().c_str());
    exit(1);
  }
  p = new PhasPrefix(sPrefix1);
  if (!oTree.add(*p))
  {
    fprintf(stderr, "Unable to add prefix: '%s'\n", p->str().c_str());
    delete p;
    exit(1);
  }

  fprintf(stdout, "Removing: '%s' (But ref count should leave it in the tree)\n", sPrefix8.c_str());
  PhasPrefix oPrefix8(sPrefix8);
  if (!oTree.remove(oPrefix8))
  {
    fprintf(stderr, "Unable to remove prefix: '%s'\n", oPrefix8.str().c_str());
    exit(1);
  }

  fprintf(stdout, "Removing: '%s'\n", sPrefix1.c_str());
  PhasPrefix oPrefix1(sPrefix1);
  if (!oTree.remove(oPrefix1))
  {
    fprintf(stderr, "Unable to remove prefix: '%s'\n", oPrefix1.str().c_str());
    exit(1);
  }


  oTree.printTree();

  PrefixList_t oPredList;
  if (!oTree.getPredList(sPrefix4, oPredList))
  {
    fprintf(stderr, "Unable to get pred list for '%s'\n", sPrefix4.c_str());
    exit(1);
  }
  else
  {
    fprintf(stdout, "Pred list for '%s' excluding fakes is:\n\t", sPrefix4.c_str());
    for (PrefixIter_t tPIter = oPredList.begin(); oPredList.end() != tPIter; tPIter++)
    {
      fprintf(stdout, "'%s'->", (*tPIter).c_str());
    }
    fprintf(stdout, "\n");
  }

  if (!oTree.getPredList(sPrefix4, oPredList, false))
  {
    fprintf(stderr, "Unable to get pred list (WITH FAKES) for '%s'\n", sPrefix4.c_str());
    exit(1);
  }
  else
  {
    fprintf(stdout, "Pred list for '%s' with fakes is:\n\t", sPrefix4.c_str());
    for (PrefixIter_t tPIter = oPredList.begin(); oPredList.end() != tPIter; tPIter++)
    {
      fprintf(stdout, "'%s'->", (*tPIter).c_str());
    }
    fprintf(stdout, "\n");
  }

  return 0;
}
