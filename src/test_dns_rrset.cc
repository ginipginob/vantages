#include <stdio.h>

#include <iostream>
#include <string>

#include "dns_rrset.h"
#include "dns_a.h"
#include "dns_soa.h"
#include "dns_rrsig.h"
#include "dns_name.h"
#include "dns_err.h"
#include "dns_rr_fact.h"

using namespace std;

int main(int argc, char *argv[])
{
  int iRet = 1;

  DnsRRset oSet1;
  DnsRRset oSameAsSet1;
  DnsRRset oDiffFromSet1;
  DnsRRset oEquivToSet1;

  RRList_t tList1;
  RRList_t tSameAsList1;
  RRList_t tDiffFromList1;
  RRList_t tEquivToList1;
  for (uint32_t i = 0; i < 10; i++)
  {
    DnsName oName = "blah.";
    DnsName oName2 = "blah.";
    int i2 = i + 20;
    string sA = "a";
    string sB = "b";
    /*
    DnsA *pA = new DnsA(); pA->set_name(oName); pA->set_rdata((u_char *) &i, 4);
    DnsA *pA2 = new DnsA(); pA2->set_name(oName); pA2->set_rdata((u_char *) &i2, 4);
    */
    /*
    DnsSoa *pA = new DnsSoa(sA, sA, 1, 1, 1, 1, 1);pA->set_name(oName);
    DnsSoa *pCopyOfA = new DnsSoa(sA, sA, 1, 1, 1, 1, 1);pCopyOfA->set_name(oName);
    DnsSoa *pA2 = new DnsSoa(sB, sB, 2, 2, 2, 2, 2);pA2->set_name(oName);
    */
    DnsSoa *pA = (DnsSoa *) DnsRrFactory::getInstance().create(DNS_RR_SOA);pA->setRName(sA);pA->setMName(sA);pA->set_name(oName);
    DnsSoa *pCopyOfA = (DnsSoa *) DnsRrFactory::getInstance().create(DNS_RR_SOA);pCopyOfA->setRName(sA);pCopyOfA->setMName(sA);pCopyOfA->set_name(oName);
    DnsSoa *pA2 = (DnsSoa *) DnsRrFactory::getInstance().create(DNS_RR_SOA);pA2->setRName(sB);pA2->setMName(sA);pA2->set_name(oName);
    DnsRrsig *pSig1 = new DnsRrsig();
    pSig1->setSig(sA);
    pSig1->set_name(oName);
    DnsRrsig *pSig2 = new DnsRrsig();
    pSig2->setSig(sB);
    pSig2->set_name(oName);

    tList1.push_back(pA);
//    tList1.push_back(pKey1);
    tList1.push_back(pSig1);

    tSameAsList1.push_back(pCopyOfA);
    tSameAsList1.push_back(pSig1);

    tDiffFromList1.push_back(pA2);
//    tDiffFromList1.push_back(pKey2);
    tDiffFromList1.push_back(pSig1);

    tEquivToList1.push_back(pCopyOfA);
    tEquivToList1.push_back(pSig2);
  }

  if (!oSet1.init(tList1))
  {
    cerr << "Unable to init set1: " << DnsError::getInstance().getError() << endl;
  }
  else if (!oSameAsSet1.init(tSameAsList1))
  {
    cerr << "Unable to init same as set 1: " << DnsError::getInstance().getError() << endl;
  }
  else if (!oDiffFromSet1.init(tDiffFromList1))
  {
    cerr << "Unable to init oDiffFromSet1: " << DnsError::getInstance().getError() << endl;
  }
  else if (!oEquivToSet1.init(tEquivToList1))
  {
    cerr << "Unable to init oEquivToSet1: " << DnsError::getInstance().getError() << endl;
  }
  else if (!(oSet1 == oSameAsSet1))
  {
    cerr << "Set1 does not seem to be the same as oSameAsSet1" << endl;
  }
  else if (oSet1 == oDiffFromSet1)
  {
    cerr << "Set1 seems to be the same as oDiffFromSet1" << endl;
  }
  else if (!oSet1.equiv(oEquivToSet1))
  {
    cerr << "Set1 does not seem to be equiv to oEquivToSet1" << endl;
  }
  else if (oSet1.equiv(oDiffFromSet1))
  {
    cerr << "Set1 seems to equiv to oDiffFromSet1" << endl;
  }
  else
  {
    cout << ">>>SUCCESS<<<" << endl;
    iRet = 0;
  }

  return iRet;
}
