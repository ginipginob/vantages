/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <string>

#include "phas_app.h"
#include "phas_bgp_msg.h"
#include "phas_set_change_event.h"
#include "phas_sub_alloc_event.h"
#include "ps_config.h"
#include "ps_logger.h"

using namespace std;

const char *PhasApp::s_szHttpPath = "phas";

//static int g_iCount = 0;

PhasApp::PhasApp()
  : m_iFD(-1),
    m_bInit(false),
    m_bPromiscAlert(false),
    m_oEventQueue(m_oEventMgr)
    
{

}

PhasApp::~PhasApp()
{
  disconnect();
}

const char *PhasApp::getHttpPath()
{
  return s_szHttpPath;
}

const char *PhasApp::getHttpPass()
{
  return PsConfig::getInstance().getValue(PHAS_CONFIG_APP_PASS);
}

HttpListenerCtx *PhasApp::createCtx()
{
  return NULL;
}

bool PhasApp::enabled()
{
  return (NULL != PsConfig::getInstance().getValue(PHAS_CONFIG_APP_ENABLED));
}

bool PhasApp::connect()
{
  bool bRet = false;

  disconnect();

  PsConfig &oConfig = PsConfig::getInstance();
  const char *szPort = NULL;
  const char *szAddr = NULL;
  int iPort = 0;
  struct in_addr tAddr;
  struct sockaddr_in tSockAddr;

  memset(&tAddr, 0, sizeof(tAddr));
  memset(&tSockAddr, 0, sizeof(tSockAddr));

  tSockAddr.sin_family = AF_INET;

  if (NULL == (szAddr = oConfig.getValue(PHAS_BGPMON_ADDR)))
  {
    ps_elog(PSL_CRITICAL, "Unable to find server name in config file.\n");
  }
  else if (NULL == (szPort = oConfig.getValue(PHAS_BGPMON_PORT)))
  {
    ps_elog(PSL_CRITICAL, "Unable to find port in config file.\n");
  }
  else
  {
    bool bSent = true;
    if (0 == inet_aton(szAddr, &tAddr))
    {
      struct hostent *pHosts = gethostbyname(szAddr);
      if (NULL == pHosts)
      {
        ps_elog(PSL_CRITICAL, "Unable to lookup address: '%s' because: '%s'\n", szAddr, hstrerror(h_errno));
        bSent = false;
      }
      else
      {
        tAddr.s_addr = *(uint32_t *) pHosts->h_addr;
      }
    }

    if (!bSent)
    {
      ps_elog(PSL_CRITICAL, "Unable to resolve server address.\n");
    }
    else if (0 == (iPort = (int) strtol(szPort, NULL, 10)))
    {
      ps_elog(PSL_CRITICAL, "Unable to convert port: '%s'", szPort);
    }
    else if (-1 == (m_iFD = socket(AF_INET, SOCK_STREAM, 0)))
    {
      ps_elog(PSL_CRITICAL, "Unable to create socket: '%s'\n", strerror(errno));
    }
    else
    {
      tSockAddr.sin_addr.s_addr = tAddr.s_addr;
      tSockAddr.sin_port = htons(iPort);
      tSockAddr.sin_family = AF_INET;
      ps_elog(PSL_INFO, "Connecting to %s : %d...\n", inet_ntoa(tAddr), iPort);

      if (0 != (::connect(m_iFD, (struct sockaddr *) &tSockAddr, (socklen_t) sizeof(tSockAddr))))
      {
        ps_elog(PSL_CRITICAL, "Unable to connect: '%s'\n", strerror(errno));
        close(m_iFD);
        m_iFD = -1;
      }
      else
      {
        bRet = true;
      }
    }
  }

  return bRet;
}

bool PhasApp::disconnect()
{
  if (-1 != m_iFD)
  {
    close(m_iFD);
    m_iFD = -1;
  }

  return true;
}

bool PhasApp::isConnected()
{
  return (-1 != m_iFD);
}

bool PhasApp::init()
{
  bool bRet = false;

  try
  {
    m_bInit = false;

    const char *szEventRootDir = PsConfig::getInstance().getValue(PHAS_CONFIG_EVENT_LOG_ROOT_DIR);

    if (NULL == szEventRootDir)
    {
      ps_elog(PSL_CRITICAL, "Unable to find root dir.  Using '.'\n");
      szEventRootDir = ".";
    }
    string sEventRootDir = szEventRootDir;

    if (!m_oPrefixTree.initTopRoots())
    {
      ps_elog(PSL_CRITICAL, "Unable to init prefix Tree top roots.\n");
    }
    else if (!m_oEventMgr.init(sEventRootDir))
    {
      ps_elog(PSL_CRITICAL, "Unable to init event manager.\n");
    }
    else if (!m_oEventQueue.init(PHAS_EVENT_QUEUE_DEFAULT_MAX))
    {
      ps_elog(PSL_CRITICAL, "Unable to init event queue.\n");
    }
    else
    {
      m_bInit = true;
      bRet = true;
    }
  }
  catch (...)
  {
    ps_elog(PSL_CRITICAL, "Caught Exception: Unable to init PHAS app.\n");
    m_bInit = false;
    bRet = false;
  }

  return bRet;
}

bool PhasApp::execute()
{
  bool bRet = false;

  if (!m_bInit)
  {
    ps_elog(PSL_CRITICAL, "Unable to run PHAS app if not initialized.\n");
  }
  else if (!m_oEventQueue.start())
  {
    ps_elog(PSL_CRITICAL, "Unable to start event queue.\n");
  }
  else
  {
    while (getRun())
    {
      try
      {
        m_bPromiscAlert = (NULL != PsConfig::getInstance().getValue(PHAS_CONFIG_PROMISC_ALERT));
        PhasBgpMsg oMsg;

        if (!isConnected())
        {
          if (!connect())
          {
            ps_elog(PSL_CRITICAL, "Unable to connect to BGPMon... Will retry in a minute...\n");
            sleep(60);
            ps_elog(PSL_CRITICAL, "Retrying BGPMon connection...\n");
          }
          else if (!m_oParser.init(m_iFD))
          {
            ps_elog(PSL_CRITICAL, "Unable to init parser... terminating.\n");
            kill();
          }
        }
        else if (!m_oParser.getNextUpdate(oMsg))
        {
          ps_elog(PSL_CRITICAL, "Unable to get message from BGPMon. Reconnecting...\n");
          disconnect();
        }
        else if (!applyUpdate(oMsg))
        {
          ps_elog(PSL_ERROR, "Unable to apply update.\n");
        }
        else
        {
          bRet = true;
        }
      }
      catch(...)
      {
        ps_elog(PSL_CRITICAL, "Caught Exception.\n");
      }
    }
  }

  return bRet;
}

bool PhasApp::applyUpdate(PhasBgpMsg &p_oMsg)
{
  bool bRet = false;

  unsigned int uOrigin = (unsigned int) strtol(p_oMsg.getOrigin().c_str(), NULL, 10);
  unsigned int uNextHop = (unsigned int) strtol(p_oMsg.getNextHop().c_str(), NULL, 10);

/*
  if (0 == uOrigin)
  {
    ps_elog(PSL_ERROR, "Unable to parse origin: '%s' into unsigned.\n", p_oMsg.getOrigin().c_str());
  }
  else if (0 == uNextHop)
  {
    ps_elog(PSL_ERROR, "Unable to parse next hop: '%s' into unsigned.\n", p_oMsg.getNextHop().c_str());
  }
  else 
*/
  if (!applyUpdate(p_oMsg, uOrigin, m_oOriginPrefixMap, PHAS_ORIGIN_ORIGIN))
  {
    ps_elog(PSL_CRITICAL, "Unable to apply update to origin map.\n");
  }
  else if (!applyUpdate(p_oMsg, uNextHop, m_oNextHopPrefixMap, PHAS_ORIGIN_NEXT_HOP))
  {
    ps_elog(PSL_CRITICAL, "Unable to apply update to next hop map.\n");
  }
  else
  {
    bRet = true;
  }

  return bRet;
}

bool PhasApp::applyUpdate(PhasBgpMsg &p_oMsg, unsigned int p_uAS, PrefixPeerMap_t &p_oPrefixMap, phas_origin_type_e p_eType)
{
  bool bRet = false;

  PrefixIter_t tPrefixIter;
  string sPeerID = p_oMsg.getMonitorID();
  int iWait = PHAS_WITHDRAW_WAIT_DEFAULT;
  const char *szWithdrawWaitTime = PsConfig::getInstance().getValue(PHAS_CONFIG_WITHDRAW_WAIT);
  if (NULL != szWithdrawWaitTime)
  {
    iWait = (int) strtol(szWithdrawWaitTime, NULL, 10);
  }

  for (tPrefixIter = p_oMsg.beginWithdrawn();
       p_oMsg.endWithdrawn() != tPrefixIter;
       tPrefixIter++)
  {
    bRet = true;
    string &sPrefix = *tPrefixIter;
    PrefixPeerIter_t tPeerIter = p_oPrefixMap.find(sPrefix);
    if (p_oPrefixMap.end() != tPeerIter)
    {
      PeerOriginMap_t &tPeerMap = tPeerIter->second;
      PeerOriginIter_t tOriginIter = tPeerMap.find(sPeerID);
      if (tPeerMap.end() != tOriginIter)
      {
        bool bFound = true;
        OriginSet_t oPreSet;
        if (!getOriginSet(sPrefix, p_oPrefixMap, oPreSet))
        {
          ps_elog(PSL_CRITICAL, "Unable to locate origin set for prefix: '%s'\n", sPrefix.c_str());
          bFound = false;
        }

        phas_origin_t tOrigin = tOriginIter->second;
        ps_elog(PSL_DEBUG, "Withdraw for '%s' -> %u [%s]\n", sPrefix.c_str(), tOrigin.m_uAS, sPeerID.c_str());
        tPeerMap.erase(tOriginIter);

        OriginSet_t oPostSet;
        if (!bFound)
        {
          ps_elog(PSL_DEBUG, "Skipping...\n");
        }
        else
        {
          getOriginSet(sPrefix, p_oPrefixMap, oPostSet);
          oPreSet.sort();
          oPostSet.sort();
          if (oPreSet != oPostSet)
          {
            PhasSetChangeEvent *pEvent = new PhasSetChangeEvent(sPrefix,
                                                                p_oMsg.getTimestamp(),
                                                                p_eType,
                                                                PHAS_SET_CHANGE_WITHDRAW,
                                                                tOrigin.m_uAS,
                                                                oPostSet);
            pEvent->setDelay(iWait);
            if (!m_oEventQueue.enqueue(*pEvent))
            {
              ps_elog(PSL_CRITICAL, "Unable to enqueue event.\n");
              delete pEvent;
            }
          }

          if (PHAS_ORIGIN_ORIGIN == p_eType
              && !subAllocAlarm(sPrefix, p_oMsg.getTimestamp(), iWait, false))
          {
            ps_elog(PSL_CRITICAL, "Unable to check sub allocation.\n");
          }
        }
      }
    }
  }

  for (tPrefixIter = p_oMsg.beginAnnounced();
       p_oMsg.endAnnounced() != tPrefixIter;
       tPrefixIter++)
  {
    string &sPrefix = *tPrefixIter;
    bRet = true;
    bool bNewAnnouncement = false;

    if (!isInteresting(sPrefix))
    {
      ps_elog(PSL_DEBUG, "Skipping 'uninteresting' prefix: '%s'\n", sPrefix.c_str());
    }
    else
    {
      OriginSet_t oPreSet;
      if (!getOriginSet(sPrefix, p_oPrefixMap, oPreSet))
      {
        ps_elog(PSL_WARNING, "Unable to get pre-origin set (may be new announcement)\n");
      }

      PeerOriginMap_t &tPeerMap = p_oPrefixMap[sPrefix];
      PeerOriginIter_t tOriginIter = tPeerMap.find(sPeerID);
      if (tPeerMap.end() != tOriginIter)
      {
        phas_origin_t &tOrigin = tOriginIter->second;
        if (tOrigin.m_uAS != p_uAS)
        {
// <EMO> DO SOMETHING HERE
          tOrigin.m_uAS = p_uAS;
// fprintf(stderr, "ANNOUNCEMENT CHANGES ORIGIN FOR '%s' -> %u [%s]\n", sPrefix.c_str(), tOrigin.m_uAS, sPeerID.c_str());
        }
        tOrigin.m_tWhen = time(NULL);
      }
      else
      {
        phas_origin_t tOrigin;
// <EMO> DO SOMETHING HERE
        tOrigin.m_uAS = p_uAS;
        tOrigin.m_tWhen = time(NULL);
        tPeerMap[sPeerID] = tOrigin;

        if (PHAS_ORIGIN_ORIGIN == p_eType)
        {
          ps_elog(PSL_DEBUG, "New origin annoucement for '%s' -> %u [%s]\n", sPrefix.c_str(), tOrigin.m_uAS, sPeerID.c_str());
        }

        bNewAnnouncement = true;
      }

      OriginSet_t oPostSet;
      if (!getOriginSet(sPrefix, p_oPrefixMap, oPostSet))
      {
        ps_elog(PSL_CRITICAL, "Unable to get post set when this new announcement MUST have added something.\n");
      }

      oPreSet.sort();
      oPostSet.sort();
      if (oPreSet != oPostSet)
      {
        PhasSetChangeEvent *pEvent = new PhasSetChangeEvent(sPrefix,
                                                            p_oMsg.getTimestamp(),
                                                            p_eType,
                                                            PHAS_SET_CHANGE_ANNOUNCE,
                                                            p_uAS,
                                                            oPostSet);
        if (!m_oEventQueue.enqueue(*pEvent))
        {
          ps_elog(PSL_CRITICAL, "Unable to enqueue event.\n");
          delete pEvent;
        }
      }
    }

    if (bNewAnnouncement
        && PHAS_ORIGIN_ORIGIN == p_eType
        && !subAllocAlarm(sPrefix, p_oMsg.getTimestamp(), iWait, true))
    {
      ps_elog(PSL_CRITICAL, "Unable to check sub allocation.\n");
    }
  }

  return bRet;
}

inline bool PhasApp::isInteresting(std::string &p_sPrefix)
{
  return true;
}

bool PhasApp::getOriginSet(std::string &p_sPrefix,
                           PrefixPeerMap_t &p_oPrefixMap,
                           OriginSet_t &p_oOut)
{
  bool bRet = false;

  map<unsigned int, unsigned int> oMap;
  PrefixPeerIter_t tPeerIter = p_oPrefixMap.find(p_sPrefix);
  if (p_oPrefixMap.end() != tPeerIter)
  {
    PeerOriginMap_t &tPeerMap = tPeerIter->second;
    for (PeerOriginIter_t tMapIter = tPeerMap.begin();
         tPeerMap.end() != tMapIter;
         tMapIter++)
    {
      unsigned int uAS = (tMapIter->second).m_uAS;
      oMap[uAS] = uAS;
    }

    for (map<unsigned int, unsigned int>::iterator tIter = oMap.begin();
         oMap.end() != tIter;
         tIter++)
    {
      char sz[11];
      memset(sz, 0, 11);
      sprintf(sz, "%u", tIter->second);
      string s(sz);
      p_oOut.push_back(s);
    }

    bRet = true;
  }

  return bRet;
}

bool PhasApp::getPredList(std::string &p_sPrefix,
                          PrefixList_t &p_oOut)
{
  return m_oPrefixTree.getPredList(p_sPrefix, p_oOut);
}

bool PhasApp::subAllocAlarm(std::string &p_sPrefix,
                            time_t p_tMsgTime,
                            int p_iWait,
                            bool p_bAnnouncement)
{
  bool bRet = false;

  PrefixList_t oPredList;

  PhasPrefix *pPrefix = m_oPrefixTree.find(p_sPrefix);
  if (p_bAnnouncement)
  {
    if (!m_oPrefixTree.add(p_sPrefix))
    {
      ps_elog(PSL_CRITICAL, "Unable to add prefix: '%s'\n", p_sPrefix.c_str());
    }
    else if (NULL != pPrefix)
    {
      ps_elog(PSL_DEBUG, "Prefix '%s' already exists, so we will have already alarmed or whatever.  No action here.\n", p_sPrefix.c_str());
//fprintf(stderr, "Prefix '%s' already exists ('%s'), so we will have already alarmed or whatever.  No action here.\n", p_sPrefix.c_str(), pPrefix->str().c_str());
      bRet = true;
    }
    else if (!m_oPrefixTree.getPredList(p_sPrefix, oPredList))
    {
      ps_elog(PSL_CRITICAL, "For announcement: unable to get post pred list for prefix: '%s'\n", p_sPrefix.c_str());
    }
    else if (!m_bPromiscAlert && isInteresting(p_sPrefix))
    {
      ps_elog(PSL_DEBUG, "Announcement is 'interesting,' so skip the alarm.\n");
//fprintf(stderr, "Announcement is 'interesting,' so skip the alarm.\n");
      bRet = true;
    }
    else
    {
      bRet = true;
      for (PrefixIter_t tIter = oPredList.begin();
           oPredList.end() != tIter;
           tIter++)
      {
        string &sCandPrefix = *tIter;
        if (m_bPromiscAlert
            || isInteresting(sCandPrefix))
        {
          ps_elog(PSL_DEBUG, "Prefix: '%s' is not ours, but is a new sub allocation, so alarm...\n", p_sPrefix.c_str());
// fprintf(stderr, "Prefix: '%s' is not ours, but is a new sub allocation, so alarm...\n", p_sPrefix.c_str());
          PhasSubAllocEvent *pEvent = new PhasSubAllocEvent(p_sPrefix, sCandPrefix, p_tMsgTime, true, oPredList);
          if (!m_oEventQueue.enqueue(*pEvent))
          {
            ps_elog(PSL_CRITICAL, "Unable to enqueue sub allocation event '%s' -> '%s'\n", p_sPrefix.c_str(), sCandPrefix.c_str());
            delete pEvent;
          }
        }
      }
    }

    if (PsLogger::getInstance().getLevel() >= PSL_DEBUG)
    {
      pPrefix = m_oPrefixTree.find(p_sPrefix);
      ps_elog(PSL_DEBUG, "TREE - ADDED REF FOR: '%s' [%d]\n", p_sPrefix.c_str(), pPrefix->getRefCount());
    }
  }
  else
  {
    if (NULL != pPrefix)
    {
      ps_elog(PSL_DEBUG, "TREE - REMOVING REF FOR: '%s' [%d]\n", p_sPrefix.c_str(), pPrefix->getRefCount());
    }

    if (NULL == pPrefix)
    {
      ps_elog(PSL_DEBUG, "We've never seen prefix '%s' before, so losing it doesn't matter.\n", p_sPrefix.c_str());
//fprintf(stderr, "We've never seen prefix '%s' before, so losing it doesn't matter.\n", p_sPrefix.c_str());
      bRet = true;
    }
    else if (!m_oPrefixTree.getPredList(p_sPrefix, oPredList))
    {
      ps_elog(PSL_CRITICAL, "For withdraw: unable to get post pred list for prefix: '%s'\n", p_sPrefix.c_str());
    }
    else if (!m_oPrefixTree.remove(p_sPrefix))
    {
// m_oPrefixTree.printTree();
      ps_elog(PSL_CRITICAL, "Unable to remove prefix: '%s'\n", p_sPrefix.c_str());
      if (NULL != pPrefix->getParent())
      {
        ps_elog(PSL_CRITICAL, "Parent of '%s' is set as: '%s'\n", p_sPrefix.c_str(), pPrefix->getParent()->str().c_str());
      }
    }
    else if (NULL != (pPrefix = m_oPrefixTree.find(p_sPrefix)))
    {
      ps_elog(PSL_DEBUG, "Withdraw did not change the hierarchy.  Prefix '%s' has %d refs.\n", p_sPrefix.c_str(), pPrefix->getRefCount());
      bRet = true;
    }
    else if (!m_bPromiscAlert && isInteresting(p_sPrefix))
    {
      ps_elog(PSL_DEBUG, "Announcement is 'interesting,' so skip the alarm.\n");
//fprintf(stderr, "Announcement is 'interesting,' so skip the alarm.\n");
      bRet = true;
    }
    else
    {
      bRet = true;
      for (PrefixIter_t tIter = oPredList.begin();
           oPredList.end() != tIter;
           tIter++)
      {
        string &sCandPrefix = *tIter;
        if (m_bPromiscAlert
            || isInteresting(sCandPrefix))
        {
          ps_elog(PSL_DEBUG, "Withdrawn prefix: '%s' is not ours and changed hierarchy, and IS a sub allocation, so alert.\n", p_sPrefix.c_str());
          PhasSubAllocEvent *pEvent = new PhasSubAllocEvent(p_sPrefix, sCandPrefix, p_tMsgTime, false, oPredList);
          pEvent->setDelay(p_iWait);
          if (!m_oEventQueue.enqueue(*pEvent))
          {
            ps_elog(PSL_CRITICAL, "Unable to enqueue sub allocation event '%s' -> '%s'\n", p_sPrefix.c_str(), sCandPrefix.c_str());
            delete pEvent;
          }
        }
      }
    }
  }

  return bRet;
}

