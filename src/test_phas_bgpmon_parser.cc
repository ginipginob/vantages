/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>

#include "phas_bgpmon_parser.h"
#include "phas_bgp_msg.h"
#include "ps_logger.h"

int main(int argc, char *argv[])
{
  int iRet = 0;

  int iMessages = 10;
  if (argc > 1)
  {
    iMessages = (int) strtol(argv[1], NULL, 10);
  }

  fprintf(stdout, "Going to get %d messages and then exit.\n", iMessages);

  PsLogger::getInstance().setLevel(PSL_INFO);

  PhasBgpmonParser oParser;
  if (!oParser.init(0))
  {
    fprintf(stderr, "Unable to init with FD 0\n");
  }
  else
  {
    for (int i = 0; i < iMessages; i++)
    {
      PhasBgpMsg oMsg;
      if (!oParser.getNextUpdate(oMsg))
      {
        fprintf(stderr, "Unable to get message number: %d\n", i);
        break;
      }
      else
      {
        fprintf(stdout, "GOT MESSAGE WITH from [%d]: ID = '%s', %d withdrawn Prefixes, %d announced, '%s' as the next hop, and '%s' as the origin.\n",
                (int) oMsg.getTimestamp(),
                oMsg.getMonitorID().c_str(),
                (int) oMsg.sizeWithdrawn(),
                (int) oMsg.sizeAnnounced(),
                oMsg.getNextHop().c_str(),
                oMsg.getOrigin().c_str());
      }
    }
  }

  return iRet;
}

