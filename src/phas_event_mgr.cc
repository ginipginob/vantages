/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>
#include <time.h>
#include <sys/stat.h>
#include <string.h>

#include <sstream>

#include "phas_event_mgr.h"
#include "phas_event.h"
#include "phas_set_change_event.h"
#include "phas_sub_alloc_event.h"
#include "phas_defs.h"
#include "ps_logger.h"

using namespace std;

PhasEventMgr::PhasEventMgr()
  : m_tLastRoll(0)
{

}

PhasEventMgr::~PhasEventMgr()
{
//  m_oOriginSetChangeStream.close();
  m_oOriginAlarmsStream.close();
//  m_oNhSetChangeStream.close();
  m_oNhAlarmsStream.close();
  m_oSubAllocStream.close();

  m_sRootDir = "";
  m_sOriginAlarmFile = "";
  m_sNhAlarmFile = "";
  m_sSubAllocFile = "";
}

bool PhasEventMgr::init(std::string &p_sRootDir)
{
  m_sRootDir = p_sRootDir;

  return rollFiles();
}

time_t PhasEventMgr::getLastRoll()
{
  return m_tLastRoll;
}

bool PhasEventMgr::handleEvent(PhasEvent &p_oEvent)
{
  bool bRet = false;

  const string &sType = p_oEvent.getEventType();

  if (m_sRootDir == "")
  {
    ps_elog(PSL_CRITICAL, "Unable to handleEvent() without root dir (call init())\n");
  }
  else if (timeToRoll() && !rollFiles())
  {
    ps_elog(PSL_CRITICAL, "Unable to roll files.\n");
  }
  else if (sType == PhasSetChangeEvent::s_sEventTypeName)
  {
    bRet = true;
    PhasSetChangeEvent &oSCE = (PhasSetChangeEvent &) p_oEvent;
    if (oSCE.getOriginType() == PHAS_ORIGIN_ORIGIN)
    {
//fprintf(stderr, "STORING: '%s'\n", oSCE.toLegacyString().c_str());
      m_oOriginAlarmsStream << oSCE.toLegacyString() << "\n";
    }
    else if (oSCE.getOriginType() == PHAS_ORIGIN_NEXT_HOP)
    {
      m_oNhAlarmsStream << oSCE.toLegacyString() << "\n";
    }
    else
    {
      ps_elog(PSL_ERROR, "Unknown origin type for event: '%d'\n", (int) oSCE.getOriginType());
      bRet = false;
    }
  }
  else if (sType == PhasSubAllocEvent::s_sEventTypeName)
  {
fprintf(stderr, "STORING: '%s'\n", p_oEvent.toLegacyString().c_str());
    m_oSubAllocStream << p_oEvent.toLegacyString() << "\n";
    bRet = true;
  }

  return bRet;
}

bool PhasEventMgr::timeToRoll()
{
  time_t tNow = time(NULL);

  return (m_tLastRoll + 86400 < tNow);
}

bool PhasEventMgr::rollFiles()
{
  bool bRet = false;

  time_t tNow = time(NULL);
  if (m_sRootDir == "")
  {
    ps_elog(PSL_CRITICAL, "Unable to roll files without a root\n");
  }
  else if (m_tLastRoll + 86400 >= tNow)
  {
    ps_elog(PSL_WARNING, "No necessary to roll files: %d < %d\n", (int) m_tLastRoll + 86400, (int) tNow);
ps_elog(PSL_CRITICAL, "No necessary to roll files: %d < %d\n", (int) m_tLastRoll + 86400, (int) tNow);
  }
  else
  {
    try
    {
      struct tm tTime;
      memset(&tTime, 0, sizeof(tTime));
      gmtime_r(&tNow, &tTime);

      string sUpperDir;

      char szUpperDir[8];
      memset(szUpperDir, 0, 8);
      sprintf(szUpperDir, "%d.%02d", (1900 + tTime.tm_year), tTime.tm_mon + 1);

      char szFileTime[11];
      memset(szFileTime, 0, 11);
      sprintf(szFileTime, "%04d%02d%02d", (1900 + tTime.tm_year), tTime.tm_mon + 1, tTime.tm_mday);

      sUpperDir = m_sRootDir + "/";
      sUpperDir += szUpperDir;
      mkdir(sUpperDir.c_str(), 0755);

      m_sNhAlarmFile = sUpperDir + "/LastHop";
      mkdir(m_sNhAlarmFile.c_str(), 0755);

      m_sNhAlarmFile += "/Alarm";
      mkdir(m_sNhAlarmFile.c_str(), 0755);

      m_sNhAlarmFile += "/LastHopAlarm.";
      m_sNhAlarmFile += szFileTime;
      m_sNhAlarmFile += ".txt";
      m_oNhAlarmsStream.close();
      m_oNhAlarmsStream.open(m_sNhAlarmFile.c_str());

      m_sOriginAlarmFile = sUpperDir + "/Origin";
      mkdir(m_sOriginAlarmFile.c_str(), 0755);

      m_sOriginAlarmFile += "/Alarm";
      mkdir(m_sOriginAlarmFile.c_str(), 0755);

      m_sOriginAlarmFile += "/OriginAlarm.";
      m_sOriginAlarmFile += szFileTime;
      m_sOriginAlarmFile += ".txt";
      m_oOriginAlarmsStream.close();
      m_oOriginAlarmsStream.open(m_sOriginAlarmFile.c_str());

      m_sSubAllocFile = sUpperDir + "/SubAllocation";
      mkdir(m_sSubAllocFile.c_str(), 0755);
      m_sSubAllocFile += "/Alarm";
      mkdir(m_sSubAllocFile.c_str(), 0755);
      m_sSubAllocFile += "/SubAllocationAlarm.";
      m_sSubAllocFile += szFileTime;
      m_sSubAllocFile += ".txt";
      m_oSubAllocStream.close();
      m_oSubAllocStream.open(m_sSubAllocFile.c_str());

      tTime.tm_sec = 0;
      m_tLastRoll = timegm(&tTime);
      bRet = true;
    }
    catch (...)
    {
      ps_elog(PSL_CRITICAL, "Caught unknown exception.\n");
      bRet = false;
    }
  }

  return bRet;
}

