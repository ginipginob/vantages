/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _PHAS_BGP_MSG_H
#define _PHAS_BGP_MSG_H

#include <string>

#include "phas_defs.h"

class PhasBgpMsg
{
  // Member Variables
  private:
    time_t m_tTimestamp;
    std::string m_sMonitorID;
    std::string m_sOrigin;
    std::string m_sNextHop;
    std::string m_sSrcAddr;
    std::string m_sSrcAS;
    std::string m_sSrcPort;
    std::string m_sDstAddr;
    std::string m_sDstAS;
    std::string m_sDstPort;
    PrefixList_t m_oWithdrawnList;
    PrefixList_t m_oAnnouncedList;

  // Methods
  public:
    PhasBgpMsg();
    virtual ~PhasBgpMsg();

    std::string getMonitorID();

    time_t getTimestamp();
    void setTimestamp(time_t p_tTimestamp);

    std::string &getSrcAddr();
    void setSrcAddr(std::string &p_sSrcAddr);

    std::string &getSrcAS();
    void setSrcAS(std::string &p_sSrcAS);

    std::string &getSrcPort();
    void setSrcPort(std::string &p_sSrcPort);

    std::string &getDstAddr();
    void setDstAddr(std::string &p_sDstAddr);

    std::string &getDstAS();
    void setDstAS(std::string &p_sDstAS);

    std::string &getDstPort();
    void setDstPort(std::string &p_sDstPort);

    void addNextAS(std::string &p_sAS);

    std::string &getOrigin();
    void setOrigin(std::string &p_sOrigin);

    std::string &getNextHop();
    void setNextHop(std::string &p_sNextHop);

    PrefixIter_t beginWithdrawn();
    PrefixIter_t endWithdrawn();
    size_t sizeWithdrawn();
    void addWithdrawn(std::string &p_sPrefix);

    PrefixIter_t beginAnnounced();
    PrefixIter_t endAnnounced();
    size_t sizeAnnounced();
    void addAnnounced(std::string &p_sPrefix);

    bool isUpdate();
};

#endif
