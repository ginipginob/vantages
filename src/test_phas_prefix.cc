/*
 * Copyright (c) 2008,2009, University of California, Los Angeles and 
 * Colorado State University All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NLnetLabs nor the names of its
 *       contributors may be used to endorse or promote products derived from this
 *       software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include "config.h"
#include <stdio.h>

#include <string>

#include "phas_prefix.h"
#include "ps_logger.h"

using namespace std;

int main(int argc, char *argv[])
{
  string sPrefix = "131.179/16";
  string sCoveredPrefix = "131.179.96/24";
  string sNotCoveredPrefix = "192.168/16";

  PsLogger::getInstance().setLevel(PSL_DEBUG);

  PhasPrefix oPrefix(sPrefix);
  PhasPrefix oCovered(sCoveredPrefix);
  PhasPrefix oNotCovered(sNotCoveredPrefix);

  bool bRet = false;

  if (!oPrefix.isValid())
  {
    fprintf(stderr, "Main Prefix is not valid.\n");
    fprintf(stderr, ">>>FAIL<<<\n");
  }
  else if (!oCovered.isValid())
  {
    fprintf(stderr, "Covered Prefix is not valid.\n");
    fprintf(stderr, ">>>FAIL<<<\n");
  }
  else if (!oNotCovered.isValid())
  {
    fprintf(stderr, "Not Covered Prefix is not valid.\n");
    fprintf(stderr, ">>>FAIL<<<\n");
  }
  else if (!oPrefix.covers(oCovered))
  {
    fprintf(stderr, "Main prefix '%s' does not cover covered prefix: '%s'\n", oPrefix.str().c_str(), oCovered.str().c_str());
    fprintf(stderr, ">>>FAIL<<<\n");
  }
  else if (!oPrefix.equals(oPrefix))
  {
    fprintf(stderr, "Main prefix '%s' is not equal to itsself: '%s'\n", oPrefix.str().c_str(), oPrefix.str().c_str());
    fprintf(stderr, ">>>FAIL<<<\n");
  }
  else if (oPrefix.covers(oNotCovered))
  {
    fprintf(stderr, "Main prefix '%s' DOES cover not-covered prefix: '%s'\n", oPrefix.str().c_str(), oNotCovered.str().c_str());
    fprintf(stderr, ">>>FAIL<<<\n");
  }
  else if (!oCovered.setParent(&oPrefix))
  {
    fprintf(stderr, "Unable to set parent '%s' of covered prefix: '%s'\n", oPrefix.str().c_str(), oCovered.str().c_str());
    fprintf(stderr, ">>>FAIL<<<\n");
  }
  else if (oPrefix.setParent(&oCovered))
  {
    fprintf(stderr, "WAS able to set parent '%s' as child prefix: '%s'\n", oCovered.str().c_str(), oPrefix.str().c_str());
    fprintf(stderr, ">>>FAIL<<<\n");
  }
  else
  {
    fprintf(stdout, ">>>SUCCESS<<<\n");
    bRet = true;
  }

  return (int) !bRet;
}
